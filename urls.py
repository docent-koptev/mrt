from django.conf.urls import patterns, include, url
from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin

from plugins.sitemap import PagesSitemap,NewsSitemap
import forms_builder.forms.urls

admin.autodiscover()

sitemaps = {
    'pages': PagesSitemap,
    'news': NewsSitemap,
    }

handler404 = 'plugins.page.views.page_view'

urlpatterns = patterns('',
    url(r'^admin/', include(admin.site.urls)),
    url(r'', include('plugins.urls')),
    url(r'^sitemap.xml$', 'django.contrib.sitemaps.views.sitemap', {'sitemaps': sitemaps}),
    url(r'^forms/', include(forms_builder.forms.urls)),
    url(r'^ckeditor/', include('ckeditor.urls')),
    url(r'^admin_tools/', include('admin_tools.urls')),
    
    

)
urlpatterns += patterns('',
    url(r'^', include('filer.server.urls')),
)
if settings.DEBUG:
    urlpatterns += patterns('',
        url(r'^media/(?P<path>.*)$', 'django.views.static.serve', {
            'document_root': settings.MEDIA_ROOT,
        }),
    )
    urlpatterns += patterns('django.contrib.staticfiles.views',
        url(r'^media/static/(?P<path>.*)$', 'serve'),
    )