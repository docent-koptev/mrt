-- MySQL dump 10.13  Distrib 5.5.35, for debian-linux-gnu (i686)
--
-- Host: localhost    Database: mrt
-- ------------------------------------------------------
-- Server version	5.5.35-0ubuntu0.13.10.2

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `admin_tools_dashboard_preferences`
--

DROP TABLE IF EXISTS `admin_tools_dashboard_preferences`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin_tools_dashboard_preferences` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `data` longtext NOT NULL,
  `dashboard_id` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `admin_tools_dashboard_preferences_dashboard_id_575b1104_uniq` (`dashboard_id`,`user_id`),
  KEY `admin_tools_dashboard_preferences_6340c63c` (`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin_tools_dashboard_preferences`
--

LOCK TABLES `admin_tools_dashboard_preferences` WRITE;
/*!40000 ALTER TABLE `admin_tools_dashboard_preferences` DISABLE KEYS */;
INSERT INTO `admin_tools_dashboard_preferences` VALUES (1,1,'{}','dashboard'),(2,1,'{}','device-dashboard'),(3,1,'{}','gblocks-dashboard');
/*!40000 ALTER TABLE `admin_tools_dashboard_preferences` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `admin_tools_menu_bookmark`
--

DROP TABLE IF EXISTS `admin_tools_menu_bookmark`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin_tools_menu_bookmark` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `url` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `admin_tools_menu_bookmark_6340c63c` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin_tools_menu_bookmark`
--

LOCK TABLES `admin_tools_menu_bookmark` WRITE;
/*!40000 ALTER TABLE `admin_tools_menu_bookmark` DISABLE KEYS */;
/*!40000 ALTER TABLE `admin_tools_menu_bookmark` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_group`
--

DROP TABLE IF EXISTS `auth_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(80) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group`
--

LOCK TABLES `auth_group` WRITE;
/*!40000 ALTER TABLE `auth_group` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_group_permissions`
--

DROP TABLE IF EXISTS `auth_group_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_group_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `group_id` (`group_id`,`permission_id`),
  KEY `auth_group_permissions_5f412f9a` (`group_id`),
  KEY `auth_group_permissions_83d7f98b` (`permission_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group_permissions`
--

LOCK TABLES `auth_group_permissions` WRITE;
/*!40000 ALTER TABLE `auth_group_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_group_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_permission`
--

DROP TABLE IF EXISTS `auth_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `codename` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `content_type_id` (`content_type_id`,`codename`),
  KEY `auth_permission_37ef4eb4` (`content_type_id`)
) ENGINE=MyISAM AUTO_INCREMENT=140 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_permission`
--

LOCK TABLES `auth_permission` WRITE;
/*!40000 ALTER TABLE `auth_permission` DISABLE KEYS */;
INSERT INTO `auth_permission` VALUES (1,'Can add log entry',1,'add_logentry'),(2,'Can change log entry',1,'change_logentry'),(3,'Can delete log entry',1,'delete_logentry'),(4,'Can add permission',2,'add_permission'),(5,'Can change permission',2,'change_permission'),(6,'Can delete permission',2,'delete_permission'),(7,'Can add group',3,'add_group'),(8,'Can change group',3,'change_group'),(9,'Can delete group',3,'delete_group'),(10,'Can add user',4,'add_user'),(11,'Can change user',4,'change_user'),(12,'Can delete user',4,'delete_user'),(13,'Can add content type',5,'add_contenttype'),(14,'Can change content type',5,'change_contenttype'),(15,'Can delete content type',5,'delete_contenttype'),(16,'Can add session',6,'add_session'),(17,'Can change session',6,'change_session'),(18,'Can delete session',6,'delete_session'),(19,'Can add site',7,'add_site'),(20,'Can change site',7,'change_site'),(21,'Can delete site',7,'delete_site'),(22,'Can add kv store',8,'add_kvstore'),(23,'Can change kv store',8,'change_kvstore'),(24,'Can delete kv store',8,'delete_kvstore'),(25,'Can add migration history',9,'add_migrationhistory'),(26,'Can change migration history',9,'change_migrationhistory'),(27,'Can delete migration history',9,'delete_migrationhistory'),(28,'Can add generic flatblock',10,'add_genericflatblock'),(29,'Can change generic flatblock',10,'change_genericflatblock'),(30,'Can delete generic flatblock',10,'delete_genericflatblock'),(31,'Can add title',11,'add_title'),(32,'Can change title',11,'change_title'),(33,'Can delete title',11,'delete_title'),(34,'Can add text',12,'add_text'),(35,'Can change text',12,'change_text'),(36,'Can delete text',12,'delete_text'),(37,'Can add image',13,'add_image'),(38,'Can change image',13,'change_image'),(39,'Can delete image',13,'delete_image'),(40,'Can add title and text',14,'add_titleandtext'),(41,'Can change title and text',14,'change_titleandtext'),(42,'Can delete title and text',14,'delete_titleandtext'),(43,'Can add title text and image',15,'add_titletextandimage'),(44,'Can change title text and image',15,'change_titletextandimage'),(45,'Can delete title text and image',15,'delete_titletextandimage'),(46,'Can add banner',16,'add_banner'),(47,'Can change banner',16,'change_banner'),(48,'Can delete banner',16,'delete_banner'),(49,'Can add Метро',17,'add_metro'),(50,'Can change Метро',17,'change_metro'),(51,'Can delete Метро',17,'delete_metro'),(52,'Can add Мощность',18,'add_power'),(53,'Can change Мощность',18,'change_power'),(54,'Can delete Мощность',18,'delete_power'),(55,'Can add Вид аппарата',19,'add_apparat'),(56,'Can change Вид аппарата',19,'change_apparat'),(57,'Can delete Вид аппарата',19,'delete_apparat'),(58,'Can add Исследовательский центр',20,'add_center'),(59,'Can change Исследовательский центр',20,'change_center'),(60,'Can delete Исследовательский центр',20,'delete_center'),(61,'Can add Вид обследования',21,'add_inspection'),(62,'Can change Вид обследования',21,'change_inspection'),(63,'Can delete Вид обследования',21,'delete_inspection'),(64,'Can add Цена',22,'add_pricelist'),(65,'Can change Цена',22,'change_pricelist'),(66,'Can delete Цена',22,'delete_pricelist'),(67,'Can add SEO (Path)',23,'add_seometadatapath'),(68,'Can change SEO (Path)',23,'change_seometadatapath'),(69,'Can delete SEO (Path)',23,'delete_seometadatapath'),(70,'Can add SEO (Model Instance)',24,'add_seometadatamodelinstance'),(71,'Can change SEO (Model Instance)',24,'change_seometadatamodelinstance'),(72,'Can delete SEO (Model Instance)',24,'delete_seometadatamodelinstance'),(73,'Can add SEO (Model)',25,'add_seometadatamodel'),(74,'Can change SEO (Model)',25,'change_seometadatamodel'),(75,'Can delete SEO (Model)',25,'delete_seometadatamodel'),(76,'Can add SEO (View)',26,'add_seometadataview'),(77,'Can change SEO (View)',26,'change_seometadataview'),(78,'Can delete SEO (View)',26,'delete_seometadataview'),(79,'Can add bookmark',31,'add_bookmark'),(80,'Can change bookmark',31,'change_bookmark'),(81,'Can delete bookmark',31,'delete_bookmark'),(82,'Can add dashboard preferences',32,'add_dashboardpreferences'),(83,'Can change dashboard preferences',32,'change_dashboardpreferences'),(84,'Can delete dashboard preferences',32,'delete_dashboardpreferences'),(85,'Can add Form entry',33,'add_formentry'),(86,'Can change Form entry',33,'change_formentry'),(87,'Can delete Form entry',33,'delete_formentry'),(88,'Can add Form field entry',34,'add_fieldentry'),(89,'Can change Form field entry',34,'change_fieldentry'),(90,'Can delete Form field entry',34,'delete_fieldentry'),(91,'Can add Form',35,'add_form'),(92,'Can change Form',35,'change_form'),(93,'Can delete Form',35,'delete_form'),(94,'Can add Field',36,'add_field'),(95,'Can change Field',36,'change_field'),(96,'Can delete Field',36,'delete_field'),(97,'Can add Folder',37,'add_folder'),(98,'Can change Folder',37,'change_folder'),(99,'Can delete Folder',37,'delete_folder'),(100,'Can use directory listing',37,'can_use_directory_listing'),(101,'Can add folder permission',38,'add_folderpermission'),(102,'Can change folder permission',38,'change_folderpermission'),(103,'Can delete folder permission',38,'delete_folderpermission'),(104,'Can add file',39,'add_file'),(105,'Can change file',39,'change_file'),(106,'Can delete file',39,'delete_file'),(107,'Can add clipboard',40,'add_clipboard'),(108,'Can change clipboard',40,'change_clipboard'),(109,'Can delete clipboard',40,'delete_clipboard'),(110,'Can add clipboard item',41,'add_clipboarditem'),(111,'Can change clipboard item',41,'change_clipboarditem'),(112,'Can delete clipboard item',41,'delete_clipboarditem'),(113,'Can add image',42,'add_image'),(114,'Can change image',42,'change_image'),(115,'Can delete image',42,'delete_image'),(116,'Can add source',43,'add_source'),(117,'Can change source',43,'change_source'),(118,'Can delete source',43,'delete_source'),(119,'Can add thumbnail',44,'add_thumbnail'),(120,'Can change thumbnail',44,'change_thumbnail'),(121,'Can delete thumbnail',44,'delete_thumbnail'),(122,'Can add thumbnail dimensions',45,'add_thumbnaildimensions'),(123,'Can change thumbnail dimensions',45,'change_thumbnaildimensions'),(124,'Can delete thumbnail dimensions',45,'delete_thumbnaildimensions'),(125,'Can add Меню',27,'add_actiongroup'),(126,'Can change Меню',27,'change_actiongroup'),(127,'Can delete Меню',27,'delete_actiongroup'),(128,'Can add Элемент меню',28,'add_action'),(129,'Can change Элемент меню',28,'change_action'),(130,'Can delete Элемент меню',28,'delete_action'),(131,'Can add текстовая страница',29,'add_page'),(132,'Can change текстовая страница',29,'change_page'),(133,'Can delete текстовая страница',29,'delete_page'),(134,'Can add Новость',30,'add_news'),(135,'Can change Новость',30,'change_news'),(136,'Can delete Новость',30,'delete_news'),(137,'Can add слайд',46,'add_slider'),(138,'Can change слайд',46,'change_slider'),(139,'Can delete слайд',46,'delete_slider');
/*!40000 ALTER TABLE `auth_permission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user`
--

DROP TABLE IF EXISTS `auth_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `password` varchar(128) NOT NULL,
  `last_login` datetime NOT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `username` varchar(30) NOT NULL,
  `first_name` varchar(30) NOT NULL,
  `last_name` varchar(30) NOT NULL,
  `email` varchar(75) NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `date_joined` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user`
--

LOCK TABLES `auth_user` WRITE;
/*!40000 ALTER TABLE `auth_user` DISABLE KEYS */;
INSERT INTO `auth_user` VALUES (1,'pbkdf2_sha256$12000$ZiV4LlpUjURz$2K6b09ITPwwmzCNZ6EayBE52upVuR0xO2GJEUFZIBR0=','2014-06-04 19:28:07',1,'ak','','','akoptev1989@yandex.ru',1,1,'2014-05-29 13:46:33');
/*!40000 ALTER TABLE `auth_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user_groups`
--

DROP TABLE IF EXISTS `auth_user_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`,`group_id`),
  KEY `auth_user_groups_6340c63c` (`user_id`),
  KEY `auth_user_groups_5f412f9a` (`group_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user_groups`
--

LOCK TABLES `auth_user_groups` WRITE;
/*!40000 ALTER TABLE `auth_user_groups` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_user_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user_user_permissions`
--

DROP TABLE IF EXISTS `auth_user_user_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user_user_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`,`permission_id`),
  KEY `auth_user_user_permissions_6340c63c` (`user_id`),
  KEY `auth_user_user_permissions_83d7f98b` (`permission_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user_user_permissions`
--

LOCK TABLES `auth_user_user_permissions` WRITE;
/*!40000 ALTER TABLE `auth_user_user_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_user_user_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `device_apparat`
--

DROP TABLE IF EXISTS `device_apparat`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `device_apparat` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(300) NOT NULL,
  `short_d` longtext NOT NULL,
  `long_d` longtext NOT NULL,
  `power_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `device_apparat_7054f357` (`power_id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `device_apparat`
--

LOCK TABLES `device_apparat` WRITE;
/*!40000 ALTER TABLE `device_apparat` DISABLE KEYS */;
INSERT INTO `device_apparat` VALUES (6,'простенький','','',9),(5,'сложный аппарат','','',8),(4,'Простой аппарат','','',7),(7,'Крутой аппарат2','','',NULL);
/*!40000 ALTER TABLE `device_apparat` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `device_center`
--

DROP TABLE IF EXISTS `device_center`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `device_center` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(300) NOT NULL,
  `address` varchar(300) NOT NULL,
  `geox` double DEFAULT NULL,
  `geoy` double DEFAULT NULL,
  `active` tinyint(1) NOT NULL,
  `site` varchar(200) DEFAULT NULL,
  `info1` longtext NOT NULL,
  `info2` longtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `device_center`
--

LOCK TABLES `device_center` WRITE;
/*!40000 ALTER TABLE `device_center` DISABLE KEYS */;
INSERT INTO `device_center` VALUES (1,'Advanced Fertility Clinic','Санкт-Петербург',59.93933,30.324496,1,'','',''),(2,'test1','Москва,бла-бла',59.939093,30.323456,1,'','',''),(3,'test3','Home str',59.939093,30.315,1,'','','');
/*!40000 ALTER TABLE `device_center` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `device_center_metro`
--

DROP TABLE IF EXISTS `device_center_metro`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `device_center_metro` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `center_id` int(11) NOT NULL,
  `metro_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `center_id` (`center_id`,`metro_id`),
  KEY `device_center_metro_a827d88f` (`center_id`),
  KEY `device_center_metro_a422c896` (`metro_id`)
) ENGINE=MyISAM AUTO_INCREMENT=70 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `device_center_metro`
--

LOCK TABLES `device_center_metro` WRITE;
/*!40000 ALTER TABLE `device_center_metro` DISABLE KEYS */;
INSERT INTO `device_center_metro` VALUES (69,1,7),(67,1,8),(68,1,14),(64,2,5),(65,2,6),(66,2,21),(60,3,8),(61,3,5),(62,3,6),(63,3,7);
/*!40000 ALTER TABLE `device_center_metro` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `device_inspection`
--

DROP TABLE IF EXISTS `device_inspection`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `device_inspection` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(300) NOT NULL,
  `body` longtext NOT NULL,
  `active` tinyint(1) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `mrt` tinyint(1) NOT NULL,
  `kt` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `device_inspection_410d0aac` (`parent_id`)
) ENGINE=MyISAM AUTO_INCREMENT=85 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `device_inspection`
--

LOCK TABLES `device_inspection` WRITE;
/*!40000 ALTER TABLE `device_inspection` DISABLE KEYS */;
INSERT INTO `device_inspection` VALUES (1,'КТ головы','ри магнитно-резонансной томографии головы можно получить полную информацию о состоянии вещества головного мозга со всеми его внутренними структурами, крупных сосудов, мягких тканей, лор-органов (носоглотка, гортань, полость носа, околоносовые пазухи, среднее и внутреннее ухо), а также содержимого глазницы (глазное яблоко, жировая клетчатка, зрительный нерв, артерия и вена сетчатки). На снимках видна и верхняя часть шеи, поэтому МРТ этой зоны также визуализируе',1,NULL,0,1),(2,'КТ глазных орбит','бла-бла',1,1,0,1),(3,'КТ пазух носа','',1,1,0,1),(4,'МРТ головы','<li class=\"current\">Головного мозга</li>',1,NULL,1,0),(45,'КТ головного мозга','',1,1,0,1),(46,'КТ позвоночника','',1,NULL,0,1),(47,'КТ шейного отдела','',1,46,0,1),(48,'КТ грудного отдела','',1,46,0,1),(49,'КТ пояснично-крестцового отдела','',1,46,0,1),(50,'КТ крестцово-повздошных сочленений','',1,46,0,1),(51,'КТ копчиковой области','',1,46,0,1),(52,'КТ органов и мягких тканей','',1,NULL,0,1),(53,'КТ мягких тканей и органов шеи','',1,52,0,1),(54,'КТ органов грудной клетки и легких','',1,52,0,1),(55,'КТ-виртуальная бронхоскопия','',1,52,0,1),(6,'МРТ головного мозга','МРТ головного мозга описание',1,4,1,0),(7,'МРТ гипофиза','мрт гипофиза описание',1,4,1,0),(8,'МРТ глазных орбит','',1,4,1,0),(9,'МРТ пазух носа','',1,4,1,0),(10,'МРТ позвоночника','',1,NULL,1,0),(11,'МРТ шейного отдела','',1,10,1,0),(12,'МРТ грудного отдела','',1,10,1,0),(13,'МРТ пояснично-крестцового отдела','',1,10,1,0),(14,'МРТ крестцово-подвздошных сочленений','',1,10,1,0),(15,'МРТ копчиковой области','',1,10,1,0),(16,'МРТ внутренних органов','',1,NULL,1,0),(17,'МРТ органов грудной клетки и средостения','',1,16,1,0),(18,'МРТ молочной железы','',1,16,1,0),(19,'МРТ органов брюшной полости','',1,16,1,0),(20,'МРТ органов забрюшинного пространства','',1,16,1,0),(21,'МР-холангиография','',1,16,1,0),(22,'МРТ органов малого таза','',1,16,1,0),(23,'МРТ кишечника','',1,16,1,0),(24,'МРТ мягких тканей','',1,NULL,1,0),(25,'МРТ мягких тканей и органов шеи','',1,24,1,0),(26,'МРТ тканей конечности','',1,24,1,0),(27,'МРТ суставов','',1,NULL,1,0),(28,'МРТ височно-нижнечелюстного сустава','',1,27,1,0),(29,'МРТ плечевого сустава','',1,27,1,0),(30,'МРТ локтевого сустава','',1,27,1,0),(31,'МРТ лучезапястного сустава','',1,27,1,0),(32,'МРТ кисти','',1,27,1,0),(33,'МРТ тазобедренного сустава','',1,27,1,0),(34,'МРТ коленного сустава','',1,27,1,0),(35,'МРТ голеностопного сустава','',1,27,1,0),(36,'МРТ стопы','',1,27,1,0),(37,'МРТ сосудов','',1,NULL,1,0),(38,'МРТ сосудов головного мозга','',1,37,1,0),(39,'МРТ сосудов шеи','',1,37,1,0),(40,'МРТ подключичных артерий','',1,37,1,0),(41,'МРТ грудного отдела аорты','',1,37,1,0),(42,'МРТ брюшного отдела аорты','',1,37,1,0),(43,'МРТ сосудов малого таза','',1,37,1,0),(44,'МРТ сосудов конечностей','',1,37,1,0),(56,'КТ органов брюшной полости','',1,52,0,1),(57,'КТ органов забрюшинного пространства','',1,52,0,1),(58,'КТ органов малого таза','',1,52,0,1),(59,'КТ-виртуальная колоноскопия','',1,52,0,1),(60,'КТ суставов и костей','',1,NULL,0,1),(61,'КТ челюсти','',1,60,0,1),(62,'КТ плечевого сустава','',1,60,0,1),(63,'КТ ключицы','',1,60,0,1),(64,'КТ локтевого сустава','',1,60,0,1),(65,'КТ лучезапястного сустава','',1,60,0,1),(66,'КТ кисти','',1,60,0,1),(67,'КТ тазобедренного сустава','',1,60,0,1),(68,'КТ коленного сустава','',1,60,0,1),(69,'КТ голеностопного сустава','',1,60,0,1),(70,'КТ стопы','',1,60,0,1),(71,'КТ височных костей','',1,60,0,1),(72,'КТ костей лицевого черепа','',1,60,0,1),(73,'КТ плечевой кости','',1,60,0,1),(74,'КТ костей таза','',1,60,0,1),(75,'КТ бедренной кости','',1,60,0,1),(76,'КТ костей голени','',1,60,0,1),(77,'КТ сосудов','',1,NULL,0,1),(78,'КТ-ангиография сосудов головного мозга','',1,77,0,1),(79,'КТ-ангиография сосудов шеи','',1,77,0,1),(80,'КТ грудной аорты и легочной артерии','',1,77,0,1),(81,'КТ-коронарография','',1,77,0,1),(82,'КТ брюшной аорты и ее ветвей','',1,77,0,1),(83,'КТ-ангиография сосудов конечностей','',1,77,0,1),(84,'МРТ пальца','',1,4,1,0);
/*!40000 ALTER TABLE `device_inspection` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `device_metro`
--

DROP TABLE IF EXISTS `device_metro`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `device_metro` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(301) NOT NULL,
  `slug` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `slug` (`slug`)
) ENGINE=MyISAM AUTO_INCREMENT=70 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `device_metro`
--

LOCK TABLES `device_metro` WRITE;
/*!40000 ALTER TABLE `device_metro` DISABLE KEYS */;
INSERT INTO `device_metro` VALUES (7,'Чернышевская','chernyshevskaya'),(5,'Электросила','elektrosila'),(6,'Чкаловская','chkalovskaya'),(8,'Чёрная речка','chyornaya-rechka'),(9,'Фрунзенская','frunzenskaya'),(10,'Улица Дыбенко','ulica-dybenko'),(11,'Удельная','udelnaya'),(12,'Технологический институт','tehnologicheskij-institut'),(13,'Старая Деревня','staraya-derevnya'),(14,'Спортивная','sportivnaya'),(15,'Спасская','spasskaya'),(16,'Сенная площадь','sennaya-ploshad'),(17,'Садовая','sadovaya'),(18,'Рыбацкое','rybackoe'),(19,'Пушкинская','pushkinskaya'),(20,'Проспект Просвещения','prospekt-prosvesheniya'),(21,'Проспект Ветеранов','prospekt-veteranov'),(22,'Проспект Большевиков','prospekt-bolshevikov'),(23,'Пролетарская','proletarskaya'),(24,'Приморская','primorskaya'),(25,'Политехническая','politehnicheskaya'),(26,'Площадь Мужества','ploshad-muzhestva'),(27,'Площадь Ленина','ploshad-lenina'),(28,'Площадь Восстания','ploshad-vosstaniya'),(29,'Площадь Александра Невского','ploshad-aleksandra-nevskogo'),(30,'Пионерская','pionerskaya'),(31,'Петроградская','petrogradskaya'),(32,'Парнас','parnas'),(33,'Парк Победы','park-pobedy'),(34,'Озерки','ozerki'),(35,'Обухово','obuhovo'),(36,'Обводный канал','obvodnyj-kanal'),(37,'Новочеркасская','novocherkasskaya'),(38,'Невский проспект','nevskij-prospekt'),(39,'Нарвская','narvskaya'),(40,'Московские ворота','moskovskie-vorota'),(41,'Московская','moskovskaya'),(42,'Международная','mezhdunarodnaya'),(43,'Маяковская','mayakovskaya'),(44,'Ломоносовская','lomonosovskaya'),(45,'Лиговский проспект','ligovskij-prospekt'),(46,'Лесная','lesnaya'),(47,'Ленинский проспект','leninskij-prospekt'),(48,'Ладожская','ladozhskaya'),(49,'Купчино','kupchino'),(50,'Крестовский остров','krestovskij-ostrov'),(51,'Комендантский проспект','komendantskij-prospekt'),(52,'Кировский завод','kirovskij-zavod'),(53,'Звенигородская','zvenigorodskaya'),(54,'Звёздная','zvyozdnaya'),(55,'Елизаровская','elizarovskaya'),(56,'Достоевская','dostoevskaya'),(57,'Девяткино','devyatkino'),(58,'Гражданский проспект','grazhdanskij-prospekt'),(59,'Гостиный двор','gostinyj-dvor'),(60,'Горьковская','gorkovskaya'),(61,'Выборгская','vyborgskaya'),(62,'Волковская','volkovskaya'),(63,'Владимирская','vladimirskaya'),(64,'Василеостровская','vasileostrovskaya'),(65,'Бухарестская','buharestskaya'),(66,'Балтийская','baltijskaya'),(67,'Академическая','akademicheskaya'),(68,'Адмиралтейская','admiraltejskaya'),(69,'Автово','avtovo');
/*!40000 ALTER TABLE `device_metro` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `device_power`
--

DROP TABLE IF EXISTS `device_power`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `device_power` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `value` varchar(10) NOT NULL,
  `slice` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `device_power`
--

LOCK TABLES `device_power` WRITE;
/*!40000 ALTER TABLE `device_power` DISABLE KEYS */;
INSERT INTO `device_power` VALUES (9,'20',1),(8,'1',0),(7,'0.5',0);
/*!40000 ALTER TABLE `device_power` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `device_pricelist`
--

DROP TABLE IF EXISTS `device_pricelist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `device_pricelist` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `center_id` int(11) NOT NULL,
  `apparat_id` int(11) NOT NULL,
  `sale` varchar(20) DEFAULT NULL,
  `info` varchar(200) DEFAULT NULL,
  `inspection_id` int(11) NOT NULL,
  `value` varchar(200) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `device_pricelist_a827d88f` (`center_id`),
  KEY `device_pricelist_68ace618` (`apparat_id`),
  KEY `device_pricelist_c935a95b` (`inspection_id`)
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `device_pricelist`
--

LOCK TABLES `device_pricelist` WRITE;
/*!40000 ALTER TABLE `device_pricelist` DISABLE KEYS */;
INSERT INTO `device_pricelist` VALUES (13,3,7,'','',6,'23333'),(12,2,6,'','',2,'2444'),(11,1,5,'','',6,'5555'),(10,1,4,'','',6,'2444');
/*!40000 ALTER TABLE `device_pricelist` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_admin_log`
--

DROP TABLE IF EXISTS `django_admin_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_admin_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action_time` datetime NOT NULL,
  `user_id` int(11) NOT NULL,
  `content_type_id` int(11) DEFAULT NULL,
  `object_id` longtext,
  `object_repr` varchar(200) NOT NULL,
  `action_flag` smallint(5) unsigned NOT NULL,
  `change_message` longtext NOT NULL,
  PRIMARY KEY (`id`),
  KEY `django_admin_log_6340c63c` (`user_id`),
  KEY `django_admin_log_37ef4eb4` (`content_type_id`)
) ENGINE=MyISAM AUTO_INCREMENT=230 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_admin_log`
--

LOCK TABLES `django_admin_log` WRITE;
/*!40000 ALTER TABLE `django_admin_log` DISABLE KEYS */;
INSERT INTO `django_admin_log` VALUES (1,'2014-05-29 14:54:06',1,17,'1','Площадь Восстания',1,''),(2,'2014-05-29 14:54:36',1,18,'1','0.5',1,''),(3,'2014-05-29 14:54:37',1,19,'1','Обычный',1,''),(4,'2014-05-29 14:55:06',1,21,'1','КТ головы',1,''),(5,'2014-05-29 14:55:15',1,21,'2','КТ глаз',1,''),(6,'2014-05-29 14:56:04',1,20,'1','Advanced Fertility Clinic',1,''),(7,'2014-05-29 15:27:27',1,17,'2','Площадь Мужества',1,''),(8,'2014-05-29 15:28:03',1,18,'2','1',1,''),(9,'2014-05-29 15:28:04',1,19,'2','Крутой аппарат',1,''),(10,'2014-05-29 15:28:11',1,20,'2','test1',1,''),(11,'2014-05-29 15:30:28',1,21,'3','КТ носа',1,''),(12,'2014-05-29 15:30:32',1,20,'3','test3',1,''),(13,'2014-05-29 15:30:53',1,20,'3','test3',2,'Добавлен Ценник \"test3 - 3222\".'),(14,'2014-05-29 15:31:12',1,20,'3','test3',2,'Изменен metro.'),(15,'2014-05-29 16:04:47',1,20,'1','Advanced Fertility Clinic',2,'Изменены value для Ценник \"Advanced Fertility Clinic - с контрастом - 2800\".'),(16,'2014-05-29 16:08:29',1,20,'1','Advanced Fertility Clinic',2,'Изменены value для Ценник \"Advanced Fertility Clinic - с контрастом - 2800<br>без контраста - 3400\".'),(17,'2014-05-29 16:09:53',1,20,'1','Advanced Fertility Clinic',2,'Изменены value для Ценник \"Advanced Fertility Clinic - с контрастом - 2800р<br>без контраста - 3400\".'),(18,'2014-05-29 18:46:00',1,21,'1','КТ головы',2,'Изменен body.'),(19,'2014-05-29 18:46:18',1,21,'4','МРТ головы',1,''),(20,'2014-05-29 18:46:32',1,21,'5','МРТ ног',1,''),(21,'2014-05-29 18:49:15',1,21,'4','МРТ головы',2,'Изменен body.'),(22,'2014-05-30 01:30:48',1,14,'1','(TitleAndTextBlock) Текст вставлен через админку',2,'Изменен title и text.'),(23,'2014-05-30 01:33:37',1,12,'1','(TextBlock) <a href=\"tel:+781291...',2,'Изменен text.'),(24,'2014-05-30 12:39:51',1,17,'3','Автово',1,''),(25,'2014-05-30 12:39:55',1,17,'4','Адмиралтейская',1,''),(26,'2014-05-30 12:40:20',1,17,'4','Адмиралтейская',3,''),(27,'2014-05-30 12:40:20',1,17,'3','Автово',3,''),(28,'2014-05-30 12:40:28',1,17,'2','Площадь Мужества',3,''),(29,'2014-05-30 12:40:28',1,17,'1','Площадь Восстания',3,''),(30,'2014-05-30 12:40:34',1,17,'5','Электросила',1,''),(31,'2014-05-30 12:40:38',1,17,'6','Чкаловская',1,''),(32,'2014-05-30 12:40:43',1,17,'7','Чернышевская',1,''),(33,'2014-05-30 12:40:47',1,17,'8','Чёрная речка',1,''),(34,'2014-05-30 12:40:51',1,17,'9','Фрунзенская',1,''),(35,'2014-05-30 12:40:56',1,17,'10','Улица Дыбенко',1,''),(36,'2014-05-30 12:41:00',1,17,'11','Удельная',1,''),(37,'2014-05-30 12:41:03',1,17,'12','Технологический институт',1,''),(38,'2014-05-30 12:41:09',1,17,'13','Старая Деревня',1,''),(39,'2014-05-30 12:41:12',1,17,'14','Спортивная',1,''),(40,'2014-05-30 12:41:16',1,17,'15','Спасская',1,''),(41,'2014-05-30 12:41:19',1,17,'16','Сенная площадь',1,''),(42,'2014-05-30 12:41:23',1,17,'17','Садовая',1,''),(43,'2014-05-30 12:41:27',1,17,'18','Рыбацкое',1,''),(44,'2014-05-30 12:41:30',1,17,'19','Пушкинская',1,''),(45,'2014-05-30 12:41:33',1,17,'20','Проспект Просвещения',1,''),(46,'2014-05-30 12:41:36',1,17,'21','Проспект Ветеранов',1,''),(47,'2014-05-30 12:41:40',1,17,'22','Проспект Большевиков',1,''),(48,'2014-05-30 12:41:43',1,17,'23','Пролетарская',1,''),(49,'2014-05-30 12:41:47',1,17,'24','Приморская',1,''),(50,'2014-05-30 12:41:51',1,17,'25','Политехническая',1,''),(51,'2014-05-30 12:41:55',1,17,'26','Площадь Мужества',1,''),(52,'2014-05-30 12:41:59',1,17,'27','Площадь Ленина',1,''),(53,'2014-05-30 12:42:03',1,17,'28','Площадь Восстания',1,''),(54,'2014-05-30 12:42:10',1,17,'29','Площадь Александра Невского',1,''),(55,'2014-05-30 12:42:14',1,17,'30','Пионерская',1,''),(56,'2014-05-30 12:42:18',1,17,'31','Петроградская',1,''),(57,'2014-05-30 12:42:21',1,17,'32','Парнас',1,''),(58,'2014-05-30 12:42:34',1,17,'33','Парк Победы',1,''),(59,'2014-05-30 12:42:40',1,17,'34','Озерки',1,''),(60,'2014-05-30 12:42:43',1,17,'35','Обухово',1,''),(61,'2014-05-30 12:42:46',1,17,'36','Обводный канал',1,''),(62,'2014-05-30 12:42:50',1,17,'37','Новочеркасская',1,''),(63,'2014-05-30 12:42:54',1,17,'38','Невский проспект',1,''),(64,'2014-05-30 12:42:57',1,17,'39','Нарвская',1,''),(65,'2014-05-30 12:43:00',1,17,'40','Московские ворота',1,''),(66,'2014-05-30 12:43:04',1,17,'41','Московская',1,''),(67,'2014-05-30 12:43:09',1,17,'42','Международная',1,''),(68,'2014-05-30 12:43:12',1,17,'43','Маяковская',1,''),(69,'2014-05-30 12:43:15',1,17,'44','Ломоносовская',1,''),(70,'2014-05-30 12:43:19',1,17,'45','Лиговский проспект',1,''),(71,'2014-05-30 12:43:22',1,17,'46','Лесная',1,''),(72,'2014-05-30 12:43:26',1,17,'47','Ленинский проспект',1,''),(73,'2014-05-30 12:43:29',1,17,'48','Ладожская',1,''),(74,'2014-05-30 12:43:32',1,17,'49','Купчино',1,''),(75,'2014-05-30 12:43:35',1,17,'50','Крестовский остров',1,''),(76,'2014-05-30 12:43:39',1,17,'51','Комендантский проспект',1,''),(77,'2014-05-30 12:43:44',1,17,'52','Кировский завод',1,''),(78,'2014-05-30 12:43:48',1,17,'53','Звенигородская',1,''),(79,'2014-05-30 12:43:51',1,17,'54','Звёздная',1,''),(80,'2014-05-30 12:44:04',1,17,'55','Елизаровская',1,''),(81,'2014-05-30 12:44:08',1,17,'56','Достоевская',1,''),(82,'2014-05-30 12:44:11',1,17,'57','Девяткино',1,''),(83,'2014-05-30 12:44:14',1,17,'58','Гражданский проспект',1,''),(84,'2014-05-30 12:44:17',1,17,'59','Гостиный двор',1,''),(85,'2014-05-30 12:44:23',1,17,'60','Горьковская',1,''),(86,'2014-05-30 12:44:26',1,17,'61','Выборгская',1,''),(87,'2014-05-30 12:44:30',1,17,'62','Волковская',1,''),(88,'2014-05-30 12:44:33',1,17,'63','Владимирская',1,''),(89,'2014-05-30 12:44:37',1,17,'64','Василеостровская',1,''),(90,'2014-05-30 12:44:40',1,17,'65','Бухарестская',1,''),(91,'2014-05-30 12:44:43',1,17,'66','Балтийская',1,''),(92,'2014-05-30 12:44:47',1,17,'67','Академическая',1,''),(93,'2014-05-30 12:44:50',1,17,'68','Адмиралтейская',1,''),(94,'2014-05-30 12:44:53',1,17,'69','Автово',1,''),(95,'2014-05-30 12:46:21',1,20,'1','Advanced Fertility Clinic',2,'Изменен metro.'),(96,'2014-05-30 12:46:26',1,20,'2','test1',2,'Изменен metro.'),(97,'2014-05-30 12:46:32',1,20,'3','test3',2,'Изменен metro.'),(98,'2014-05-30 12:47:59',1,20,'2','test1',2,'Изменен metro.'),(99,'2014-05-30 12:48:15',1,20,'2','test1',2,'Изменен metro.'),(100,'2014-05-30 12:51:49',1,18,'3','3',1,''),(101,'2014-05-30 12:53:56',1,21,'6','МРТ головного мозга',1,''),(102,'2014-05-30 12:54:34',1,21,'7','МРТ гипофиза',1,''),(103,'2014-05-30 12:54:44',1,21,'8','МРТ глазных орбит',1,''),(104,'2014-05-30 12:54:53',1,21,'9','МРТ пазух носа',1,''),(105,'2014-05-30 12:55:08',1,21,'10','МРТ позвоночника',1,''),(106,'2014-05-30 12:55:19',1,21,'11','МРТ шейного отдела',1,''),(107,'2014-05-30 12:55:30',1,21,'12','МРТ грудного отдела',1,''),(108,'2014-05-30 12:55:41',1,21,'13','МРТ пояснично-крестцового отдела',1,''),(109,'2014-05-30 12:55:49',1,21,'14','МРТ крестцово-подвздошных сочленений',1,''),(110,'2014-05-30 12:55:57',1,21,'15','МРТ копчиковой области',1,''),(111,'2014-05-30 12:56:09',1,21,'16','МРТ внутренних органов',1,''),(112,'2014-05-30 12:56:19',1,21,'17','МРТ органов грудной клетки и средостения',1,''),(113,'2014-05-30 12:56:31',1,21,'18','МРТ молочной железы',1,''),(114,'2014-05-30 12:56:39',1,21,'19','МРТ органов брюшной полости',1,''),(115,'2014-05-30 12:56:47',1,21,'20','МРТ органов забрюшинного пространства',1,''),(116,'2014-05-30 12:56:55',1,21,'21','МР-холангиография',1,''),(117,'2014-05-30 12:57:02',1,21,'22','МРТ органов малого таза',1,''),(118,'2014-05-30 12:57:10',1,21,'23','МРТ кишечника',1,''),(119,'2014-05-30 12:57:24',1,21,'24','МРТ мягких тканей',1,''),(120,'2014-05-30 12:57:32',1,21,'24','МРТ мягких тканей',2,'Изменен parent.'),(121,'2014-05-30 12:57:50',1,21,'25','МРТ мягких тканей и органов шеи',1,''),(122,'2014-05-30 12:58:00',1,21,'26','МРТ тканей конечности',1,''),(123,'2014-05-30 12:58:07',1,21,'27','МРТ суставов',1,''),(124,'2014-05-30 12:58:15',1,21,'28','МРТ височно-нижнечелюстного сустава',1,''),(125,'2014-05-30 12:58:26',1,21,'29','МРТ плечевого сустава',1,''),(126,'2014-05-30 12:58:37',1,21,'30','МРТ локтевого сустава',1,''),(127,'2014-05-30 12:58:46',1,21,'31','МРТ лучезапястного сустава',1,''),(128,'2014-05-30 12:58:56',1,21,'32','МРТ кисти',1,''),(129,'2014-05-30 12:59:04',1,21,'33','МРТ тазобедренного сустава',1,''),(130,'2014-05-30 12:59:12',1,21,'34','МРТ коленного сустава',1,''),(131,'2014-05-30 12:59:21',1,21,'35','МРТ голеностопного сустава',1,''),(132,'2014-05-30 12:59:30',1,21,'36','МРТ стопы',1,''),(133,'2014-05-30 12:59:39',1,21,'37','МРТ сосудов',1,''),(134,'2014-05-30 12:59:49',1,21,'38','МРТ сосудов головного мозга',1,''),(135,'2014-05-30 12:59:57',1,21,'39','МРТ сосудов шеи',1,''),(136,'2014-05-30 13:00:10',1,21,'40','МРТ подключичных артерий',1,''),(137,'2014-05-30 13:00:20',1,21,'41','МРТ грудного отдела аорты',1,''),(138,'2014-05-30 13:00:30',1,21,'42','МРТ брюшного отдела аорты',1,''),(139,'2014-05-30 13:00:39',1,21,'43','МРТ сосудов малого таза',1,''),(140,'2014-05-30 13:00:48',1,21,'44','МРТ сосудов конечностей',1,''),(141,'2014-05-30 13:01:11',1,21,'5','МРТ ног',3,''),(142,'2014-05-30 13:04:38',1,21,'1','КТ головы',2,'Ни одно поле не изменено.'),(143,'2014-05-30 13:04:55',1,21,'2','КТ глазных орбит',2,'Изменен name.'),(144,'2014-05-30 13:05:22',1,21,'3','КТ пазух носа',2,'Изменен name.'),(145,'2014-05-30 13:05:44',1,21,'45','КТ головного мозга',1,''),(146,'2014-05-30 13:06:46',1,21,'46','КТ позвоночника',1,''),(147,'2014-05-30 13:06:57',1,21,'47','КТ шейного отдела',1,''),(148,'2014-05-30 13:07:06',1,21,'48','КТ грудного отдела',1,''),(149,'2014-05-30 13:07:15',1,21,'49','КТ пояснично-крестцового отдела',1,''),(150,'2014-05-30 13:07:23',1,21,'50','КТ крестцово-повздошных сочленений',1,''),(151,'2014-05-30 13:07:30',1,21,'51','КТ копчиковой области',1,''),(152,'2014-05-30 13:07:38',1,21,'52','КТ органов и мягких тканей',1,''),(153,'2014-05-30 13:07:49',1,21,'53','КТ мягких тканей и органов шеи',1,''),(154,'2014-05-30 13:07:57',1,21,'54','КТ органов грудной клетки и легких',1,''),(155,'2014-05-30 13:08:04',1,21,'55','КТ-виртуальная бронхоскопия',1,''),(156,'2014-05-30 13:08:11',1,21,'56','КТ органов брюшной полости',1,''),(157,'2014-05-30 13:08:18',1,21,'57','КТ органов забрюшинного пространства',1,''),(158,'2014-05-30 13:08:26',1,21,'58','КТ органов малого таза',1,''),(159,'2014-05-30 13:08:32',1,21,'59','КТ-виртуальная колоноскопия',1,''),(160,'2014-05-30 13:08:40',1,21,'60','КТ суставов и костей',1,''),(161,'2014-05-30 13:09:08',1,21,'61','КТ челюсти',1,''),(162,'2014-05-30 13:09:16',1,21,'62','КТ плечевого сустава',1,''),(163,'2014-05-30 13:09:25',1,21,'63','КТ ключицы',1,''),(164,'2014-05-30 13:09:33',1,21,'64','КТ локтевого сустава',1,''),(165,'2014-05-30 13:09:42',1,21,'65','КТ лучезапястного сустава',1,''),(166,'2014-05-30 13:09:52',1,21,'66','КТ кисти',1,''),(167,'2014-05-30 13:10:00',1,21,'67','КТ тазобедренного сустава',1,''),(168,'2014-05-30 13:10:07',1,21,'68','КТ коленного сустава',1,''),(169,'2014-05-30 13:10:16',1,21,'69','КТ голеностопного сустава',1,''),(170,'2014-05-30 13:10:27',1,21,'70','КТ стопы',1,''),(171,'2014-05-30 13:10:41',1,21,'71','КТ височных костей',1,''),(172,'2014-05-30 13:10:59',1,21,'72','КТ костей лицевого черепа',1,''),(173,'2014-05-30 13:11:08',1,21,'73','КТ плечевой кости',1,''),(174,'2014-05-30 13:11:18',1,21,'74','КТ костей таза',1,''),(175,'2014-05-30 13:11:30',1,21,'75','КТ бедренной кости',1,''),(176,'2014-05-30 13:11:39',1,21,'76','КТ костей голени',1,''),(177,'2014-05-30 13:11:44',1,21,'77','КТ сосудов',1,''),(178,'2014-05-30 13:11:53',1,21,'78','КТ-ангиография сосудов головного мозга',1,''),(179,'2014-05-30 13:12:02',1,21,'79','КТ-ангиография сосудов шеи',1,''),(180,'2014-05-30 13:12:11',1,21,'80','КТ грудной аорты и легочной артерии',1,''),(181,'2014-05-30 13:12:21',1,21,'81','КТ-коронарография',1,''),(182,'2014-05-30 13:12:32',1,21,'82','КТ брюшной аорты и ее ветвей',1,''),(183,'2014-05-30 13:12:41',1,21,'83','КТ-ангиография сосудов конечностей',1,''),(184,'2014-05-30 13:14:11',1,20,'1','Advanced Fertility Clinic',2,'Добавлен Ценник \"Advanced Fertility Clinic - 999999999999999999\".'),(185,'2014-05-30 13:14:41',1,20,'1','Advanced Fertility Clinic',2,'Изменены inspection для Ценник \"Advanced Fertility Clinic - 999999999999999999\".'),(186,'2014-05-30 13:43:31',1,20,'1','Advanced Fertility Clinic',2,'Изменены value для Ценник \"Advanced Fertility Clinic - 999999999999999999999999999998\".'),(187,'2014-05-30 15:10:17',1,20,'1','Advanced Fertility Clinic',2,'Добавлен Ценник \"Advanced Fertility Clinic - \".'),(188,'2014-05-30 15:10:30',1,20,'1','Advanced Fertility Clinic',2,'Изменены info для Ценник \"Advanced Fertility Clinic - \".'),(189,'2014-05-30 15:42:46',1,20,'1','Advanced Fertility Clinic',2,'Изменены value для Ценник \"Advanced Fertility Clinic - 9999999999999998\". Изменены value для Ценник \"Advanced Fertility Clinic - Скидка 30% 3500 \".'),(190,'2014-05-30 15:43:07',1,20,'1','Advanced Fertility Clinic',2,'Изменены info и value для Ценник \"Advanced Fertility Clinic -  3500 \".'),(191,'2014-05-30 18:26:24',1,18,'4','20',1,''),(192,'2014-05-30 18:29:21',1,18,'5','0.2',1,''),(193,'2014-05-31 03:19:04',1,20,'2','test1',2,'Добавлен Ценник \"test1 - 23453\".'),(194,'2014-05-31 03:20:19',1,21,'84','МРТ пальца',1,''),(195,'2014-05-31 10:35:29',1,30,'1','Главная',1,''),(196,'2014-05-31 10:43:52',1,30,'1','Главная',2,'Изменен body и short.'),(197,'2014-05-31 10:45:29',1,30,'2','Счастливые часы на МРТ!',1,''),(198,'2014-05-31 10:45:50',1,30,'3','Прием врача до и после МРТ со скидкой 25%! ',1,''),(199,'2014-05-31 21:35:33',1,30,'3','Прием врача до и после МРТ со скидкой 25%! ',2,'Изменен due_date.'),(200,'2014-05-31 21:38:25',1,30,'3','Прием врача до и после МРТ со скидкой 25%! ',2,'Ни одно поле не изменено.'),(201,'2014-05-31 21:42:53',1,30,'2','Счастливые часы на МРТ!',2,'Изменен due_date.'),(202,'2014-05-31 21:42:57',1,30,'1','Главная',2,'Изменен due_date.'),(203,'2014-06-01 15:08:28',1,21,'6','МРТ головного мозга',2,'Изменен body.'),(204,'2014-06-01 15:25:47',1,14,'2','(TitleAndTextBlock) Почему МРТ?',2,'Изменен title и text.'),(205,'2014-06-01 15:26:45',1,14,'3','(TitleAndTextBlock) Что покажет КТ?',2,'Изменен title и text.'),(206,'2014-06-04 21:08:06',1,18,'6','1.1',1,''),(207,'2014-06-04 21:08:08',1,19,'3','мега',1,''),(208,'2014-06-04 21:08:17',1,20,'2','test1',2,'Добавлен Ценник \"test1 - 666\".'),(209,'2014-06-04 21:09:21',1,20,'2','test1',2,'Изменены inspection для Ценник \"test1 - 666\".'),(210,'2014-06-04 22:27:45',1,21,'7','МРТ гипофиза',2,'Изменен body.'),(211,'2014-06-05 11:33:52',1,18,'6','1.1',3,''),(212,'2014-06-05 11:33:52',1,18,'5','0.2',3,''),(213,'2014-06-05 11:33:52',1,18,'4','20',3,''),(214,'2014-06-05 11:33:52',1,18,'3','3',3,''),(215,'2014-06-05 11:33:52',1,18,'2','1',3,''),(216,'2014-06-05 11:33:52',1,18,'1','0.5',3,''),(217,'2014-06-05 11:34:26',1,18,'7','0.5',1,''),(218,'2014-06-05 11:34:28',1,19,'4','Простой аппарат',1,''),(219,'2014-06-05 11:34:51',1,18,'8','1',1,''),(220,'2014-06-05 11:34:52',1,19,'5','сложный аппарат',1,''),(221,'2014-06-05 11:35:02',1,20,'1','Advanced Fertility Clinic',2,'Добавлен Ценник \"Advanced Fertility Clinic - 2444\". Добавлен Ценник \"Advanced Fertility Clinic - 5555\".'),(222,'2014-06-05 11:35:23',1,18,'9','20',1,''),(223,'2014-06-05 11:35:25',1,19,'6','простенький',1,''),(224,'2014-06-05 11:35:31',1,20,'2','test1',2,'Добавлен Ценник \"test1 - 2444\".'),(225,'2014-06-05 12:18:36',1,19,'7','Крутой аппарат2',1,''),(226,'2014-06-05 12:18:44',1,20,'3','test3',2,'Добавлен Ценник \"test3 - 23333\".'),(227,'2014-06-05 13:59:51',1,20,'3','test3',2,'Изменен geox и geoy.'),(228,'2014-06-05 14:00:20',1,20,'2','test1',2,'Изменен geox и geoy.'),(229,'2014-06-05 14:32:29',1,20,'1','Advanced Fertility Clinic',2,'Изменен geox и geoy.');
/*!40000 ALTER TABLE `django_admin_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_content_type`
--

DROP TABLE IF EXISTS `django_content_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_content_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `app_label` varchar(100) NOT NULL,
  `model` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `app_label` (`app_label`,`model`)
) ENGINE=MyISAM AUTO_INCREMENT=47 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_content_type`
--

LOCK TABLES `django_content_type` WRITE;
/*!40000 ALTER TABLE `django_content_type` DISABLE KEYS */;
INSERT INTO `django_content_type` VALUES (1,'log entry','admin','logentry'),(2,'permission','auth','permission'),(3,'group','auth','group'),(4,'user','auth','user'),(5,'content type','contenttypes','contenttype'),(6,'session','sessions','session'),(7,'site','sites','site'),(8,'kv store','thumbnail','kvstore'),(9,'migration history','south','migrationhistory'),(10,'generic flatblock','django_generic_flatblocks','genericflatblock'),(11,'title','gblocks','title'),(12,'text','gblocks','text'),(13,'image','gblocks','image'),(14,'title and text','gblocks','titleandtext'),(15,'title text and image','gblocks','titletextandimage'),(16,'banner','gblocks','banner'),(17,'Метро','device','metro'),(18,'Мощность','device','power'),(19,'Вид аппарата','device','apparat'),(20,'Исследовательский центр','device','center'),(21,'Вид обследования','device','inspection'),(22,'Цена','device','pricelist'),(23,'SEO (Path)','seo','seometadatapath'),(24,'SEO (Model Instance)','seo','seometadatamodelinstance'),(25,'SEO (Model)','seo','seometadatamodel'),(26,'SEO (View)','seo','seometadataview'),(27,'Меню','page','actiongroup'),(28,'Элемент меню','page','action'),(29,'текстовая страница','page','page'),(30,'Новость','news','news'),(31,'bookmark','menu','bookmark'),(32,'dashboard preferences','dashboard','dashboardpreferences'),(33,'Form entry','forms','formentry'),(34,'Form field entry','forms','fieldentry'),(35,'Form','forms','form'),(36,'Field','forms','field'),(37,'Folder','filer','folder'),(38,'folder permission','filer','folderpermission'),(39,'file','filer','file'),(40,'clipboard','filer','clipboard'),(41,'clipboard item','filer','clipboarditem'),(42,'image','filer','image'),(43,'source','easy_thumbnails','source'),(44,'thumbnail','easy_thumbnails','thumbnail'),(45,'thumbnail dimensions','easy_thumbnails','thumbnaildimensions'),(46,'слайд','slider','slider');
/*!40000 ALTER TABLE `django_content_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_generic_flatblocks_genericflatblock`
--

DROP TABLE IF EXISTS `django_generic_flatblocks_genericflatblock`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_generic_flatblocks_genericflatblock` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `slug` varchar(255) NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `object_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `slug` (`slug`),
  KEY `django_generic_flatblocks_genericflatblock_37ef4eb4` (`content_type_id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_generic_flatblocks_genericflatblock`
--

LOCK TABLES `django_generic_flatblocks_genericflatblock` WRITE;
/*!40000 ALTER TABLE `django_generic_flatblocks_genericflatblock` DISABLE KEYS */;
INSERT INTO `django_generic_flatblocks_genericflatblock` VALUES (1,'help',14,1),(2,'phone',12,1),(3,'about_mrt',14,2),(4,'about_kt',14,3);
/*!40000 ALTER TABLE `django_generic_flatblocks_genericflatblock` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_session`
--

DROP TABLE IF EXISTS `django_session`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_session` (
  `session_key` varchar(40) NOT NULL,
  `session_data` longtext NOT NULL,
  `expire_date` datetime NOT NULL,
  PRIMARY KEY (`session_key`),
  KEY `django_session_b7b81f0c` (`expire_date`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_session`
--

LOCK TABLES `django_session` WRITE;
/*!40000 ALTER TABLE `django_session` DISABLE KEYS */;
INSERT INTO `django_session` VALUES ('f7ubtaxmahg8jgrjorlzh4zj4dsponow','NTdkMDU5ZDI1ZDczYWMzZDkzYzlkNmE3YTE3MzQyYWIxOTlhODc4ZTp7InNvcnQiOiItY3JlYXRpb25fZGF0ZSIsIl9tZXNzYWdlcyI6IltbXCJfX2pzb25fbWVzc2FnZVwiLDAsMjUsXCJcXHUwNDEyXFx1MDQzOFxcdTA0MzQgXFx1MDQzZVxcdTA0MzFcXHUwNDQxXFx1MDQzYlxcdTA0MzVcXHUwNDM0XFx1MDQzZVxcdTA0MzJcXHUwNDMwXFx1MDQzZFxcdTA0MzhcXHUwNDRmIFxcXCJcXHUwNDFjXFx1MDQyMFxcdTA0MjIgXFx1MDQzM1xcdTA0M2VcXHUwNDNiXFx1MDQzZVxcdTA0MzJcXHUwNDNkXFx1MDQzZVxcdTA0MzNcXHUwNDNlIFxcdTA0M2NcXHUwNDNlXFx1MDQzN1xcdTA0MzNcXHUwNDMwXFxcIiBcXHUwNDMxXFx1MDQ0YlxcdTA0M2IgXFx1MDQ0M1xcdTA0NDFcXHUwNDNmXFx1MDQzNVxcdTA0NDhcXHUwNDNkXFx1MDQzZSBcXHUwNDM4XFx1MDQzN1xcdTA0M2NcXHUwNDM1XFx1MDQzZFxcdTA0MzVcXHUwNDNkLlwiXSxbXCJfX2pzb25fbWVzc2FnZVwiLDAsMjUsXCJ0aXRsZSBhbmQgdGV4dCBcXFwiKFRpdGxlQW5kVGV4dEJsb2NrKSBcXHUwNDFmXFx1MDQzZVxcdTA0NDdcXHUwNDM1XFx1MDQzY1xcdTA0NDMgXFx1MDQxY1xcdTA0MjBcXHUwNDIyP1xcXCIgXFx1MDQzMVxcdTA0NGJcXHUwNDNiIFxcdTA0NDNcXHUwNDQxXFx1MDQzZlxcdTA0MzVcXHUwNDQ4XFx1MDQzZFxcdTA0M2UgXFx1MDQzOFxcdTA0MzdcXHUwNDNjXFx1MDQzNVxcdTA0M2RcXHUwNDM1XFx1MDQzZC5cIl0sW1wiX19qc29uX21lc3NhZ2VcIiwwLDI1LFwidGl0bGUgYW5kIHRleHQgXFxcIihUaXRsZUFuZFRleHRCbG9jaykgXFx1MDQyN1xcdTA0NDJcXHUwNDNlIFxcdTA0M2ZcXHUwNDNlXFx1MDQzYVxcdTA0MzBcXHUwNDM2XFx1MDQzNVxcdTA0NDIgXFx1MDQxYVxcdTA0MjI/XFxcIiBcXHUwNDMxXFx1MDQ0YlxcdTA0M2IgXFx1MDQ0M1xcdTA0NDFcXHUwNDNmXFx1MDQzNVxcdTA0NDhcXHUwNDNkXFx1MDQzZSBcXHUwNDM4XFx1MDQzN1xcdTA0M2NcXHUwNDM1XFx1MDQzZFxcdTA0MzVcXHUwNDNkLlwiXV0iLCJfYXV0aF91c2VyX2lkIjoxLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCJ9','2014-06-15 17:01:19'),('93p142eq59dwnay3jcksk12b75t2bl63','ZmM3ZTNjYjdmNzNiZDNkYzcwOGYwNzljYmNiNGRjOTVkOTkxOGM2ZTp7InNvcnQiOiItY3JlYXRpb25fZGF0ZSIsIl9tZXNzYWdlcyI6IltbXCJfX2pzb25fbWVzc2FnZVwiLDAsMjUsXCJcXHUwNDE4XFx1MDQ0MVxcdTA0NDFcXHUwNDNiXFx1MDQzNVxcdTA0MzRcXHUwNDNlXFx1MDQzMlxcdTA0MzBcXHUwNDQyXFx1MDQzNVxcdTA0M2JcXHUwNDRjXFx1MDQ0MVxcdTA0M2FcXHUwNDM4XFx1MDQzOSBcXHUwNDQ2XFx1MDQzNVxcdTA0M2RcXHUwNDQyXFx1MDQ0MCBcXFwidGVzdDFcXFwiIFxcdTA0MzFcXHUwNDRiXFx1MDQzYiBcXHUwNDQzXFx1MDQ0MVxcdTA0M2ZcXHUwNDM1XFx1MDQ0OFxcdTA0M2RcXHUwNDNlIFxcdTA0MzhcXHUwNDM3XFx1MDQzY1xcdTA0MzVcXHUwNDNkXFx1MDQzNVxcdTA0M2QuXCJdLFtcIl9fanNvbl9tZXNzYWdlXCIsMCwyNSxcIlxcdTA0MThcXHUwNDQxXFx1MDQ0MVxcdTA0M2JcXHUwNDM1XFx1MDQzNFxcdTA0M2VcXHUwNDMyXFx1MDQzMFxcdTA0NDJcXHUwNDM1XFx1MDQzYlxcdTA0NGNcXHUwNDQxXFx1MDQzYVxcdTA0MzhcXHUwNDM5IFxcdTA0NDZcXHUwNDM1XFx1MDQzZFxcdTA0NDJcXHUwNDQwIFxcXCJ0ZXN0M1xcXCIgXFx1MDQzMVxcdTA0NGJcXHUwNDNiIFxcdTA0NDNcXHUwNDQxXFx1MDQzZlxcdTA0MzVcXHUwNDQ4XFx1MDQzZFxcdTA0M2UgXFx1MDQzOFxcdTA0MzdcXHUwNDNjXFx1MDQzNVxcdTA0M2RcXHUwNDM1XFx1MDQzZC5cIl0sW1wiX19qc29uX21lc3NhZ2VcIiwwLDI1LFwiXFx1MDQxOFxcdTA0NDFcXHUwNDQxXFx1MDQzYlxcdTA0MzVcXHUwNDM0XFx1MDQzZVxcdTA0MzJcXHUwNDMwXFx1MDQ0MlxcdTA0MzVcXHUwNDNiXFx1MDQ0Y1xcdTA0NDFcXHUwNDNhXFx1MDQzOFxcdTA0MzkgXFx1MDQ0NlxcdTA0MzVcXHUwNDNkXFx1MDQ0MlxcdTA0NDAgXFxcInRlc3QzXFxcIiBcXHUwNDQzXFx1MDQ0MVxcdTA0M2ZcXHUwNDM1XFx1MDQ0OFxcdTA0M2RcXHUwNDNlIFxcdTA0MzhcXHUwNDM3XFx1MDQzY1xcdTA0MzVcXHUwNDNkXFx1MDQzNVxcdTA0M2QuIFxcdTA0MWRcXHUwNDM4XFx1MDQzNlxcdTA0MzUgXFx1MDQzMlxcdTA0NGIgXFx1MDQzY1xcdTA0M2VcXHUwNDM2XFx1MDQzNVxcdTA0NDJcXHUwNDM1IFxcdTA0NDBcXHUwNDM1XFx1MDQzNFxcdTA0MzBcXHUwNDNhXFx1MDQ0MlxcdTA0MzhcXHUwNDQwXFx1MDQzZVxcdTA0MzJcXHUwNDMwXFx1MDQ0MlxcdTA0NGMgXFx1MDQ0MVxcdTA0M2RcXHUwNDNlXFx1MDQzMlxcdTA0MzAuXCJdLFtcIl9fanNvbl9tZXNzYWdlXCIsMCwyNSxcIlxcdTA0MThcXHUwNDQxXFx1MDQ0MVxcdTA0M2JcXHUwNDM1XFx1MDQzNFxcdTA0M2VcXHUwNDMyXFx1MDQzMFxcdTA0NDJcXHUwNDM1XFx1MDQzYlxcdTA0NGNcXHUwNDQxXFx1MDQzYVxcdTA0MzhcXHUwNDM5IFxcdTA0NDZcXHUwNDM1XFx1MDQzZFxcdTA0NDJcXHUwNDQwIFxcXCJ0ZXN0MVxcXCIgXFx1MDQzMVxcdTA0NGJcXHUwNDNiIFxcdTA0NDNcXHUwNDQxXFx1MDQzZlxcdTA0MzVcXHUwNDQ4XFx1MDQzZFxcdTA0M2UgXFx1MDQzOFxcdTA0MzdcXHUwNDNjXFx1MDQzNVxcdTA0M2RcXHUwNDM1XFx1MDQzZC5cIl0sW1wiX19qc29uX21lc3NhZ2VcIiwwLDI1LFwiXFx1MDQxOFxcdTA0NDFcXHUwNDQxXFx1MDQzYlxcdTA0MzVcXHUwNDM0XFx1MDQzZVxcdTA0MzJcXHUwNDMwXFx1MDQ0MlxcdTA0MzVcXHUwNDNiXFx1MDQ0Y1xcdTA0NDFcXHUwNDNhXFx1MDQzOFxcdTA0MzkgXFx1MDQ0NlxcdTA0MzVcXHUwNDNkXFx1MDQ0MlxcdTA0NDAgXFxcIkFkdmFuY2VkIEZlcnRpbGl0eSBDbGluaWNcXFwiIFxcdTA0MzFcXHUwNDRiXFx1MDQzYiBcXHUwNDQzXFx1MDQ0MVxcdTA0M2ZcXHUwNDM1XFx1MDQ0OFxcdTA0M2RcXHUwNDNlIFxcdTA0MzhcXHUwNDM3XFx1MDQzY1xcdTA0MzVcXHUwNDNkXFx1MDQzNVxcdTA0M2QuXCJdXSIsIl9hdXRoX3VzZXJfaWQiOjEsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=','2014-06-19 14:32:29'),('9th19wjhm6n4bdi72ps73kyc8jvhy54p','N2M0YzJiYzc1MzFmYmFkZGZhMTkwNGRkMzZiZWRkN2Q0ZmFhNTBlNzp7Il9tZXNzYWdlcyI6IltbXCJfX2pzb25fbWVzc2FnZVwiLDAsMjUsXCJcXHUwNDFjXFx1MDQzZVxcdTA0NDlcXHUwNDNkXFx1MDQzZVxcdTA0NDFcXHUwNDQyXFx1MDQ0YyBcXFwiMC4yXFxcIiBcXHUwNDMxXFx1MDQ0YlxcdTA0M2IgXFx1MDQ0M1xcdTA0NDFcXHUwNDNmXFx1MDQzNVxcdTA0NDhcXHUwNDNkXFx1MDQzZSBcXHUwNDM0XFx1MDQzZVxcdTA0MzFcXHUwNDMwXFx1MDQzMlxcdTA0M2JcXHUwNDM1XFx1MDQzZC5cIl1dIiwiX2F1dGhfdXNlcl9pZCI6MSwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQifQ==','2014-06-13 18:29:21'),('6ua6nr53q2wjhwpey97tknlaeuxt1f0o','M2EyMGUwNTI4ZWFhN2NkMmM5NDg3M2U0NTg0MDg5MDg3ZThkYmEyYjp7ImZpbGVyX2xhc3RfZm9sZGVyX2lkIjpudWxsLCJfYXV0aF91c2VyX2lkIjoxLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCJ9','2014-06-13 06:48:33'),('xlsd5nxsoj4028ysoye21og1vqedbdj9','ODE0YTIyZjQ4OGZlOWUyZmY1NjE2ZWM1NjZjNGFiMGU2ZjIzOTg1NDp7Il9tZXNzYWdlcyI6IltbXCJfX2pzb25fbWVzc2FnZVwiLDAsMjUsXCJcXHUwNDIzXFx1MDQ0MVxcdTA0M2ZcXHUwNDM1XFx1MDQ0OFxcdTA0M2RcXHUwNDNlIFxcdTA0NDNcXHUwNDM0XFx1MDQzMFxcdTA0M2JcXHUwNDM1XFx1MDQzZFxcdTA0NGIgMiBcXHUwNDFjXFx1MDQzNVxcdTA0NDJcXHUwNDQwXFx1MDQzZS5cIl0sW1wiX19qc29uX21lc3NhZ2VcIiwwLDI1LFwiXFx1MDQxY1xcdTA0MzVcXHUwNDQyXFx1MDQ0MFxcdTA0M2UgXFxcIlxcdTA0MmRcXHUwNDNiXFx1MDQzNVxcdTA0M2FcXHUwNDQyXFx1MDQ0MFxcdTA0M2VcXHUwNDQxXFx1MDQzOFxcdTA0M2JcXHUwNDMwXFxcIiBcXHUwNDMxXFx1MDQ0YlxcdTA0M2IgXFx1MDQ0M1xcdTA0NDFcXHUwNDNmXFx1MDQzNVxcdTA0NDhcXHUwNDNkXFx1MDQzZSBcXHUwNDM0XFx1MDQzZVxcdTA0MzFcXHUwNDMwXFx1MDQzMlxcdTA0M2JcXHUwNDM1XFx1MDQzZC5cIl0sW1wiX19qc29uX21lc3NhZ2VcIiwwLDI1LFwiXFx1MDQxY1xcdTA0MzVcXHUwNDQyXFx1MDQ0MFxcdTA0M2UgXFxcIlxcdTA0MjdcXHUwNDNhXFx1MDQzMFxcdTA0M2JcXHUwNDNlXFx1MDQzMlxcdTA0NDFcXHUwNDNhXFx1MDQzMFxcdTA0NGZcXFwiIFxcdTA0MzFcXHUwNDRiXFx1MDQzYiBcXHUwNDQzXFx1MDQ0MVxcdTA0M2ZcXHUwNDM1XFx1MDQ0OFxcdTA0M2RcXHUwNDNlIFxcdTA0MzRcXHUwNDNlXFx1MDQzMVxcdTA0MzBcXHUwNDMyXFx1MDQzYlxcdTA0MzVcXHUwNDNkLlwiXSxbXCJfX2pzb25fbWVzc2FnZVwiLDAsMjUsXCJcXHUwNDFjXFx1MDQzNVxcdTA0NDJcXHUwNDQwXFx1MDQzZSBcXFwiXFx1MDQyN1xcdTA0MzVcXHUwNDQwXFx1MDQzZFxcdTA0NGJcXHUwNDQ4XFx1MDQzNVxcdTA0MzJcXHUwNDQxXFx1MDQzYVxcdTA0MzBcXHUwNDRmXFxcIiBcXHUwNDMxXFx1MDQ0YlxcdTA0M2IgXFx1MDQ0M1xcdTA0NDFcXHUwNDNmXFx1MDQzNVxcdTA0NDhcXHUwNDNkXFx1MDQzZSBcXHUwNDM0XFx1MDQzZVxcdTA0MzFcXHUwNDMwXFx1MDQzMlxcdTA0M2JcXHUwNDM1XFx1MDQzZC5cIl0sW1wiX19qc29uX21lc3NhZ2VcIiwwLDI1LFwiXFx1MDQxY1xcdTA0MzVcXHUwNDQyXFx1MDQ0MFxcdTA0M2UgXFxcIlxcdTA0MjdcXHUwNDUxXFx1MDQ0MFxcdTA0M2RcXHUwNDMwXFx1MDQ0ZiBcXHUwNDQwXFx1MDQzNVxcdTA0NDdcXHUwNDNhXFx1MDQzMFxcXCIgXFx1MDQzMVxcdTA0NGJcXHUwNDNiIFxcdTA0NDNcXHUwNDQxXFx1MDQzZlxcdTA0MzVcXHUwNDQ4XFx1MDQzZFxcdTA0M2UgXFx1MDQzNFxcdTA0M2VcXHUwNDMxXFx1MDQzMFxcdTA0MzJcXHUwNDNiXFx1MDQzNVxcdTA0M2QuXCJdLFtcIl9fanNvbl9tZXNzYWdlXCIsMCwyNSxcIlxcdTA0MWNcXHUwNDM1XFx1MDQ0MlxcdTA0NDBcXHUwNDNlIFxcXCJcXHUwNDI0XFx1MDQ0MFxcdTA0NDNcXHUwNDNkXFx1MDQzN1xcdTA0MzVcXHUwNDNkXFx1MDQ0MVxcdTA0M2FcXHUwNDMwXFx1MDQ0ZlxcXCIgXFx1MDQzMVxcdTA0NGJcXHUwNDNiIFxcdTA0NDNcXHUwNDQxXFx1MDQzZlxcdTA0MzVcXHUwNDQ4XFx1MDQzZFxcdTA0M2UgXFx1MDQzNFxcdTA0M2VcXHUwNDMxXFx1MDQzMFxcdTA0MzJcXHUwNDNiXFx1MDQzNVxcdTA0M2QuXCJdLFtcIl9fanNvbl9tZXNzYWdlXCIsMCwyNSxcIlxcdTA0MWNcXHUwNDM1XFx1MDQ0MlxcdTA0NDBcXHUwNDNlIFxcXCJcXHUwNDIzXFx1MDQzYlxcdTA0MzhcXHUwNDQ2XFx1MDQzMCBcXHUwNDE0XFx1MDQ0YlxcdTA0MzFcXHUwNDM1XFx1MDQzZFxcdTA0M2FcXHUwNDNlXFxcIiBcXHUwNDMxXFx1MDQ0YlxcdTA0M2IgXFx1MDQ0M1xcdTA0NDFcXHUwNDNmXFx1MDQzNVxcdTA0NDhcXHUwNDNkXFx1MDQzZSBcXHUwNDM0XFx1MDQzZVxcdTA0MzFcXHUwNDMwXFx1MDQzMlxcdTA0M2JcXHUwNDM1XFx1MDQzZC5cIl0sW1wiX19qc29uX21lc3NhZ2VcIiwwLDI1LFwiXFx1MDQxY1xcdTA0MzVcXHUwNDQyXFx1MDQ0MFxcdTA0M2UgXFxcIlxcdTA0MjNcXHUwNDM0XFx1MDQzNVxcdTA0M2JcXHUwNDRjXFx1MDQzZFxcdTA0MzBcXHUwNDRmXFxcIiBcXHUwNDMxXFx1MDQ0YlxcdTA0M2IgXFx1MDQ0M1xcdTA0NDFcXHUwNDNmXFx1MDQzNVxcdTA0NDhcXHUwNDNkXFx1MDQzZSBcXHUwNDM0XFx1MDQzZVxcdTA0MzFcXHUwNDMwXFx1MDQzMlxcdTA0M2JcXHUwNDM1XFx1MDQzZC5cIl0sW1wiX19qc29uX21lc3NhZ2VcIiwwLDI1LFwiXFx1MDQxY1xcdTA0MzVcXHUwNDQyXFx1MDQ0MFxcdTA0M2UgXFxcIlxcdTA0MjJcXHUwNDM1XFx1MDQ0NVxcdTA0M2RcXHUwNDNlXFx1MDQzYlxcdTA0M2VcXHUwNDMzXFx1MDQzOFxcdTA0NDdcXHUwNDM1XFx1MDQ0MVxcdTA0M2FcXHUwNDM4XFx1MDQzOSBcXHUwNDM4XFx1MDQzZFxcdTA0NDFcXHUwNDQyXFx1MDQzOFxcdTA0NDJcXHUwNDQzXFx1MDQ0MlxcXCIgXFx1MDQzMVxcdTA0NGJcXHUwNDNiIFxcdTA0NDNcXHUwNDQxXFx1MDQzZlxcdTA0MzVcXHUwNDQ4XFx1MDQzZFxcdTA0M2UgXFx1MDQzNFxcdTA0M2VcXHUwNDMxXFx1MDQzMFxcdTA0MzJcXHUwNDNiXFx1MDQzNVxcdTA0M2QuXCJdLFtcIl9fanNvbl9tZXNzYWdlXCIsMCwyNSxcIlxcdTA0MWNcXHUwNDM1XFx1MDQ0MlxcdTA0NDBcXHUwNDNlIFxcXCJcXHUwNDIxXFx1MDQ0MlxcdTA0MzBcXHUwNDQwXFx1MDQzMFxcdTA0NGYgXFx1MDQxNFxcdTA0MzVcXHUwNDQwXFx1MDQzNVxcdTA0MzJcXHUwNDNkXFx1MDQ0ZlxcXCIgXFx1MDQzMVxcdTA0NGJcXHUwNDNiIFxcdTA0NDNcXHUwNDQxXFx1MDQzZlxcdTA0MzVcXHUwNDQ4XFx1MDQzZFxcdTA0M2UgXFx1MDQzNFxcdTA0M2VcXHUwNDMxXFx1MDQzMFxcdTA0MzJcXHUwNDNiXFx1MDQzNVxcdTA0M2QuXCJdLFtcIl9fanNvbl9tZXNzYWdlXCIsMCwyNSxcIlxcdTA0MWNcXHUwNDM1XFx1MDQ0MlxcdTA0NDBcXHUwNDNlIFxcXCJcXHUwNDIxXFx1MDQzZlxcdTA0M2VcXHUwNDQwXFx1MDQ0MlxcdTA0MzhcXHUwNDMyXFx1MDQzZFxcdTA0MzBcXHUwNDRmXFxcIiBcXHUwNDMxXFx1MDQ0YlxcdTA0M2IgXFx1MDQ0M1xcdTA0NDFcXHUwNDNmXFx1MDQzNVxcdTA0NDhcXHUwNDNkXFx1MDQzZSBcXHUwNDM0XFx1MDQzZVxcdTA0MzFcXHUwNDMwXFx1MDQzMlxcdTA0M2JcXHUwNDM1XFx1MDQzZC5cIl0sW1wiX19qc29uX21lc3NhZ2VcIiwwLDI1LFwiXFx1MDQxY1xcdTA0MzVcXHUwNDQyXFx1MDQ0MFxcdTA0M2UgXFxcIlxcdTA0MjFcXHUwNDNmXFx1MDQzMFxcdTA0NDFcXHUwNDQxXFx1MDQzYVxcdTA0MzBcXHUwNDRmXFxcIiBcXHUwNDMxXFx1MDQ0YlxcdTA0M2IgXFx1MDQ0M1xcdTA0NDFcXHUwNDNmXFx1MDQzNVxcdTA0NDhcXHUwNDNkXFx1MDQzZSBcXHUwNDM0XFx1MDQzZVxcdTA0MzFcXHUwNDMwXFx1MDQzMlxcdTA0M2JcXHUwNDM1XFx1MDQzZC5cIl0sW1wiX19qc29uX21lc3NhZ2VcIiwwLDI1LFwiXFx1MDQxY1xcdTA0MzVcXHUwNDQyXFx1MDQ0MFxcdTA0M2UgXFxcIlxcdTA0MjFcXHUwNDM1XFx1MDQzZFxcdTA0M2RcXHUwNDMwXFx1MDQ0ZiBcXHUwNDNmXFx1MDQzYlxcdTA0M2VcXHUwNDQ5XFx1MDQzMFxcdTA0MzRcXHUwNDRjXFxcIiBcXHUwNDMxXFx1MDQ0YlxcdTA0M2IgXFx1MDQ0M1xcdTA0NDFcXHUwNDNmXFx1MDQzNVxcdTA0NDhcXHUwNDNkXFx1MDQzZSBcXHUwNDM0XFx1MDQzZVxcdTA0MzFcXHUwNDMwXFx1MDQzMlxcdTA0M2JcXHUwNDM1XFx1MDQzZC5cIl0sW1wiX19qc29uX21lc3NhZ2VcIiwwLDI1LFwiXFx1MDQxY1xcdTA0MzVcXHUwNDQyXFx1MDQ0MFxcdTA0M2UgXFxcIlxcdTA0MjFcXHUwNDMwXFx1MDQzNFxcdTA0M2VcXHUwNDMyXFx1MDQzMFxcdTA0NGZcXFwiIFxcdTA0MzFcXHUwNDRiXFx1MDQzYiBcXHUwNDQzXFx1MDQ0MVxcdTA0M2ZcXHUwNDM1XFx1MDQ0OFxcdTA0M2RcXHUwNDNlIFxcdTA0MzRcXHUwNDNlXFx1MDQzMVxcdTA0MzBcXHUwNDMyXFx1MDQzYlxcdTA0MzVcXHUwNDNkLlwiXSxbXCJfX2pzb25fbWVzc2FnZVwiLDAsMjUsXCJcXHUwNDFjXFx1MDQzNVxcdTA0NDJcXHUwNDQwXFx1MDQzZSBcXFwiXFx1MDQyMFxcdTA0NGJcXHUwNDMxXFx1MDQzMFxcdTA0NDZcXHUwNDNhXFx1MDQzZVxcdTA0MzVcXFwiIFxcdTA0MzFcXHUwNDRiXFx1MDQzYiBcXHUwNDQzXFx1MDQ0MVxcdTA0M2ZcXHUwNDM1XFx1MDQ0OFxcdTA0M2RcXHUwNDNlIFxcdTA0MzRcXHUwNDNlXFx1MDQzMVxcdTA0MzBcXHUwNDMyXFx1MDQzYlxcdTA0MzVcXHUwNDNkLlwiXSxbXCJfX2pzb25fbWVzc2FnZVwiLDAsMjUsXCJcXHUwNDFjXFx1MDQzNVxcdTA0NDJcXHUwNDQwXFx1MDQzZSBcXFwiXFx1MDQxZlxcdTA0NDNcXHUwNDQ4XFx1MDQzYVxcdTA0MzhcXHUwNDNkXFx1MDQ0MVxcdTA0M2FcXHUwNDMwXFx1MDQ0ZlxcXCIgXFx1MDQzMVxcdTA0NGJcXHUwNDNiIFxcdTA0NDNcXHUwNDQxXFx1MDQzZlxcdTA0MzVcXHUwNDQ4XFx1MDQzZFxcdTA0M2UgXFx1MDQzNFxcdTA0M2VcXHUwNDMxXFx1MDQzMFxcdTA0MzJcXHUwNDNiXFx1MDQzNVxcdTA0M2QuXCJdLFtcIl9fanNvbl9tZXNzYWdlXCIsMCwyNSxcIlxcdTA0MWNcXHUwNDM1XFx1MDQ0MlxcdTA0NDBcXHUwNDNlIFxcXCJcXHUwNDFmXFx1MDQ0MFxcdTA0M2VcXHUwNDQxXFx1MDQzZlxcdTA0MzVcXHUwNDNhXFx1MDQ0MiBcXHUwNDFmXFx1MDQ0MFxcdTA0M2VcXHUwNDQxXFx1MDQzMlxcdTA0MzVcXHUwNDQ5XFx1MDQzNVxcdTA0M2RcXHUwNDM4XFx1MDQ0ZlxcXCIgXFx1MDQzMVxcdTA0NGJcXHUwNDNiIFxcdTA0NDNcXHUwNDQxXFx1MDQzZlxcdTA0MzVcXHUwNDQ4XFx1MDQzZFxcdTA0M2UgXFx1MDQzNFxcdTA0M2VcXHUwNDMxXFx1MDQzMFxcdTA0MzJcXHUwNDNiXFx1MDQzNVxcdTA0M2QuXCJdLFtcIl9fanNvbl9tZXNzYWdlXCIsMCwyNSxcIlxcdTA0MWNcXHUwNDM1XFx1MDQ0MlxcdTA0NDBcXHUwNDNlIFxcXCJcXHUwNDFmXFx1MDQ0MFxcdTA0M2VcXHUwNDQxXFx1MDQzZlxcdTA0MzVcXHUwNDNhXFx1MDQ0MiBcXHUwNDEyXFx1MDQzNVxcdTA0NDJcXHUwNDM1XFx1MDQ0MFxcdTA0MzBcXHUwNDNkXFx1MDQzZVxcdTA0MzJcXFwiIFxcdTA0MzFcXHUwNDRiXFx1MDQzYiBcXHUwNDQzXFx1MDQ0MVxcdTA0M2ZcXHUwNDM1XFx1MDQ0OFxcdTA0M2RcXHUwNDNlIFxcdTA0MzRcXHUwNDNlXFx1MDQzMVxcdTA0MzBcXHUwNDMyXFx1MDQzYlxcdTA0MzVcXHUwNDNkLlwiXSxbXCJfX2pzb25fbWVzc2FnZVwiLDAsMjUsXCJcXHUwNDFjXFx1MDQzNVxcdTA0NDJcXHUwNDQwXFx1MDQzZSBcXFwiXFx1MDQxZlxcdTA0NDBcXHUwNDNlXFx1MDQ0MVxcdTA0M2ZcXHUwNDM1XFx1MDQzYVxcdTA0NDIgXFx1MDQxMVxcdTA0M2VcXHUwNDNiXFx1MDQ0Y1xcdTA0NDhcXHUwNDM1XFx1MDQzMlxcdTA0MzhcXHUwNDNhXFx1MDQzZVxcdTA0MzJcXFwiIFxcdTA0MzFcXHUwNDRiXFx1MDQzYiBcXHUwNDQzXFx1MDQ0MVxcdTA0M2ZcXHUwNDM1XFx1MDQ0OFxcdTA0M2RcXHUwNDNlIFxcdTA0MzRcXHUwNDNlXFx1MDQzMVxcdTA0MzBcXHUwNDMyXFx1MDQzYlxcdTA0MzVcXHUwNDNkLlwiXSxbXCJfX2pzb25fbWVzc2FnZVwiLDAsMjUsXCJcXHUwNDFjXFx1MDQzNVxcdTA0NDJcXHUwNDQwXFx1MDQzZSBcXFwiXFx1MDQxZlxcdTA0NDBcXHUwNDNlXFx1MDQzYlxcdTA0MzVcXHUwNDQyXFx1MDQzMFxcdTA0NDBcXHUwNDQxXFx1MDQzYVxcdTA0MzBcXHUwNDRmXFxcIiBcXHUwNDMxXFx1MDQ0YlxcdTA0M2IgXFx1MDQ0M1xcdTA0NDFcXHUwNDNmXFx1MDQzNVxcdTA0NDhcXHUwNDNkXFx1MDQzZSBcXHUwNDM0XFx1MDQzZVxcdTA0MzFcXHUwNDMwXFx1MDQzMlxcdTA0M2JcXHUwNDM1XFx1MDQzZC5cIl0sW1wiX19qc29uX21lc3NhZ2VcIiwwLDI1LFwiXFx1MDQxY1xcdTA0MzVcXHUwNDQyXFx1MDQ0MFxcdTA0M2UgXFxcIlxcdTA0MWZcXHUwNDQwXFx1MDQzOFxcdTA0M2NcXHUwNDNlXFx1MDQ0MFxcdTA0NDFcXHUwNDNhXFx1MDQzMFxcdTA0NGZcXFwiIFxcdTA0MzFcXHUwNDRiXFx1MDQzYiBcXHUwNDQzXFx1MDQ0MVxcdTA0M2ZcXHUwNDM1XFx1MDQ0OFxcdTA0M2RcXHUwNDNlIFxcdTA0MzRcXHUwNDNlXFx1MDQzMVxcdTA0MzBcXHUwNDMyXFx1MDQzYlxcdTA0MzVcXHUwNDNkLlwiXSxbXCJfX2pzb25fbWVzc2FnZVwiLDAsMjUsXCJcXHUwNDFjXFx1MDQzNVxcdTA0NDJcXHUwNDQwXFx1MDQzZSBcXFwiXFx1MDQxZlxcdTA0M2VcXHUwNDNiXFx1MDQzOFxcdTA0NDJcXHUwNDM1XFx1MDQ0NVxcdTA0M2RcXHUwNDM4XFx1MDQ0N1xcdTA0MzVcXHUwNDQxXFx1MDQzYVxcdTA0MzBcXHUwNDRmXFxcIiBcXHUwNDMxXFx1MDQ0YlxcdTA0M2IgXFx1MDQ0M1xcdTA0NDFcXHUwNDNmXFx1MDQzNVxcdTA0NDhcXHUwNDNkXFx1MDQzZSBcXHUwNDM0XFx1MDQzZVxcdTA0MzFcXHUwNDMwXFx1MDQzMlxcdTA0M2JcXHUwNDM1XFx1MDQzZC5cIl0sW1wiX19qc29uX21lc3NhZ2VcIiwwLDI1LFwiXFx1MDQxY1xcdTA0MzVcXHUwNDQyXFx1MDQ0MFxcdTA0M2UgXFxcIlxcdTA0MWZcXHUwNDNiXFx1MDQzZVxcdTA0NDlcXHUwNDMwXFx1MDQzNFxcdTA0NGMgXFx1MDQxY1xcdTA0NDNcXHUwNDM2XFx1MDQzNVxcdTA0NDFcXHUwNDQyXFx1MDQzMlxcdTA0MzBcXFwiIFxcdTA0MzFcXHUwNDRiXFx1MDQzYiBcXHUwNDQzXFx1MDQ0MVxcdTA0M2ZcXHUwNDM1XFx1MDQ0OFxcdTA0M2RcXHUwNDNlIFxcdTA0MzRcXHUwNDNlXFx1MDQzMVxcdTA0MzBcXHUwNDMyXFx1MDQzYlxcdTA0MzVcXHUwNDNkLlwiXSxbXCJfX2pzb25fbWVzc2FnZVwiLDAsMjUsXCJcXHUwNDFjXFx1MDQzNVxcdTA0NDJcXHUwNDQwXFx1MDQzZSBcXFwiXFx1MDQxZlxcdTA0M2JcXHUwNDNlXFx1MDQ0OVxcdTA0MzBcXHUwNDM0XFx1MDQ0YyBcXHUwNDFiXFx1MDQzNVxcdTA0M2RcXHUwNDM4XFx1MDQzZFxcdTA0MzBcXFwiIFxcdTA0MzFcXHUwNDRiXFx1MDQzYiBcXHUwNDQzXFx1MDQ0MVxcdTA0M2ZcXHUwNDM1XFx1MDQ0OFxcdTA0M2RcXHUwNDNlIFxcdTA0MzRcXHUwNDNlXFx1MDQzMVxcdTA0MzBcXHUwNDMyXFx1MDQzYlxcdTA0MzVcXHUwNDNkLlwiXSxbXCJfX2pzb25fbWVzc2FnZVwiLDAsMjUsXCJcXHUwNDFjXFx1MDQzNVxcdTA0NDJcXHUwNDQwXFx1MDQzZSBcXFwiXFx1MDQxZlxcdTA0M2JcXHUwNDNlXFx1MDQ0OVxcdTA0MzBcXHUwNDM0XFx1MDQ0YyBcXHUwNDEyXFx1MDQzZVxcdTA0NDFcXHUwNDQxXFx1MDQ0MlxcdTA0MzBcXHUwNDNkXFx1MDQzOFxcdTA0NGZcXFwiIFxcdTA0MzFcXHUwNDRiXFx1MDQzYiBcXHUwNDQzXFx1MDQ0MVxcdTA0M2ZcXHUwNDM1XFx1MDQ0OFxcdTA0M2RcXHUwNDNlIFxcdTA0MzRcXHUwNDNlXFx1MDQzMVxcdTA0MzBcXHUwNDMyXFx1MDQzYlxcdTA0MzVcXHUwNDNkLlwiXSxbXCJfX2pzb25fbWVzc2FnZVwiLDAsMjUsXCJcXHUwNDFjXFx1MDQzNVxcdTA0NDJcXHUwNDQwXFx1MDQzZSBcXFwiXFx1MDQxZlxcdTA0M2JcXHUwNDNlXFx1MDQ0OVxcdTA0MzBcXHUwNDM0XFx1MDQ0YyBcXHUwNDEwXFx1MDQzYlxcdTA0MzVcXHUwNDNhXFx1MDQ0MVxcdTA0MzBcXHUwNDNkXFx1MDQzNFxcdTA0NDBcXHUwNDMwIFxcdTA0MWRcXHUwNDM1XFx1MDQzMlxcdTA0NDFcXHUwNDNhXFx1MDQzZVxcdTA0MzNcXHUwNDNlXFxcIiBcXHUwNDMxXFx1MDQ0YlxcdTA0M2IgXFx1MDQ0M1xcdTA0NDFcXHUwNDNmXFx1MDQzNVxcdTA0NDhcXHUwNDNkXFx1MDQzZSBcXHUwNDM0XFx1MDQzZVxcdTA0MzFcXHUwNDMwXFx1MDQzMlxcdTA0M2JcXHUwNDM1XFx1MDQzZC5cIl0sW1wiX19qc29uX21lc3NhZ2VcIiwwLDI1LFwiXFx1MDQxY1xcdTA0MzVcXHUwNDQyXFx1MDQ0MFxcdTA0M2UgXFxcIlxcdTA0MWZcXHUwNDM4XFx1MDQzZVxcdTA0M2RcXHUwNDM1XFx1MDQ0MFxcdTA0NDFcXHUwNDNhXFx1MDQzMFxcdTA0NGZcXFwiIFxcdTA0MzFcXHUwNDRiXFx1MDQzYiBcXHUwNDQzXFx1MDQ0MVxcdTA0M2ZcXHUwNDM1XFx1MDQ0OFxcdTA0M2RcXHUwNDNlIFxcdTA0MzRcXHUwNDNlXFx1MDQzMVxcdTA0MzBcXHUwNDMyXFx1MDQzYlxcdTA0MzVcXHUwNDNkLlwiXSxbXCJfX2pzb25fbWVzc2FnZVwiLDAsMjUsXCJcXHUwNDFjXFx1MDQzNVxcdTA0NDJcXHUwNDQwXFx1MDQzZSBcXFwiXFx1MDQxZlxcdTA0MzVcXHUwNDQyXFx1MDQ0MFxcdTA0M2VcXHUwNDMzXFx1MDQ0MFxcdTA0MzBcXHUwNDM0XFx1MDQ0MVxcdTA0M2FcXHUwNDMwXFx1MDQ0ZlxcXCIgXFx1MDQzMVxcdTA0NGJcXHUwNDNiIFxcdTA0NDNcXHUwNDQxXFx1MDQzZlxcdTA0MzVcXHUwNDQ4XFx1MDQzZFxcdTA0M2UgXFx1MDQzNFxcdTA0M2VcXHUwNDMxXFx1MDQzMFxcdTA0MzJcXHUwNDNiXFx1MDQzNVxcdTA0M2QuXCJdLFtcIl9fanNvbl9tZXNzYWdlXCIsMCwyNSxcIlxcdTA0MWNcXHUwNDM1XFx1MDQ0MlxcdTA0NDBcXHUwNDNlIFxcXCJcXHUwNDFmXFx1MDQzMFxcdTA0NDBcXHUwNDNkXFx1MDQzMFxcdTA0NDFcXFwiIFxcdTA0MzFcXHUwNDRiXFx1MDQzYiBcXHUwNDQzXFx1MDQ0MVxcdTA0M2ZcXHUwNDM1XFx1MDQ0OFxcdTA0M2RcXHUwNDNlIFxcdTA0MzRcXHUwNDNlXFx1MDQzMVxcdTA0MzBcXHUwNDMyXFx1MDQzYlxcdTA0MzVcXHUwNDNkLlwiXSxbXCJfX2pzb25fbWVzc2FnZVwiLDAsMjUsXCJcXHUwNDFjXFx1MDQzNVxcdTA0NDJcXHUwNDQwXFx1MDQzZSBcXFwiXFx1MDQxZlxcdTA0MzBcXHUwNDQwXFx1MDQzYSBcXHUwNDFmXFx1MDQzZVxcdTA0MzFcXHUwNDM1XFx1MDQzNFxcdTA0NGJcXFwiIFxcdTA0MzFcXHUwNDRiXFx1MDQzYiBcXHUwNDQzXFx1MDQ0MVxcdTA0M2ZcXHUwNDM1XFx1MDQ0OFxcdTA0M2RcXHUwNDNlIFxcdTA0MzRcXHUwNDNlXFx1MDQzMVxcdTA0MzBcXHUwNDMyXFx1MDQzYlxcdTA0MzVcXHUwNDNkLlwiXSxbXCJfX2pzb25fbWVzc2FnZVwiLDAsMjUsXCJcXHUwNDFjXFx1MDQzNVxcdTA0NDJcXHUwNDQwXFx1MDQzZSBcXFwiXFx1MDQxZVxcdTA0MzdcXHUwNDM1XFx1MDQ0MFxcdTA0M2FcXHUwNDM4XFxcIiBcXHUwNDMxXFx1MDQ0YlxcdTA0M2IgXFx1MDQ0M1xcdTA0NDFcXHUwNDNmXFx1MDQzNVxcdTA0NDhcXHUwNDNkXFx1MDQzZSBcXHUwNDM0XFx1MDQzZVxcdTA0MzFcXHUwNDMwXFx1MDQzMlxcdTA0M2JcXHUwNDM1XFx1MDQzZC5cIl0sW1wiX19qc29uX21lc3NhZ2VcIiwwLDI1LFwiXFx1MDQxY1xcdTA0MzVcXHUwNDQyXFx1MDQ0MFxcdTA0M2UgXFxcIlxcdTA0MWVcXHUwNDMxXFx1MDQ0M1xcdTA0NDVcXHUwNDNlXFx1MDQzMlxcdTA0M2VcXFwiIFxcdTA0MzFcXHUwNDRiXFx1MDQzYiBcXHUwNDQzXFx1MDQ0MVxcdTA0M2ZcXHUwNDM1XFx1MDQ0OFxcdTA0M2RcXHUwNDNlIFxcdTA0MzRcXHUwNDNlXFx1MDQzMVxcdTA0MzBcXHUwNDMyXFx1MDQzYlxcdTA0MzVcXHUwNDNkLlwiXSxbXCJfX2pzb25fbWVzc2FnZVwiLDAsMjUsXCJcXHUwNDFjXFx1MDQzNVxcdTA0NDJcXHUwNDQwXFx1MDQzZSBcXFwiXFx1MDQxZVxcdTA0MzFcXHUwNDMyXFx1MDQzZVxcdTA0MzRcXHUwNDNkXFx1MDQ0YlxcdTA0MzkgXFx1MDQzYVxcdTA0MzBcXHUwNDNkXFx1MDQzMFxcdTA0M2JcXFwiIFxcdTA0MzFcXHUwNDRiXFx1MDQzYiBcXHUwNDQzXFx1MDQ0MVxcdTA0M2ZcXHUwNDM1XFx1MDQ0OFxcdTA0M2RcXHUwNDNlIFxcdTA0MzRcXHUwNDNlXFx1MDQzMVxcdTA0MzBcXHUwNDMyXFx1MDQzYlxcdTA0MzVcXHUwNDNkLlwiXSxbXCJfX2pzb25fbWVzc2FnZVwiLDAsMjUsXCJcXHUwNDFjXFx1MDQzNVxcdTA0NDJcXHUwNDQwXFx1MDQzZSBcXFwiXFx1MDQxZFxcdTA0M2VcXHUwNDMyXFx1MDQzZVxcdTA0NDdcXHUwNDM1XFx1MDQ0MFxcdTA0M2FcXHUwNDMwXFx1MDQ0MVxcdTA0NDFcXHUwNDNhXFx1MDQzMFxcdTA0NGZcXFwiIFxcdTA0MzFcXHUwNDRiXFx1MDQzYiBcXHUwNDQzXFx1MDQ0MVxcdTA0M2ZcXHUwNDM1XFx1MDQ0OFxcdTA0M2RcXHUwNDNlIFxcdTA0MzRcXHUwNDNlXFx1MDQzMVxcdTA0MzBcXHUwNDMyXFx1MDQzYlxcdTA0MzVcXHUwNDNkLlwiXSxbXCJfX2pzb25fbWVzc2FnZVwiLDAsMjUsXCJcXHUwNDFjXFx1MDQzNVxcdTA0NDJcXHUwNDQwXFx1MDQzZSBcXFwiXFx1MDQxZFxcdTA0MzVcXHUwNDMyXFx1MDQ0MVxcdTA0M2FcXHUwNDM4XFx1MDQzOSBcXHUwNDNmXFx1MDQ0MFxcdTA0M2VcXHUwNDQxXFx1MDQzZlxcdTA0MzVcXHUwNDNhXFx1MDQ0MlxcXCIgXFx1MDQzMVxcdTA0NGJcXHUwNDNiIFxcdTA0NDNcXHUwNDQxXFx1MDQzZlxcdTA0MzVcXHUwNDQ4XFx1MDQzZFxcdTA0M2UgXFx1MDQzNFxcdTA0M2VcXHUwNDMxXFx1MDQzMFxcdTA0MzJcXHUwNDNiXFx1MDQzNVxcdTA0M2QuXCJdLFtcIl9fanNvbl9tZXNzYWdlXCIsMCwyNSxcIlxcdTA0MWNcXHUwNDM1XFx1MDQ0MlxcdTA0NDBcXHUwNDNlIFxcXCJcXHUwNDFkXFx1MDQzMFxcdTA0NDBcXHUwNDMyXFx1MDQ0MVxcdTA0M2FcXHUwNDMwXFx1MDQ0ZlxcXCIgXFx1MDQzMVxcdTA0NGJcXHUwNDNiIFxcdTA0NDNcXHUwNDQxXFx1MDQzZlxcdTA0MzVcXHUwNDQ4XFx1MDQzZFxcdTA0M2UgXFx1MDQzNFxcdTA0M2VcXHUwNDMxXFx1MDQzMFxcdTA0MzJcXHUwNDNiXFx1MDQzNVxcdTA0M2QuXCJdLFtcIl9fanNvbl9tZXNzYWdlXCIsMCwyNSxcIlxcdTA0MWNcXHUwNDM1XFx1MDQ0MlxcdTA0NDBcXHUwNDNlIFxcXCJcXHUwNDFjXFx1MDQzZVxcdTA0NDFcXHUwNDNhXFx1MDQzZVxcdTA0MzJcXHUwNDQxXFx1MDQzYVxcdTA0MzhcXHUwNDM1IFxcdTA0MzJcXHUwNDNlXFx1MDQ0MFxcdTA0M2VcXHUwNDQyXFx1MDQzMFxcXCIgXFx1MDQzMVxcdTA0NGJcXHUwNDNiIFxcdTA0NDNcXHUwNDQxXFx1MDQzZlxcdTA0MzVcXHUwNDQ4XFx1MDQzZFxcdTA0M2UgXFx1MDQzNFxcdTA0M2VcXHUwNDMxXFx1MDQzMFxcdTA0MzJcXHUwNDNiXFx1MDQzNVxcdTA0M2QuXCJdLFtcIl9fanNvbl9tZXNzYWdlXCIsMCwyNSxcIlxcdTA0MWNcXHUwNDM1XFx1MDQ0MlxcdTA0NDBcXHUwNDNlIFxcXCJcXHUwNDFjXFx1MDQzZVxcdTA0NDFcXHUwNDNhXFx1MDQzZVxcdTA0MzJcXHUwNDQxXFx1MDQzYVxcdTA0MzBcXHUwNDRmXFxcIiBcXHUwNDMxXFx1MDQ0YlxcdTA0M2IgXFx1MDQ0M1xcdTA0NDFcXHUwNDNmXFx1MDQzNVxcdTA0NDhcXHUwNDNkXFx1MDQzZSBcXHUwNDM0XFx1MDQzZVxcdTA0MzFcXHUwNDMwXFx1MDQzMlxcdTA0M2JcXHUwNDM1XFx1MDQzZC5cIl0sW1wiX19qc29uX21lc3NhZ2VcIiwwLDI1LFwiXFx1MDQxY1xcdTA0MzVcXHUwNDQyXFx1MDQ0MFxcdTA0M2UgXFxcIlxcdTA0MWNcXHUwNDM1XFx1MDQzNlxcdTA0MzRcXHUwNDQzXFx1MDQzZFxcdTA0MzBcXHUwNDQwXFx1MDQzZVxcdTA0MzRcXHUwNDNkXFx1MDQzMFxcdTA0NGZcXFwiIFxcdTA0MzFcXHUwNDRiXFx1MDQzYiBcXHUwNDQzXFx1MDQ0MVxcdTA0M2ZcXHUwNDM1XFx1MDQ0OFxcdTA0M2RcXHUwNDNlIFxcdTA0MzRcXHUwNDNlXFx1MDQzMVxcdTA0MzBcXHUwNDMyXFx1MDQzYlxcdTA0MzVcXHUwNDNkLlwiXSxbXCJfX2pzb25fbWVzc2FnZVwiLDAsMjUsXCJcXHUwNDFjXFx1MDQzNVxcdTA0NDJcXHUwNDQwXFx1MDQzZSBcXFwiXFx1MDQxY1xcdTA0MzBcXHUwNDRmXFx1MDQzYVxcdTA0M2VcXHUwNDMyXFx1MDQ0MVxcdTA0M2FcXHUwNDMwXFx1MDQ0ZlxcXCIgXFx1MDQzMVxcdTA0NGJcXHUwNDNiIFxcdTA0NDNcXHUwNDQxXFx1MDQzZlxcdTA0MzVcXHUwNDQ4XFx1MDQzZFxcdTA0M2UgXFx1MDQzNFxcdTA0M2VcXHUwNDMxXFx1MDQzMFxcdTA0MzJcXHUwNDNiXFx1MDQzNVxcdTA0M2QuXCJdLFtcIl9fanNvbl9tZXNzYWdlXCIsMCwyNSxcIlxcdTA0MWNcXHUwNDM1XFx1MDQ0MlxcdTA0NDBcXHUwNDNlIFxcXCJcXHUwNDFiXFx1MDQzZVxcdTA0M2NcXHUwNDNlXFx1MDQzZFxcdTA0M2VcXHUwNDQxXFx1MDQzZVxcdTA0MzJcXHUwNDQxXFx1MDQzYVxcdTA0MzBcXHUwNDRmXFxcIiBcXHUwNDMxXFx1MDQ0YlxcdTA0M2IgXFx1MDQ0M1xcdTA0NDFcXHUwNDNmXFx1MDQzNVxcdTA0NDhcXHUwNDNkXFx1MDQzZSBcXHUwNDM0XFx1MDQzZVxcdTA0MzFcXHUwNDMwXFx1MDQzMlxcdTA0M2JcXHUwNDM1XFx1MDQzZC5cIl0sW1wiX19qc29uX21lc3NhZ2VcIiwwLDI1LFwiXFx1MDQxY1xcdTA0MzVcXHUwNDQyXFx1MDQ0MFxcdTA0M2UgXFxcIlxcdTA0MWJcXHUwNDM4XFx1MDQzM1xcdTA0M2VcXHUwNDMyXFx1MDQ0MVxcdTA0M2FcXHUwNDM4XFx1MDQzOSBcXHUwNDNmXFx1MDQ0MFxcdTA0M2VcXHUwNDQxXFx1MDQzZlxcdTA0MzVcXHUwNDNhXFx1MDQ0MlxcXCIgXFx1MDQzMVxcdTA0NGJcXHUwNDNiIFxcdTA0NDNcXHUwNDQxXFx1MDQzZlxcdTA0MzVcXHUwNDQ4XFx1MDQzZFxcdTA0M2UgXFx1MDQzNFxcdTA0M2VcXHUwNDMxXFx1MDQzMFxcdTA0MzJcXHUwNDNiXFx1MDQzNVxcdTA0M2QuXCJdLFtcIl9fanNvbl9tZXNzYWdlXCIsMCwyNSxcIlxcdTA0MWNcXHUwNDM1XFx1MDQ0MlxcdTA0NDBcXHUwNDNlIFxcXCJcXHUwNDFiXFx1MDQzNVxcdTA0NDFcXHUwNDNkXFx1MDQzMFxcdTA0NGZcXFwiIFxcdTA0MzFcXHUwNDRiXFx1MDQzYiBcXHUwNDQzXFx1MDQ0MVxcdTA0M2ZcXHUwNDM1XFx1MDQ0OFxcdTA0M2RcXHUwNDNlIFxcdTA0MzRcXHUwNDNlXFx1MDQzMVxcdTA0MzBcXHUwNDMyXFx1MDQzYlxcdTA0MzVcXHUwNDNkLlwiXSxbXCJfX2pzb25fbWVzc2FnZVwiLDAsMjUsXCJcXHUwNDFjXFx1MDQzNVxcdTA0NDJcXHUwNDQwXFx1MDQzZSBcXFwiXFx1MDQxYlxcdTA0MzVcXHUwNDNkXFx1MDQzOFxcdTA0M2RcXHUwNDQxXFx1MDQzYVxcdTA0MzhcXHUwNDM5IFxcdTA0M2ZcXHUwNDQwXFx1MDQzZVxcdTA0NDFcXHUwNDNmXFx1MDQzNVxcdTA0M2FcXHUwNDQyXFxcIiBcXHUwNDMxXFx1MDQ0YlxcdTA0M2IgXFx1MDQ0M1xcdTA0NDFcXHUwNDNmXFx1MDQzNVxcdTA0NDhcXHUwNDNkXFx1MDQzZSBcXHUwNDM0XFx1MDQzZVxcdTA0MzFcXHUwNDMwXFx1MDQzMlxcdTA0M2JcXHUwNDM1XFx1MDQzZC5cIl0sW1wiX19qc29uX21lc3NhZ2VcIiwwLDI1LFwiXFx1MDQxY1xcdTA0MzVcXHUwNDQyXFx1MDQ0MFxcdTA0M2UgXFxcIlxcdTA0MWJcXHUwNDMwXFx1MDQzNFxcdTA0M2VcXHUwNDM2XFx1MDQ0MVxcdTA0M2FcXHUwNDMwXFx1MDQ0ZlxcXCIgXFx1MDQzMVxcdTA0NGJcXHUwNDNiIFxcdTA0NDNcXHUwNDQxXFx1MDQzZlxcdTA0MzVcXHUwNDQ4XFx1MDQzZFxcdTA0M2UgXFx1MDQzNFxcdTA0M2VcXHUwNDMxXFx1MDQzMFxcdTA0MzJcXHUwNDNiXFx1MDQzNVxcdTA0M2QuXCJdLFtcIl9fanNvbl9tZXNzYWdlXCIsMCwyNSxcIlxcdTA0MWNcXHUwNDM1XFx1MDQ0MlxcdTA0NDBcXHUwNDNlIFxcXCJcXHUwNDFhXFx1MDQ0M1xcdTA0M2ZcXHUwNDQ3XFx1MDQzOFxcdTA0M2RcXHUwNDNlXFxcIiBcXHUwNDMxXFx1MDQ0YlxcdTA0M2IgXFx1MDQ0M1xcdTA0NDFcXHUwNDNmXFx1MDQzNVxcdTA0NDhcXHUwNDNkXFx1MDQzZSBcXHUwNDM0XFx1MDQzZVxcdTA0MzFcXHUwNDMwXFx1MDQzMlxcdTA0M2JcXHUwNDM1XFx1MDQzZC5cIl0sW1wiX19qc29uX21lc3NhZ2VcIiwwLDI1LFwiXFx1MDQxY1xcdTA0MzVcXHUwNDQyXFx1MDQ0MFxcdTA0M2UgXFxcIlxcdTA0MWFcXHUwNDQwXFx1MDQzNVxcdTA0NDFcXHUwNDQyXFx1MDQzZVxcdTA0MzJcXHUwNDQxXFx1MDQzYVxcdTA0MzhcXHUwNDM5IFxcdTA0M2VcXHUwNDQxXFx1MDQ0MlxcdTA0NDBcXHUwNDNlXFx1MDQzMlxcXCIgXFx1MDQzMVxcdTA0NGJcXHUwNDNiIFxcdTA0NDNcXHUwNDQxXFx1MDQzZlxcdTA0MzVcXHUwNDQ4XFx1MDQzZFxcdTA0M2UgXFx1MDQzNFxcdTA0M2VcXHUwNDMxXFx1MDQzMFxcdTA0MzJcXHUwNDNiXFx1MDQzNVxcdTA0M2QuXCJdLFtcIl9fanNvbl9tZXNzYWdlXCIsMCwyNSxcIlxcdTA0MWNcXHUwNDM1XFx1MDQ0MlxcdTA0NDBcXHUwNDNlIFxcXCJcXHUwNDFhXFx1MDQzZVxcdTA0M2NcXHUwNDM1XFx1MDQzZFxcdTA0MzRcXHUwNDMwXFx1MDQzZFxcdTA0NDJcXHUwNDQxXFx1MDQzYVxcdTA0MzhcXHUwNDM5IFxcdTA0M2ZcXHUwNDQwXFx1MDQzZVxcdTA0NDFcXHUwNDNmXFx1MDQzNVxcdTA0M2FcXHUwNDQyXFxcIiBcXHUwNDMxXFx1MDQ0YlxcdTA0M2IgXFx1MDQ0M1xcdTA0NDFcXHUwNDNmXFx1MDQzNVxcdTA0NDhcXHUwNDNkXFx1MDQzZSBcXHUwNDM0XFx1MDQzZVxcdTA0MzFcXHUwNDMwXFx1MDQzMlxcdTA0M2JcXHUwNDM1XFx1MDQzZC5cIl0sW1wiX19qc29uX21lc3NhZ2VcIiwwLDI1LFwiXFx1MDQxY1xcdTA0MzVcXHUwNDQyXFx1MDQ0MFxcdTA0M2UgXFxcIlxcdTA0MWFcXHUwNDM4XFx1MDQ0MFxcdTA0M2VcXHUwNDMyXFx1MDQ0MVxcdTA0M2FcXHUwNDM4XFx1MDQzOSBcXHUwNDM3XFx1MDQzMFxcdTA0MzJcXHUwNDNlXFx1MDQzNFxcXCIgXFx1MDQzMVxcdTA0NGJcXHUwNDNiIFxcdTA0NDNcXHUwNDQxXFx1MDQzZlxcdTA0MzVcXHUwNDQ4XFx1MDQzZFxcdTA0M2UgXFx1MDQzNFxcdTA0M2VcXHUwNDMxXFx1MDQzMFxcdTA0MzJcXHUwNDNiXFx1MDQzNVxcdTA0M2QuXCJdLFtcIl9fanNvbl9tZXNzYWdlXCIsMCwyNSxcIlxcdTA0MWNcXHUwNDM1XFx1MDQ0MlxcdTA0NDBcXHUwNDNlIFxcXCJcXHUwNDE3XFx1MDQzMlxcdTA0MzVcXHUwNDNkXFx1MDQzOFxcdTA0MzNcXHUwNDNlXFx1MDQ0MFxcdTA0M2VcXHUwNDM0XFx1MDQ0MVxcdTA0M2FcXHUwNDMwXFx1MDQ0ZlxcXCIgXFx1MDQzMVxcdTA0NGJcXHUwNDNiIFxcdTA0NDNcXHUwNDQxXFx1MDQzZlxcdTA0MzVcXHUwNDQ4XFx1MDQzZFxcdTA0M2UgXFx1MDQzNFxcdTA0M2VcXHUwNDMxXFx1MDQzMFxcdTA0MzJcXHUwNDNiXFx1MDQzNVxcdTA0M2QuXCJdLFtcIl9fanNvbl9tZXNzYWdlXCIsMCwyNSxcIlxcdTA0MWNcXHUwNDM1XFx1MDQ0MlxcdTA0NDBcXHUwNDNlIFxcXCJcXHUwNDE3XFx1MDQzMlxcdTA0NTFcXHUwNDM3XFx1MDQzNFxcdTA0M2RcXHUwNDMwXFx1MDQ0ZlxcXCIgXFx1MDQzMVxcdTA0NGJcXHUwNDNiIFxcdTA0NDNcXHUwNDQxXFx1MDQzZlxcdTA0MzVcXHUwNDQ4XFx1MDQzZFxcdTA0M2UgXFx1MDQzNFxcdTA0M2VcXHUwNDMxXFx1MDQzMFxcdTA0MzJcXHUwNDNiXFx1MDQzNVxcdTA0M2QuXCJdLFtcIl9fanNvbl9tZXNzYWdlXCIsMCwyNSxcIlxcdTA0MWNcXHUwNDM1XFx1MDQ0MlxcdTA0NDBcXHUwNDNlIFxcXCJcXHUwNDE1XFx1MDQzYlxcdTA0MzhcXHUwNDM3XFx1MDQzMFxcdTA0NDBcXHUwNDNlXFx1MDQzMlxcdTA0NDFcXHUwNDNhXFx1MDQzMFxcdTA0NGZcXFwiIFxcdTA0MzFcXHUwNDRiXFx1MDQzYiBcXHUwNDQzXFx1MDQ0MVxcdTA0M2ZcXHUwNDM1XFx1MDQ0OFxcdTA0M2RcXHUwNDNlIFxcdTA0MzRcXHUwNDNlXFx1MDQzMVxcdTA0MzBcXHUwNDMyXFx1MDQzYlxcdTA0MzVcXHUwNDNkLlwiXSxbXCJfX2pzb25fbWVzc2FnZVwiLDAsMjUsXCJcXHUwNDFjXFx1MDQzNVxcdTA0NDJcXHUwNDQwXFx1MDQzZSBcXFwiXFx1MDQxNFxcdTA0M2VcXHUwNDQxXFx1MDQ0MlxcdTA0M2VcXHUwNDM1XFx1MDQzMlxcdTA0NDFcXHUwNDNhXFx1MDQzMFxcdTA0NGZcXFwiIFxcdTA0MzFcXHUwNDRiXFx1MDQzYiBcXHUwNDQzXFx1MDQ0MVxcdTA0M2ZcXHUwNDM1XFx1MDQ0OFxcdTA0M2RcXHUwNDNlIFxcdTA0MzRcXHUwNDNlXFx1MDQzMVxcdTA0MzBcXHUwNDMyXFx1MDQzYlxcdTA0MzVcXHUwNDNkLlwiXSxbXCJfX2pzb25fbWVzc2FnZVwiLDAsMjUsXCJcXHUwNDFjXFx1MDQzNVxcdTA0NDJcXHUwNDQwXFx1MDQzZSBcXFwiXFx1MDQxNFxcdTA0MzVcXHUwNDMyXFx1MDQ0ZlxcdTA0NDJcXHUwNDNhXFx1MDQzOFxcdTA0M2RcXHUwNDNlXFxcIiBcXHUwNDMxXFx1MDQ0YlxcdTA0M2IgXFx1MDQ0M1xcdTA0NDFcXHUwNDNmXFx1MDQzNVxcdTA0NDhcXHUwNDNkXFx1MDQzZSBcXHUwNDM0XFx1MDQzZVxcdTA0MzFcXHUwNDMwXFx1MDQzMlxcdTA0M2JcXHUwNDM1XFx1MDQzZC5cIl0sW1wiX19qc29uX21lc3NhZ2VcIiwwLDI1LFwiXFx1MDQxY1xcdTA0MzVcXHUwNDQyXFx1MDQ0MFxcdTA0M2UgXFxcIlxcdTA0MTNcXHUwNDQwXFx1MDQzMFxcdTA0MzZcXHUwNDM0XFx1MDQzMFxcdTA0M2RcXHUwNDQxXFx1MDQzYVxcdTA0MzhcXHUwNDM5IFxcdTA0M2ZcXHUwNDQwXFx1MDQzZVxcdTA0NDFcXHUwNDNmXFx1MDQzNVxcdTA0M2FcXHUwNDQyXFxcIiBcXHUwNDMxXFx1MDQ0YlxcdTA0M2IgXFx1MDQ0M1xcdTA0NDFcXHUwNDNmXFx1MDQzNVxcdTA0NDhcXHUwNDNkXFx1MDQzZSBcXHUwNDM0XFx1MDQzZVxcdTA0MzFcXHUwNDMwXFx1MDQzMlxcdTA0M2JcXHUwNDM1XFx1MDQzZC5cIl0sW1wiX19qc29uX21lc3NhZ2VcIiwwLDI1LFwiXFx1MDQxY1xcdTA0MzVcXHUwNDQyXFx1MDQ0MFxcdTA0M2UgXFxcIlxcdTA0MTNcXHUwNDNlXFx1MDQ0MVxcdTA0NDJcXHUwNDM4XFx1MDQzZFxcdTA0NGJcXHUwNDM5IFxcdTA0MzRcXHUwNDMyXFx1MDQzZVxcdTA0NDBcXFwiIFxcdTA0MzFcXHUwNDRiXFx1MDQzYiBcXHUwNDQzXFx1MDQ0MVxcdTA0M2ZcXHUwNDM1XFx1MDQ0OFxcdTA0M2RcXHUwNDNlIFxcdTA0MzRcXHUwNDNlXFx1MDQzMVxcdTA0MzBcXHUwNDMyXFx1MDQzYlxcdTA0MzVcXHUwNDNkLlwiXSxbXCJfX2pzb25fbWVzc2FnZVwiLDAsMjUsXCJcXHUwNDFjXFx1MDQzNVxcdTA0NDJcXHUwNDQwXFx1MDQzZSBcXFwiXFx1MDQxM1xcdTA0M2VcXHUwNDQwXFx1MDQ0Y1xcdTA0M2FcXHUwNDNlXFx1MDQzMlxcdTA0NDFcXHUwNDNhXFx1MDQzMFxcdTA0NGZcXFwiIFxcdTA0MzFcXHUwNDRiXFx1MDQzYiBcXHUwNDQzXFx1MDQ0MVxcdTA0M2ZcXHUwNDM1XFx1MDQ0OFxcdTA0M2RcXHUwNDNlIFxcdTA0MzRcXHUwNDNlXFx1MDQzMVxcdTA0MzBcXHUwNDMyXFx1MDQzYlxcdTA0MzVcXHUwNDNkLlwiXSxbXCJfX2pzb25fbWVzc2FnZVwiLDAsMjUsXCJcXHUwNDFjXFx1MDQzNVxcdTA0NDJcXHUwNDQwXFx1MDQzZSBcXFwiXFx1MDQxMlxcdTA0NGJcXHUwNDMxXFx1MDQzZVxcdTA0NDBcXHUwNDMzXFx1MDQ0MVxcdTA0M2FcXHUwNDMwXFx1MDQ0ZlxcXCIgXFx1MDQzMVxcdTA0NGJcXHUwNDNiIFxcdTA0NDNcXHUwNDQxXFx1MDQzZlxcdTA0MzVcXHUwNDQ4XFx1MDQzZFxcdTA0M2UgXFx1MDQzNFxcdTA0M2VcXHUwNDMxXFx1MDQzMFxcdTA0MzJcXHUwNDNiXFx1MDQzNVxcdTA0M2QuXCJdLFtcIl9fanNvbl9tZXNzYWdlXCIsMCwyNSxcIlxcdTA0MWNcXHUwNDM1XFx1MDQ0MlxcdTA0NDBcXHUwNDNlIFxcXCJcXHUwNDEyXFx1MDQzZVxcdTA0M2JcXHUwNDNhXFx1MDQzZVxcdTA0MzJcXHUwNDQxXFx1MDQzYVxcdTA0MzBcXHUwNDRmXFxcIiBcXHUwNDMxXFx1MDQ0YlxcdTA0M2IgXFx1MDQ0M1xcdTA0NDFcXHUwNDNmXFx1MDQzNVxcdTA0NDhcXHUwNDNkXFx1MDQzZSBcXHUwNDM0XFx1MDQzZVxcdTA0MzFcXHUwNDMwXFx1MDQzMlxcdTA0M2JcXHUwNDM1XFx1MDQzZC5cIl0sW1wiX19qc29uX21lc3NhZ2VcIiwwLDI1LFwiXFx1MDQxY1xcdTA0MzVcXHUwNDQyXFx1MDQ0MFxcdTA0M2UgXFxcIlxcdTA0MTJcXHUwNDNiXFx1MDQzMFxcdTA0MzRcXHUwNDM4XFx1MDQzY1xcdTA0MzhcXHUwNDQwXFx1MDQ0MVxcdTA0M2FcXHUwNDMwXFx1MDQ0ZlxcXCIgXFx1MDQzMVxcdTA0NGJcXHUwNDNiIFxcdTA0NDNcXHUwNDQxXFx1MDQzZlxcdTA0MzVcXHUwNDQ4XFx1MDQzZFxcdTA0M2UgXFx1MDQzNFxcdTA0M2VcXHUwNDMxXFx1MDQzMFxcdTA0MzJcXHUwNDNiXFx1MDQzNVxcdTA0M2QuXCJdLFtcIl9fanNvbl9tZXNzYWdlXCIsMCwyNSxcIlxcdTA0MWNcXHUwNDM1XFx1MDQ0MlxcdTA0NDBcXHUwNDNlIFxcXCJcXHUwNDEyXFx1MDQzMFxcdTA0NDFcXHUwNDM4XFx1MDQzYlxcdTA0MzVcXHUwNDNlXFx1MDQ0MVxcdTA0NDJcXHUwNDQwXFx1MDQzZVxcdTA0MzJcXHUwNDQxXFx1MDQzYVxcdTA0MzBcXHUwNDRmXFxcIiBcXHUwNDMxXFx1MDQ0YlxcdTA0M2IgXFx1MDQ0M1xcdTA0NDFcXHUwNDNmXFx1MDQzNVxcdTA0NDhcXHUwNDNkXFx1MDQzZSBcXHUwNDM0XFx1MDQzZVxcdTA0MzFcXHUwNDMwXFx1MDQzMlxcdTA0M2JcXHUwNDM1XFx1MDQzZC5cIl0sW1wiX19qc29uX21lc3NhZ2VcIiwwLDI1LFwiXFx1MDQxY1xcdTA0MzVcXHUwNDQyXFx1MDQ0MFxcdTA0M2UgXFxcIlxcdTA0MTFcXHUwNDQzXFx1MDQ0NVxcdTA0MzBcXHUwNDQwXFx1MDQzNVxcdTA0NDFcXHUwNDQyXFx1MDQ0MVxcdTA0M2FcXHUwNDMwXFx1MDQ0ZlxcXCIgXFx1MDQzMVxcdTA0NGJcXHUwNDNiIFxcdTA0NDNcXHUwNDQxXFx1MDQzZlxcdTA0MzVcXHUwNDQ4XFx1MDQzZFxcdTA0M2UgXFx1MDQzNFxcdTA0M2VcXHUwNDMxXFx1MDQzMFxcdTA0MzJcXHUwNDNiXFx1MDQzNVxcdTA0M2QuXCJdLFtcIl9fanNvbl9tZXNzYWdlXCIsMCwyNSxcIlxcdTA0MWNcXHUwNDM1XFx1MDQ0MlxcdTA0NDBcXHUwNDNlIFxcXCJcXHUwNDExXFx1MDQzMFxcdTA0M2JcXHUwNDQyXFx1MDQzOFxcdTA0MzlcXHUwNDQxXFx1MDQzYVxcdTA0MzBcXHUwNDRmXFxcIiBcXHUwNDMxXFx1MDQ0YlxcdTA0M2IgXFx1MDQ0M1xcdTA0NDFcXHUwNDNmXFx1MDQzNVxcdTA0NDhcXHUwNDNkXFx1MDQzZSBcXHUwNDM0XFx1MDQzZVxcdTA0MzFcXHUwNDMwXFx1MDQzMlxcdTA0M2JcXHUwNDM1XFx1MDQzZC5cIl0sW1wiX19qc29uX21lc3NhZ2VcIiwwLDI1LFwiXFx1MDQxY1xcdTA0MzVcXHUwNDQyXFx1MDQ0MFxcdTA0M2UgXFxcIlxcdTA0MTBcXHUwNDNhXFx1MDQzMFxcdTA0MzRcXHUwNDM1XFx1MDQzY1xcdTA0MzhcXHUwNDQ3XFx1MDQzNVxcdTA0NDFcXHUwNDNhXFx1MDQzMFxcdTA0NGZcXFwiIFxcdTA0MzFcXHUwNDRiXFx1MDQzYiBcXHUwNDQzXFx1MDQ0MVxcdTA0M2ZcXHUwNDM1XFx1MDQ0OFxcdTA0M2RcXHUwNDNlIFxcdTA0MzRcXHUwNDNlXFx1MDQzMVxcdTA0MzBcXHUwNDMyXFx1MDQzYlxcdTA0MzVcXHUwNDNkLlwiXSxbXCJfX2pzb25fbWVzc2FnZVwiLDAsMjUsXCJcXHUwNDFjXFx1MDQzNVxcdTA0NDJcXHUwNDQwXFx1MDQzZSBcXFwiXFx1MDQxMFxcdTA0MzRcXHUwNDNjXFx1MDQzOFxcdTA0NDBcXHUwNDMwXFx1MDQzYlxcdTA0NDJcXHUwNDM1XFx1MDQzOVxcdTA0NDFcXHUwNDNhXFx1MDQzMFxcdTA0NGZcXFwiIFxcdTA0MzFcXHUwNDRiXFx1MDQzYiBcXHUwNDQzXFx1MDQ0MVxcdTA0M2ZcXHUwNDM1XFx1MDQ0OFxcdTA0M2RcXHUwNDNlIFxcdTA0MzRcXHUwNDNlXFx1MDQzMVxcdTA0MzBcXHUwNDMyXFx1MDQzYlxcdTA0MzVcXHUwNDNkLlwiXSxbXCJfX2pzb25fbWVzc2FnZVwiLDAsMjUsXCJcXHUwNDFjXFx1MDQzNVxcdTA0NDJcXHUwNDQwXFx1MDQzZSBcXFwiXFx1MDQxMFxcdTA0MzJcXHUwNDQyXFx1MDQzZVxcdTA0MzJcXHUwNDNlXFxcIiBcXHUwNDMxXFx1MDQ0YlxcdTA0M2IgXFx1MDQ0M1xcdTA0NDFcXHUwNDNmXFx1MDQzNVxcdTA0NDhcXHUwNDNkXFx1MDQzZSBcXHUwNDM0XFx1MDQzZVxcdTA0MzFcXHUwNDMwXFx1MDQzMlxcdTA0M2JcXHUwNDM1XFx1MDQzZC5cIl0sW1wiX19qc29uX21lc3NhZ2VcIiwwLDI1LFwiXFx1MDQxOFxcdTA0NDFcXHUwNDQxXFx1MDQzYlxcdTA0MzVcXHUwNDM0XFx1MDQzZVxcdTA0MzJcXHUwNDMwXFx1MDQ0MlxcdTA0MzVcXHUwNDNiXFx1MDQ0Y1xcdTA0NDFcXHUwNDNhXFx1MDQzOFxcdTA0MzkgXFx1MDQ0NlxcdTA0MzVcXHUwNDNkXFx1MDQ0MlxcdTA0NDAgXFxcIkFkdmFuY2VkIEZlcnRpbGl0eSBDbGluaWNcXFwiIFxcdTA0MzFcXHUwNDRiXFx1MDQzYiBcXHUwNDQzXFx1MDQ0MVxcdTA0M2ZcXHUwNDM1XFx1MDQ0OFxcdTA0M2RcXHUwNDNlIFxcdTA0MzhcXHUwNDM3XFx1MDQzY1xcdTA0MzVcXHUwNDNkXFx1MDQzNVxcdTA0M2QuXCJdLFtcIl9fanNvbl9tZXNzYWdlXCIsMCwyNSxcIlxcdTA0MThcXHUwNDQxXFx1MDQ0MVxcdTA0M2JcXHUwNDM1XFx1MDQzNFxcdTA0M2VcXHUwNDMyXFx1MDQzMFxcdTA0NDJcXHUwNDM1XFx1MDQzYlxcdTA0NGNcXHUwNDQxXFx1MDQzYVxcdTA0MzhcXHUwNDM5IFxcdTA0NDZcXHUwNDM1XFx1MDQzZFxcdTA0NDJcXHUwNDQwIFxcXCJ0ZXN0MVxcXCIgXFx1MDQzMVxcdTA0NGJcXHUwNDNiIFxcdTA0NDNcXHUwNDQxXFx1MDQzZlxcdTA0MzVcXHUwNDQ4XFx1MDQzZFxcdTA0M2UgXFx1MDQzOFxcdTA0MzdcXHUwNDNjXFx1MDQzNVxcdTA0M2RcXHUwNDM1XFx1MDQzZC5cIl0sW1wiX19qc29uX21lc3NhZ2VcIiwwLDI1LFwiXFx1MDQxOFxcdTA0NDFcXHUwNDQxXFx1MDQzYlxcdTA0MzVcXHUwNDM0XFx1MDQzZVxcdTA0MzJcXHUwNDMwXFx1MDQ0MlxcdTA0MzVcXHUwNDNiXFx1MDQ0Y1xcdTA0NDFcXHUwNDNhXFx1MDQzOFxcdTA0MzkgXFx1MDQ0NlxcdTA0MzVcXHUwNDNkXFx1MDQ0MlxcdTA0NDAgXFxcInRlc3QzXFxcIiBcXHUwNDMxXFx1MDQ0YlxcdTA0M2IgXFx1MDQ0M1xcdTA0NDFcXHUwNDNmXFx1MDQzNVxcdTA0NDhcXHUwNDNkXFx1MDQzZSBcXHUwNDM4XFx1MDQzN1xcdTA0M2NcXHUwNDM1XFx1MDQzZFxcdTA0MzVcXHUwNDNkLlwiXSxbXCJfX2pzb25fbWVzc2FnZVwiLDAsMjUsXCJcXHUwNDE4XFx1MDQ0MVxcdTA0NDFcXHUwNDNiXFx1MDQzNVxcdTA0MzRcXHUwNDNlXFx1MDQzMlxcdTA0MzBcXHUwNDQyXFx1MDQzNVxcdTA0M2JcXHUwNDRjXFx1MDQ0MVxcdTA0M2FcXHUwNDM4XFx1MDQzOSBcXHUwNDQ2XFx1MDQzNVxcdTA0M2RcXHUwNDQyXFx1MDQ0MCBcXFwidGVzdDFcXFwiIFxcdTA0MzFcXHUwNDRiXFx1MDQzYiBcXHUwNDQzXFx1MDQ0MVxcdTA0M2ZcXHUwNDM1XFx1MDQ0OFxcdTA0M2RcXHUwNDNlIFxcdTA0MzhcXHUwNDM3XFx1MDQzY1xcdTA0MzVcXHUwNDNkXFx1MDQzNVxcdTA0M2QuXCJdLFtcIl9fanNvbl9tZXNzYWdlXCIsMCwyNSxcIlxcdTA0MThcXHUwNDQxXFx1MDQ0MVxcdTA0M2JcXHUwNDM1XFx1MDQzNFxcdTA0M2VcXHUwNDMyXFx1MDQzMFxcdTA0NDJcXHUwNDM1XFx1MDQzYlxcdTA0NGNcXHUwNDQxXFx1MDQzYVxcdTA0MzhcXHUwNDM5IFxcdTA0NDZcXHUwNDM1XFx1MDQzZFxcdTA0NDJcXHUwNDQwIFxcXCJ0ZXN0MVxcXCIgXFx1MDQzMVxcdTA0NGJcXHUwNDNiIFxcdTA0NDNcXHUwNDQxXFx1MDQzZlxcdTA0MzVcXHUwNDQ4XFx1MDQzZFxcdTA0M2UgXFx1MDQzOFxcdTA0MzdcXHUwNDNjXFx1MDQzNVxcdTA0M2RcXHUwNDM1XFx1MDQzZC5cIl0sW1wiX19qc29uX21lc3NhZ2VcIiwwLDI1LFwiXFx1MDQxY1xcdTA0M2VcXHUwNDQ5XFx1MDQzZFxcdTA0M2VcXHUwNDQxXFx1MDQ0MlxcdTA0NGMgXFxcIjNcXFwiIFxcdTA0MzFcXHUwNDRiXFx1MDQzYiBcXHUwNDQzXFx1MDQ0MVxcdTA0M2ZcXHUwNDM1XFx1MDQ0OFxcdTA0M2RcXHUwNDNlIFxcdTA0MzRcXHUwNDNlXFx1MDQzMVxcdTA0MzBcXHUwNDMyXFx1MDQzYlxcdTA0MzVcXHUwNDNkLlwiXSxbXCJfX2pzb25fbWVzc2FnZVwiLDAsMjUsXCJcXHUwNDEyXFx1MDQzOFxcdTA0MzQgXFx1MDQzZVxcdTA0MzFcXHUwNDQxXFx1MDQzYlxcdTA0MzVcXHUwNDM0XFx1MDQzZVxcdTA0MzJcXHUwNDMwXFx1MDQzZFxcdTA0MzhcXHUwNDRmIFxcXCJcXHUwNDFjXFx1MDQyMFxcdTA0MjIgXFx1MDQzM1xcdTA0M2VcXHUwNDNiXFx1MDQzZVxcdTA0MzJcXHUwNDNkXFx1MDQzZVxcdTA0MzNcXHUwNDNlIFxcdTA0M2NcXHUwNDNlXFx1MDQzN1xcdTA0MzNcXHUwNDMwXFxcIiBcXHUwNDMxXFx1MDQ0YlxcdTA0M2IgXFx1MDQ0M1xcdTA0NDFcXHUwNDNmXFx1MDQzNVxcdTA0NDhcXHUwNDNkXFx1MDQzZSBcXHUwNDM0XFx1MDQzZVxcdTA0MzFcXHUwNDMwXFx1MDQzMlxcdTA0M2JcXHUwNDM1XFx1MDQzZC5cIl0sW1wiX19qc29uX21lc3NhZ2VcIiwwLDI1LFwiXFx1MDQxMlxcdTA0MzhcXHUwNDM0IFxcdTA0M2VcXHUwNDMxXFx1MDQ0MVxcdTA0M2JcXHUwNDM1XFx1MDQzNFxcdTA0M2VcXHUwNDMyXFx1MDQzMFxcdTA0M2RcXHUwNDM4XFx1MDQ0ZiBcXFwiXFx1MDQxY1xcdTA0MjBcXHUwNDIyIFxcdTA0MzNcXHUwNDM4XFx1MDQzZlxcdTA0M2VcXHUwNDQ0XFx1MDQzOFxcdTA0MzdcXHUwNDMwXFxcIiBcXHUwNDMxXFx1MDQ0YlxcdTA0M2IgXFx1MDQ0M1xcdTA0NDFcXHUwNDNmXFx1MDQzNVxcdTA0NDhcXHUwNDNkXFx1MDQzZSBcXHUwNDM0XFx1MDQzZVxcdTA0MzFcXHUwNDMwXFx1MDQzMlxcdTA0M2JcXHUwNDM1XFx1MDQzZC5cIl0sW1wiX19qc29uX21lc3NhZ2VcIiwwLDI1LFwiXFx1MDQxMlxcdTA0MzhcXHUwNDM0IFxcdTA0M2VcXHUwNDMxXFx1MDQ0MVxcdTA0M2JcXHUwNDM1XFx1MDQzNFxcdTA0M2VcXHUwNDMyXFx1MDQzMFxcdTA0M2RcXHUwNDM4XFx1MDQ0ZiBcXFwiXFx1MDQxY1xcdTA0MjBcXHUwNDIyIFxcdTA0MzNcXHUwNDNiXFx1MDQzMFxcdTA0MzdcXHUwNDNkXFx1MDQ0YlxcdTA0NDUgXFx1MDQzZVxcdTA0NDBcXHUwNDMxXFx1MDQzOFxcdTA0NDJcXFwiIFxcdTA0MzFcXHUwNDRiXFx1MDQzYiBcXHUwNDQzXFx1MDQ0MVxcdTA0M2ZcXHUwNDM1XFx1MDQ0OFxcdTA0M2RcXHUwNDNlIFxcdTA0MzRcXHUwNDNlXFx1MDQzMVxcdTA0MzBcXHUwNDMyXFx1MDQzYlxcdTA0MzVcXHUwNDNkLlwiXSxbXCJfX2pzb25fbWVzc2FnZVwiLDAsMjUsXCJcXHUwNDEyXFx1MDQzOFxcdTA0MzQgXFx1MDQzZVxcdTA0MzFcXHUwNDQxXFx1MDQzYlxcdTA0MzVcXHUwNDM0XFx1MDQzZVxcdTA0MzJcXHUwNDMwXFx1MDQzZFxcdTA0MzhcXHUwNDRmIFxcXCJcXHUwNDFjXFx1MDQyMFxcdTA0MjIgXFx1MDQzZlxcdTA0MzBcXHUwNDM3XFx1MDQ0M1xcdTA0NDUgXFx1MDQzZFxcdTA0M2VcXHUwNDQxXFx1MDQzMFxcXCIgXFx1MDQzMVxcdTA0NGJcXHUwNDNiIFxcdTA0NDNcXHUwNDQxXFx1MDQzZlxcdTA0MzVcXHUwNDQ4XFx1MDQzZFxcdTA0M2UgXFx1MDQzNFxcdTA0M2VcXHUwNDMxXFx1MDQzMFxcdTA0MzJcXHUwNDNiXFx1MDQzNVxcdTA0M2QuXCJdLFtcIl9fanNvbl9tZXNzYWdlXCIsMCwyNSxcIlxcdTA0MTJcXHUwNDM4XFx1MDQzNCBcXHUwNDNlXFx1MDQzMVxcdTA0NDFcXHUwNDNiXFx1MDQzNVxcdTA0MzRcXHUwNDNlXFx1MDQzMlxcdTA0MzBcXHUwNDNkXFx1MDQzOFxcdTA0NGYgXFxcIlxcdTA0MWNcXHUwNDIwXFx1MDQyMiBcXHUwNDNmXFx1MDQzZVxcdTA0MzdcXHUwNDMyXFx1MDQzZVxcdTA0M2RcXHUwNDNlXFx1MDQ0N1xcdTA0M2RcXHUwNDM4XFx1MDQzYVxcdTA0MzBcXFwiIFxcdTA0MzFcXHUwNDRiXFx1MDQzYiBcXHUwNDQzXFx1MDQ0MVxcdTA0M2ZcXHUwNDM1XFx1MDQ0OFxcdTA0M2RcXHUwNDNlIFxcdTA0MzRcXHUwNDNlXFx1MDQzMVxcdTA0MzBcXHUwNDMyXFx1MDQzYlxcdTA0MzVcXHUwNDNkLlwiXSxbXCJfX2pzb25fbWVzc2FnZVwiLDAsMjUsXCJcXHUwNDEyXFx1MDQzOFxcdTA0MzQgXFx1MDQzZVxcdTA0MzFcXHUwNDQxXFx1MDQzYlxcdTA0MzVcXHUwNDM0XFx1MDQzZVxcdTA0MzJcXHUwNDMwXFx1MDQzZFxcdTA0MzhcXHUwNDRmIFxcXCJcXHUwNDFjXFx1MDQyMFxcdTA0MjIgXFx1MDQ0OFxcdTA0MzVcXHUwNDM5XFx1MDQzZFxcdTA0M2VcXHUwNDMzXFx1MDQzZSBcXHUwNDNlXFx1MDQ0MlxcdTA0MzRcXHUwNDM1XFx1MDQzYlxcdTA0MzBcXFwiIFxcdTA0MzFcXHUwNDRiXFx1MDQzYiBcXHUwNDQzXFx1MDQ0MVxcdTA0M2ZcXHUwNDM1XFx1MDQ0OFxcdTA0M2RcXHUwNDNlIFxcdTA0MzRcXHUwNDNlXFx1MDQzMVxcdTA0MzBcXHUwNDMyXFx1MDQzYlxcdTA0MzVcXHUwNDNkLlwiXSxbXCJfX2pzb25fbWVzc2FnZVwiLDAsMjUsXCJcXHUwNDEyXFx1MDQzOFxcdTA0MzQgXFx1MDQzZVxcdTA0MzFcXHUwNDQxXFx1MDQzYlxcdTA0MzVcXHUwNDM0XFx1MDQzZVxcdTA0MzJcXHUwNDMwXFx1MDQzZFxcdTA0MzhcXHUwNDRmIFxcXCJcXHUwNDFjXFx1MDQyMFxcdTA0MjIgXFx1MDQzM1xcdTA0NDBcXHUwNDQzXFx1MDQzNFxcdTA0M2RcXHUwNDNlXFx1MDQzM1xcdTA0M2UgXFx1MDQzZVxcdTA0NDJcXHUwNDM0XFx1MDQzNVxcdTA0M2JcXHUwNDMwXFxcIiBcXHUwNDMxXFx1MDQ0YlxcdTA0M2IgXFx1MDQ0M1xcdTA0NDFcXHUwNDNmXFx1MDQzNVxcdTA0NDhcXHUwNDNkXFx1MDQzZSBcXHUwNDM0XFx1MDQzZVxcdTA0MzFcXHUwNDMwXFx1MDQzMlxcdTA0M2JcXHUwNDM1XFx1MDQzZC5cIl0sW1wiX19qc29uX21lc3NhZ2VcIiwwLDI1LFwiXFx1MDQxMlxcdTA0MzhcXHUwNDM0IFxcdTA0M2VcXHUwNDMxXFx1MDQ0MVxcdTA0M2JcXHUwNDM1XFx1MDQzNFxcdTA0M2VcXHUwNDMyXFx1MDQzMFxcdTA0M2RcXHUwNDM4XFx1MDQ0ZiBcXFwiXFx1MDQxY1xcdTA0MjBcXHUwNDIyIFxcdTA0M2ZcXHUwNDNlXFx1MDQ0ZlxcdTA0NDFcXHUwNDNkXFx1MDQzOFxcdTA0NDdcXHUwNDNkXFx1MDQzZS1cXHUwNDNhXFx1MDQ0MFxcdTA0MzVcXHUwNDQxXFx1MDQ0MlxcdTA0NDZcXHUwNDNlXFx1MDQzMlxcdTA0M2VcXHUwNDMzXFx1MDQzZSBcXHUwNDNlXFx1MDQ0MlxcdTA0MzRcXHUwNDM1XFx1MDQzYlxcdTA0MzBcXFwiIFxcdTA0MzFcXHUwNDRiXFx1MDQzYiBcXHUwNDQzXFx1MDQ0MVxcdTA0M2ZcXHUwNDM1XFx1MDQ0OFxcdTA0M2RcXHUwNDNlIFxcdTA0MzRcXHUwNDNlXFx1MDQzMVxcdTA0MzBcXHUwNDMyXFx1MDQzYlxcdTA0MzVcXHUwNDNkLlwiXSxbXCJfX2pzb25fbWVzc2FnZVwiLDAsMjUsXCJcXHUwNDEyXFx1MDQzOFxcdTA0MzQgXFx1MDQzZVxcdTA0MzFcXHUwNDQxXFx1MDQzYlxcdTA0MzVcXHUwNDM0XFx1MDQzZVxcdTA0MzJcXHUwNDMwXFx1MDQzZFxcdTA0MzhcXHUwNDRmIFxcXCJcXHUwNDFjXFx1MDQyMFxcdTA0MjIgXFx1MDQzYVxcdTA0NDBcXHUwNDM1XFx1MDQ0MVxcdTA0NDJcXHUwNDQ2XFx1MDQzZVxcdTA0MzJcXHUwNDNlLVxcdTA0M2ZcXHUwNDNlXFx1MDQzNFxcdTA0MzJcXHUwNDM3XFx1MDQzNFxcdTA0M2VcXHUwNDQ4XFx1MDQzZFxcdTA0NGJcXHUwNDQ1IFxcdTA0NDFcXHUwNDNlXFx1MDQ0N1xcdTA0M2JcXHUwNDM1XFx1MDQzZFxcdTA0MzVcXHUwNDNkXFx1MDQzOFxcdTA0MzlcXFwiIFxcdTA0MzFcXHUwNDRiXFx1MDQzYiBcXHUwNDQzXFx1MDQ0MVxcdTA0M2ZcXHUwNDM1XFx1MDQ0OFxcdTA0M2RcXHUwNDNlIFxcdTA0MzRcXHUwNDNlXFx1MDQzMVxcdTA0MzBcXHUwNDMyXFx1MDQzYlxcdTA0MzVcXHUwNDNkLlwiXSxbXCJfX2pzb25fbWVzc2FnZVwiLDAsMjUsXCJcXHUwNDEyXFx1MDQzOFxcdTA0MzQgXFx1MDQzZVxcdTA0MzFcXHUwNDQxXFx1MDQzYlxcdTA0MzVcXHUwNDM0XFx1MDQzZVxcdTA0MzJcXHUwNDMwXFx1MDQzZFxcdTA0MzhcXHUwNDRmIFxcXCJcXHUwNDFjXFx1MDQyMFxcdTA0MjIgXFx1MDQzYVxcdTA0M2VcXHUwNDNmXFx1MDQ0N1xcdTA0MzhcXHUwNDNhXFx1MDQzZVxcdTA0MzJcXHUwNDNlXFx1MDQzOSBcXHUwNDNlXFx1MDQzMVxcdTA0M2JcXHUwNDMwXFx1MDQ0MVxcdTA0NDJcXHUwNDM4XFxcIiBcXHUwNDMxXFx1MDQ0YlxcdTA0M2IgXFx1MDQ0M1xcdTA0NDFcXHUwNDNmXFx1MDQzNVxcdTA0NDhcXHUwNDNkXFx1MDQzZSBcXHUwNDM0XFx1MDQzZVxcdTA0MzFcXHUwNDMwXFx1MDQzMlxcdTA0M2JcXHUwNDM1XFx1MDQzZC5cIl0sW1wiX19qc29uX21lc3NhZ2VcIiwwLDI1LFwiXFx1MDQxMlxcdTA0MzhcXHUwNDM0IFxcdTA0M2VcXHUwNDMxXFx1MDQ0MVxcdTA0M2JcXHUwNDM1XFx1MDQzNFxcdTA0M2VcXHUwNDMyXFx1MDQzMFxcdTA0M2RcXHUwNDM4XFx1MDQ0ZiBcXFwiXFx1MDQxY1xcdTA0MjBcXHUwNDIyIFxcdTA0MzJcXHUwNDNkXFx1MDQ0M1xcdTA0NDJcXHUwNDQwXFx1MDQzNVxcdTA0M2RcXHUwNDNkXFx1MDQzOFxcdTA0NDUgXFx1MDQzZVxcdTA0NDBcXHUwNDMzXFx1MDQzMFxcdTA0M2RcXHUwNDNlXFx1MDQzMlxcXCIgXFx1MDQzMVxcdTA0NGJcXHUwNDNiIFxcdTA0NDNcXHUwNDQxXFx1MDQzZlxcdTA0MzVcXHUwNDQ4XFx1MDQzZFxcdTA0M2UgXFx1MDQzNFxcdTA0M2VcXHUwNDMxXFx1MDQzMFxcdTA0MzJcXHUwNDNiXFx1MDQzNVxcdTA0M2QuXCJdLFtcIl9fanNvbl9tZXNzYWdlXCIsMCwyNSxcIlxcdTA0MTJcXHUwNDM4XFx1MDQzNCBcXHUwNDNlXFx1MDQzMVxcdTA0NDFcXHUwNDNiXFx1MDQzNVxcdTA0MzRcXHUwNDNlXFx1MDQzMlxcdTA0MzBcXHUwNDNkXFx1MDQzOFxcdTA0NGYgXFxcIlxcdTA0MWNcXHUwNDIwXFx1MDQyMiBcXHUwNDNlXFx1MDQ0MFxcdTA0MzNcXHUwNDMwXFx1MDQzZFxcdTA0M2VcXHUwNDMyIFxcdTA0MzNcXHUwNDQwXFx1MDQ0M1xcdTA0MzRcXHUwNDNkXFx1MDQzZVxcdTA0MzkgXFx1MDQzYVxcdTA0M2JcXHUwNDM1XFx1MDQ0MlxcdTA0M2FcXHUwNDM4IFxcdTA0MzggXFx1MDQ0MVxcdTA0NDBcXHUwNDM1XFx1MDQzNFxcdTA0M2VcXHUwNDQxXFx1MDQ0MlxcdTA0MzVcXHUwNDNkXFx1MDQzOFxcdTA0NGZcXFwiIFxcdTA0MzFcXHUwNDRiXFx1MDQzYiBcXHUwNDQzXFx1MDQ0MVxcdTA0M2ZcXHUwNDM1XFx1MDQ0OFxcdTA0M2RcXHUwNDNlIFxcdTA0MzRcXHUwNDNlXFx1MDQzMVxcdTA0MzBcXHUwNDMyXFx1MDQzYlxcdTA0MzVcXHUwNDNkLlwiXSxbXCJfX2pzb25fbWVzc2FnZVwiLDAsMjUsXCJcXHUwNDEyXFx1MDQzOFxcdTA0MzQgXFx1MDQzZVxcdTA0MzFcXHUwNDQxXFx1MDQzYlxcdTA0MzVcXHUwNDM0XFx1MDQzZVxcdTA0MzJcXHUwNDMwXFx1MDQzZFxcdTA0MzhcXHUwNDRmIFxcXCJcXHUwNDFjXFx1MDQyMFxcdTA0MjIgXFx1MDQzY1xcdTA0M2VcXHUwNDNiXFx1MDQzZVxcdTA0NDdcXHUwNDNkXFx1MDQzZVxcdTA0MzkgXFx1MDQzNlxcdTA0MzVcXHUwNDNiXFx1MDQzNVxcdTA0MzdcXHUwNDRiXFxcIiBcXHUwNDMxXFx1MDQ0YlxcdTA0M2IgXFx1MDQ0M1xcdTA0NDFcXHUwNDNmXFx1MDQzNVxcdTA0NDhcXHUwNDNkXFx1MDQzZSBcXHUwNDM0XFx1MDQzZVxcdTA0MzFcXHUwNDMwXFx1MDQzMlxcdTA0M2JcXHUwNDM1XFx1MDQzZC5cIl0sW1wiX19qc29uX21lc3NhZ2VcIiwwLDI1LFwiXFx1MDQxMlxcdTA0MzhcXHUwNDM0IFxcdTA0M2VcXHUwNDMxXFx1MDQ0MVxcdTA0M2JcXHUwNDM1XFx1MDQzNFxcdTA0M2VcXHUwNDMyXFx1MDQzMFxcdTA0M2RcXHUwNDM4XFx1MDQ0ZiBcXFwiXFx1MDQxY1xcdTA0MjBcXHUwNDIyIFxcdTA0M2VcXHUwNDQwXFx1MDQzM1xcdTA0MzBcXHUwNDNkXFx1MDQzZVxcdTA0MzIgXFx1MDQzMVxcdTA0NDBcXHUwNDRlXFx1MDQ0OFxcdTA0M2RcXHUwNDNlXFx1MDQzOSBcXHUwNDNmXFx1MDQzZVxcdTA0M2JcXHUwNDNlXFx1MDQ0MVxcdTA0NDJcXHUwNDM4XFxcIiBcXHUwNDMxXFx1MDQ0YlxcdTA0M2IgXFx1MDQ0M1xcdTA0NDFcXHUwNDNmXFx1MDQzNVxcdTA0NDhcXHUwNDNkXFx1MDQzZSBcXHUwNDM0XFx1MDQzZVxcdTA0MzFcXHUwNDMwXFx1MDQzMlxcdTA0M2JcXHUwNDM1XFx1MDQzZC5cIl0sW1wiX19qc29uX21lc3NhZ2VcIiwwLDI1LFwiXFx1MDQxMlxcdTA0MzhcXHUwNDM0IFxcdTA0M2VcXHUwNDMxXFx1MDQ0MVxcdTA0M2JcXHUwNDM1XFx1MDQzNFxcdTA0M2VcXHUwNDMyXFx1MDQzMFxcdTA0M2RcXHUwNDM4XFx1MDQ0ZiBcXFwiXFx1MDQxY1xcdTA0MjBcXHUwNDIyIFxcdTA0M2VcXHUwNDQwXFx1MDQzM1xcdTA0MzBcXHUwNDNkXFx1MDQzZVxcdTA0MzIgXFx1MDQzN1xcdTA0MzBcXHUwNDMxXFx1MDQ0MFxcdTA0NGVcXHUwNDQ4XFx1MDQzOFxcdTA0M2RcXHUwNDNkXFx1MDQzZVxcdTA0MzNcXHUwNDNlIFxcdTA0M2ZcXHUwNDQwXFx1MDQzZVxcdTA0NDFcXHUwNDQyXFx1MDQ0MFxcdTA0MzBcXHUwNDNkXFx1MDQ0MVxcdTA0NDJcXHUwNDMyXFx1MDQzMFxcXCIgXFx1MDQzMVxcdTA0NGJcXHUwNDNiIFxcdTA0NDNcXHUwNDQxXFx1MDQzZlxcdTA0MzVcXHUwNDQ4XFx1MDQzZFxcdTA0M2UgXFx1MDQzNFxcdTA0M2VcXHUwNDMxXFx1MDQzMFxcdTA0MzJcXHUwNDNiXFx1MDQzNVxcdTA0M2QuXCJdLFtcIl9fanNvbl9tZXNzYWdlXCIsMCwyNSxcIlxcdTA0MTJcXHUwNDM4XFx1MDQzNCBcXHUwNDNlXFx1MDQzMVxcdTA0NDFcXHUwNDNiXFx1MDQzNVxcdTA0MzRcXHUwNDNlXFx1MDQzMlxcdTA0MzBcXHUwNDNkXFx1MDQzOFxcdTA0NGYgXFxcIlxcdTA0MWNcXHUwNDIwLVxcdTA0NDVcXHUwNDNlXFx1MDQzYlxcdTA0MzBcXHUwNDNkXFx1MDQzM1xcdTA0MzhcXHUwNDNlXFx1MDQzM1xcdTA0NDBcXHUwNDMwXFx1MDQ0NFxcdTA0MzhcXHUwNDRmXFxcIiBcXHUwNDMxXFx1MDQ0YlxcdTA0M2IgXFx1MDQ0M1xcdTA0NDFcXHUwNDNmXFx1MDQzNVxcdTA0NDhcXHUwNDNkXFx1MDQzZSBcXHUwNDM0XFx1MDQzZVxcdTA0MzFcXHUwNDMwXFx1MDQzMlxcdTA0M2JcXHUwNDM1XFx1MDQzZC5cIl0sW1wiX19qc29uX21lc3NhZ2VcIiwwLDI1LFwiXFx1MDQxMlxcdTA0MzhcXHUwNDM0IFxcdTA0M2VcXHUwNDMxXFx1MDQ0MVxcdTA0M2JcXHUwNDM1XFx1MDQzNFxcdTA0M2VcXHUwNDMyXFx1MDQzMFxcdTA0M2RcXHUwNDM4XFx1MDQ0ZiBcXFwiXFx1MDQxY1xcdTA0MjBcXHUwNDIyIFxcdTA0M2VcXHUwNDQwXFx1MDQzM1xcdTA0MzBcXHUwNDNkXFx1MDQzZVxcdTA0MzIgXFx1MDQzY1xcdTA0MzBcXHUwNDNiXFx1MDQzZVxcdTA0MzNcXHUwNDNlIFxcdTA0NDJcXHUwNDMwXFx1MDQzN1xcdTA0MzBcXFwiIFxcdTA0MzFcXHUwNDRiXFx1MDQzYiBcXHUwNDQzXFx1MDQ0MVxcdTA0M2ZcXHUwNDM1XFx1MDQ0OFxcdTA0M2RcXHUwNDNlIFxcdTA0MzRcXHUwNDNlXFx1MDQzMVxcdTA0MzBcXHUwNDMyXFx1MDQzYlxcdTA0MzVcXHUwNDNkLlwiXSxbXCJfX2pzb25fbWVzc2FnZVwiLDAsMjUsXCJcXHUwNDEyXFx1MDQzOFxcdTA0MzQgXFx1MDQzZVxcdTA0MzFcXHUwNDQxXFx1MDQzYlxcdTA0MzVcXHUwNDM0XFx1MDQzZVxcdTA0MzJcXHUwNDMwXFx1MDQzZFxcdTA0MzhcXHUwNDRmIFxcXCJcXHUwNDFjXFx1MDQyMFxcdTA0MjIgXFx1MDQzYVxcdTA0MzhcXHUwNDQ4XFx1MDQzNVxcdTA0NDdcXHUwNDNkXFx1MDQzOFxcdTA0M2FcXHUwNDMwXFxcIiBcXHUwNDMxXFx1MDQ0YlxcdTA0M2IgXFx1MDQ0M1xcdTA0NDFcXHUwNDNmXFx1MDQzNVxcdTA0NDhcXHUwNDNkXFx1MDQzZSBcXHUwNDM0XFx1MDQzZVxcdTA0MzFcXHUwNDMwXFx1MDQzMlxcdTA0M2JcXHUwNDM1XFx1MDQzZC5cIl0sW1wiX19qc29uX21lc3NhZ2VcIiwwLDI1LFwiXFx1MDQxMlxcdTA0MzhcXHUwNDM0IFxcdTA0M2VcXHUwNDMxXFx1MDQ0MVxcdTA0M2JcXHUwNDM1XFx1MDQzNFxcdTA0M2VcXHUwNDMyXFx1MDQzMFxcdTA0M2RcXHUwNDM4XFx1MDQ0ZiBcXFwiXFx1MDQxY1xcdTA0MjBcXHUwNDIyIFxcdTA0M2NcXHUwNDRmXFx1MDQzM1xcdTA0M2FcXHUwNDM4XFx1MDQ0NSBcXHUwNDQyXFx1MDQzYVxcdTA0MzBcXHUwNDNkXFx1MDQzNVxcdTA0MzlcXFwiIFxcdTA0MzFcXHUwNDRiXFx1MDQzYiBcXHUwNDQzXFx1MDQ0MVxcdTA0M2ZcXHUwNDM1XFx1MDQ0OFxcdTA0M2RcXHUwNDNlIFxcdTA0MzRcXHUwNDNlXFx1MDQzMVxcdTA0MzBcXHUwNDMyXFx1MDQzYlxcdTA0MzVcXHUwNDNkLlwiXSxbXCJfX2pzb25fbWVzc2FnZVwiLDAsMjUsXCJcXHUwNDEyXFx1MDQzOFxcdTA0MzQgXFx1MDQzZVxcdTA0MzFcXHUwNDQxXFx1MDQzYlxcdTA0MzVcXHUwNDM0XFx1MDQzZVxcdTA0MzJcXHUwNDMwXFx1MDQzZFxcdTA0MzhcXHUwNDRmIFxcXCJcXHUwNDFjXFx1MDQyMFxcdTA0MjIgXFx1MDQzY1xcdTA0NGZcXHUwNDMzXFx1MDQzYVxcdTA0MzhcXHUwNDQ1IFxcdTA0NDJcXHUwNDNhXFx1MDQzMFxcdTA0M2RcXHUwNDM1XFx1MDQzOVxcXCIgXFx1MDQzMVxcdTA0NGJcXHUwNDNiIFxcdTA0NDNcXHUwNDQxXFx1MDQzZlxcdTA0MzVcXHUwNDQ4XFx1MDQzZFxcdTA0M2UgXFx1MDQzOFxcdTA0MzdcXHUwNDNjXFx1MDQzNVxcdTA0M2RcXHUwNDM1XFx1MDQzZC5cIl0sW1wiX19qc29uX21lc3NhZ2VcIiwwLDI1LFwiXFx1MDQxMlxcdTA0MzhcXHUwNDM0IFxcdTA0M2VcXHUwNDMxXFx1MDQ0MVxcdTA0M2JcXHUwNDM1XFx1MDQzNFxcdTA0M2VcXHUwNDMyXFx1MDQzMFxcdTA0M2RcXHUwNDM4XFx1MDQ0ZiBcXFwiXFx1MDQxY1xcdTA0MjBcXHUwNDIyIFxcdTA0M2NcXHUwNDRmXFx1MDQzM1xcdTA0M2FcXHUwNDM4XFx1MDQ0NSBcXHUwNDQyXFx1MDQzYVxcdTA0MzBcXHUwNDNkXFx1MDQzNVxcdTA0MzkgXFx1MDQzOCBcXHUwNDNlXFx1MDQ0MFxcdTA0MzNcXHUwNDMwXFx1MDQzZFxcdTA0M2VcXHUwNDMyIFxcdTA0NDhcXHUwNDM1XFx1MDQzOFxcXCIgXFx1MDQzMVxcdTA0NGJcXHUwNDNiIFxcdTA0NDNcXHUwNDQxXFx1MDQzZlxcdTA0MzVcXHUwNDQ4XFx1MDQzZFxcdTA0M2UgXFx1MDQzNFxcdTA0M2VcXHUwNDMxXFx1MDQzMFxcdTA0MzJcXHUwNDNiXFx1MDQzNVxcdTA0M2QuXCJdLFtcIl9fanNvbl9tZXNzYWdlXCIsMCwyNSxcIlxcdTA0MTJcXHUwNDM4XFx1MDQzNCBcXHUwNDNlXFx1MDQzMVxcdTA0NDFcXHUwNDNiXFx1MDQzNVxcdTA0MzRcXHUwNDNlXFx1MDQzMlxcdTA0MzBcXHUwNDNkXFx1MDQzOFxcdTA0NGYgXFxcIlxcdTA0MWNcXHUwNDIwXFx1MDQyMiBcXHUwNDQyXFx1MDQzYVxcdTA0MzBcXHUwNDNkXFx1MDQzNVxcdTA0MzkgXFx1MDQzYVxcdTA0M2VcXHUwNDNkXFx1MDQzNVxcdTA0NDdcXHUwNDNkXFx1MDQzZVxcdTA0NDFcXHUwNDQyXFx1MDQzOFxcXCIgXFx1MDQzMVxcdTA0NGJcXHUwNDNiIFxcdTA0NDNcXHUwNDQxXFx1MDQzZlxcdTA0MzVcXHUwNDQ4XFx1MDQzZFxcdTA0M2UgXFx1MDQzNFxcdTA0M2VcXHUwNDMxXFx1MDQzMFxcdTA0MzJcXHUwNDNiXFx1MDQzNVxcdTA0M2QuXCJdLFtcIl9fanNvbl9tZXNzYWdlXCIsMCwyNSxcIlxcdTA0MTJcXHUwNDM4XFx1MDQzNCBcXHUwNDNlXFx1MDQzMVxcdTA0NDFcXHUwNDNiXFx1MDQzNVxcdTA0MzRcXHUwNDNlXFx1MDQzMlxcdTA0MzBcXHUwNDNkXFx1MDQzOFxcdTA0NGYgXFxcIlxcdTA0MWNcXHUwNDIwXFx1MDQyMiBcXHUwNDQxXFx1MDQ0M1xcdTA0NDFcXHUwNDQyXFx1MDQzMFxcdTA0MzJcXHUwNDNlXFx1MDQzMlxcXCIgXFx1MDQzMVxcdTA0NGJcXHUwNDNiIFxcdTA0NDNcXHUwNDQxXFx1MDQzZlxcdTA0MzVcXHUwNDQ4XFx1MDQzZFxcdTA0M2UgXFx1MDQzNFxcdTA0M2VcXHUwNDMxXFx1MDQzMFxcdTA0MzJcXHUwNDNiXFx1MDQzNVxcdTA0M2QuXCJdLFtcIl9fanNvbl9tZXNzYWdlXCIsMCwyNSxcIlxcdTA0MTJcXHUwNDM4XFx1MDQzNCBcXHUwNDNlXFx1MDQzMVxcdTA0NDFcXHUwNDNiXFx1MDQzNVxcdTA0MzRcXHUwNDNlXFx1MDQzMlxcdTA0MzBcXHUwNDNkXFx1MDQzOFxcdTA0NGYgXFxcIlxcdTA0MWNcXHUwNDIwXFx1MDQyMiBcXHUwNDMyXFx1MDQzOFxcdTA0NDFcXHUwNDNlXFx1MDQ0N1xcdTA0M2RcXHUwNDNlLVxcdTA0M2RcXHUwNDM4XFx1MDQzNlxcdTA0M2RcXHUwNDM1XFx1MDQ0N1xcdTA0MzVcXHUwNDNiXFx1MDQ0ZVxcdTA0NDFcXHUwNDQyXFx1MDQzZFxcdTA0M2VcXHUwNDMzXFx1MDQzZSBcXHUwNDQxXFx1MDQ0M1xcdTA0NDFcXHUwNDQyXFx1MDQzMFxcdTA0MzJcXHUwNDMwXFxcIiBcXHUwNDMxXFx1MDQ0YlxcdTA0M2IgXFx1MDQ0M1xcdTA0NDFcXHUwNDNmXFx1MDQzNVxcdTA0NDhcXHUwNDNkXFx1MDQzZSBcXHUwNDM0XFx1MDQzZVxcdTA0MzFcXHUwNDMwXFx1MDQzMlxcdTA0M2JcXHUwNDM1XFx1MDQzZC5cIl0sW1wiX19qc29uX21lc3NhZ2VcIiwwLDI1LFwiXFx1MDQxMlxcdTA0MzhcXHUwNDM0IFxcdTA0M2VcXHUwNDMxXFx1MDQ0MVxcdTA0M2JcXHUwNDM1XFx1MDQzNFxcdTA0M2VcXHUwNDMyXFx1MDQzMFxcdTA0M2RcXHUwNDM4XFx1MDQ0ZiBcXFwiXFx1MDQxY1xcdTA0MjBcXHUwNDIyIFxcdTA0M2ZcXHUwNDNiXFx1MDQzNVxcdTA0NDdcXHUwNDM1XFx1MDQzMlxcdTA0M2VcXHUwNDMzXFx1MDQzZSBcXHUwNDQxXFx1MDQ0M1xcdTA0NDFcXHUwNDQyXFx1MDQzMFxcdTA0MzJcXHUwNDMwXFxcIiBcXHUwNDMxXFx1MDQ0YlxcdTA0M2IgXFx1MDQ0M1xcdTA0NDFcXHUwNDNmXFx1MDQzNVxcdTA0NDhcXHUwNDNkXFx1MDQzZSBcXHUwNDM0XFx1MDQzZVxcdTA0MzFcXHUwNDMwXFx1MDQzMlxcdTA0M2JcXHUwNDM1XFx1MDQzZC5cIl0sW1wiX19qc29uX21lc3NhZ2VcIiwwLDI1LFwiXFx1MDQxMlxcdTA0MzhcXHUwNDM0IFxcdTA0M2VcXHUwNDMxXFx1MDQ0MVxcdTA0M2JcXHUwNDM1XFx1MDQzNFxcdTA0M2VcXHUwNDMyXFx1MDQzMFxcdTA0M2RcXHUwNDM4XFx1MDQ0ZiBcXFwiXFx1MDQxY1xcdTA0MjBcXHUwNDIyIFxcdTA0M2JcXHUwNDNlXFx1MDQzYVxcdTA0NDJcXHUwNDM1XFx1MDQzMlxcdTA0M2VcXHUwNDMzXFx1MDQzZSBcXHUwNDQxXFx1MDQ0M1xcdTA0NDFcXHUwNDQyXFx1MDQzMFxcdTA0MzJcXHUwNDMwXFxcIiBcXHUwNDMxXFx1MDQ0YlxcdTA0M2IgXFx1MDQ0M1xcdTA0NDFcXHUwNDNmXFx1MDQzNVxcdTA0NDhcXHUwNDNkXFx1MDQzZSBcXHUwNDM0XFx1MDQzZVxcdTA0MzFcXHUwNDMwXFx1MDQzMlxcdTA0M2JcXHUwNDM1XFx1MDQzZC5cIl0sW1wiX19qc29uX21lc3NhZ2VcIiwwLDI1LFwiXFx1MDQxMlxcdTA0MzhcXHUwNDM0IFxcdTA0M2VcXHUwNDMxXFx1MDQ0MVxcdTA0M2JcXHUwNDM1XFx1MDQzNFxcdTA0M2VcXHUwNDMyXFx1MDQzMFxcdTA0M2RcXHUwNDM4XFx1MDQ0ZiBcXFwiXFx1MDQxY1xcdTA0MjBcXHUwNDIyIFxcdTA0M2JcXHUwNDQzXFx1MDQ0N1xcdTA0MzVcXHUwNDM3XFx1MDQzMFxcdTA0M2ZcXHUwNDRmXFx1MDQ0MVxcdTA0NDJcXHUwNDNkXFx1MDQzZVxcdTA0MzNcXHUwNDNlIFxcdTA0NDFcXHUwNDQzXFx1MDQ0MVxcdTA0NDJcXHUwNDMwXFx1MDQzMlxcdTA0MzBcXFwiIFxcdTA0MzFcXHUwNDRiXFx1MDQzYiBcXHUwNDQzXFx1MDQ0MVxcdTA0M2ZcXHUwNDM1XFx1MDQ0OFxcdTA0M2RcXHUwNDNlIFxcdTA0MzRcXHUwNDNlXFx1MDQzMVxcdTA0MzBcXHUwNDMyXFx1MDQzYlxcdTA0MzVcXHUwNDNkLlwiXSxbXCJfX2pzb25fbWVzc2FnZVwiLDAsMjUsXCJcXHUwNDEyXFx1MDQzOFxcdTA0MzQgXFx1MDQzZVxcdTA0MzFcXHUwNDQxXFx1MDQzYlxcdTA0MzVcXHUwNDM0XFx1MDQzZVxcdTA0MzJcXHUwNDMwXFx1MDQzZFxcdTA0MzhcXHUwNDRmIFxcXCJcXHUwNDFjXFx1MDQyMFxcdTA0MjIgXFx1MDQzYVxcdTA0MzhcXHUwNDQxXFx1MDQ0MlxcdTA0MzhcXFwiIFxcdTA0MzFcXHUwNDRiXFx1MDQzYiBcXHUwNDQzXFx1MDQ0MVxcdTA0M2ZcXHUwNDM1XFx1MDQ0OFxcdTA0M2RcXHUwNDNlIFxcdTA0MzRcXHUwNDNlXFx1MDQzMVxcdTA0MzBcXHUwNDMyXFx1MDQzYlxcdTA0MzVcXHUwNDNkLlwiXSxbXCJfX2pzb25fbWVzc2FnZVwiLDAsMjUsXCJcXHUwNDEyXFx1MDQzOFxcdTA0MzQgXFx1MDQzZVxcdTA0MzFcXHUwNDQxXFx1MDQzYlxcdTA0MzVcXHUwNDM0XFx1MDQzZVxcdTA0MzJcXHUwNDMwXFx1MDQzZFxcdTA0MzhcXHUwNDRmIFxcXCJcXHUwNDFjXFx1MDQyMFxcdTA0MjIgXFx1MDQ0MlxcdTA0MzBcXHUwNDM3XFx1MDQzZVxcdTA0MzFcXHUwNDM1XFx1MDQzNFxcdTA0NDBcXHUwNDM1XFx1MDQzZFxcdTA0M2RcXHUwNDNlXFx1MDQzM1xcdTA0M2UgXFx1MDQ0MVxcdTA0NDNcXHUwNDQxXFx1MDQ0MlxcdTA0MzBcXHUwNDMyXFx1MDQzMFxcXCIgXFx1MDQzMVxcdTA0NGJcXHUwNDNiIFxcdTA0NDNcXHUwNDQxXFx1MDQzZlxcdTA0MzVcXHUwNDQ4XFx1MDQzZFxcdTA0M2UgXFx1MDQzNFxcdTA0M2VcXHUwNDMxXFx1MDQzMFxcdTA0MzJcXHUwNDNiXFx1MDQzNVxcdTA0M2QuXCJdLFtcIl9fanNvbl9tZXNzYWdlXCIsMCwyNSxcIlxcdTA0MTJcXHUwNDM4XFx1MDQzNCBcXHUwNDNlXFx1MDQzMVxcdTA0NDFcXHUwNDNiXFx1MDQzNVxcdTA0MzRcXHUwNDNlXFx1MDQzMlxcdTA0MzBcXHUwNDNkXFx1MDQzOFxcdTA0NGYgXFxcIlxcdTA0MWNcXHUwNDIwXFx1MDQyMiBcXHUwNDNhXFx1MDQzZVxcdTA0M2JcXHUwNDM1XFx1MDQzZFxcdTA0M2RcXHUwNDNlXFx1MDQzM1xcdTA0M2UgXFx1MDQ0MVxcdTA0NDNcXHUwNDQxXFx1MDQ0MlxcdTA0MzBcXHUwNDMyXFx1MDQzMFxcXCIgXFx1MDQzMVxcdTA0NGJcXHUwNDNiIFxcdTA0NDNcXHUwNDQxXFx1MDQzZlxcdTA0MzVcXHUwNDQ4XFx1MDQzZFxcdTA0M2UgXFx1MDQzNFxcdTA0M2VcXHUwNDMxXFx1MDQzMFxcdTA0MzJcXHUwNDNiXFx1MDQzNVxcdTA0M2QuXCJdLFtcIl9fanNvbl9tZXNzYWdlXCIsMCwyNSxcIlxcdTA0MTJcXHUwNDM4XFx1MDQzNCBcXHUwNDNlXFx1MDQzMVxcdTA0NDFcXHUwNDNiXFx1MDQzNVxcdTA0MzRcXHUwNDNlXFx1MDQzMlxcdTA0MzBcXHUwNDNkXFx1MDQzOFxcdTA0NGYgXFxcIlxcdTA0MWNcXHUwNDIwXFx1MDQyMiBcXHUwNDMzXFx1MDQzZVxcdTA0M2JcXHUwNDM1XFx1MDQzZFxcdTA0M2VcXHUwNDQxXFx1MDQ0MlxcdTA0M2VcXHUwNDNmXFx1MDQzZFxcdTA0M2VcXHUwNDMzXFx1MDQzZSBcXHUwNDQxXFx1MDQ0M1xcdTA0NDFcXHUwNDQyXFx1MDQzMFxcdTA0MzJcXHUwNDMwXFxcIiBcXHUwNDMxXFx1MDQ0YlxcdTA0M2IgXFx1MDQ0M1xcdTA0NDFcXHUwNDNmXFx1MDQzNVxcdTA0NDhcXHUwNDNkXFx1MDQzZSBcXHUwNDM0XFx1MDQzZVxcdTA0MzFcXHUwNDMwXFx1MDQzMlxcdTA0M2JcXHUwNDM1XFx1MDQzZC5cIl0sW1wiX19qc29uX21lc3NhZ2VcIiwwLDI1LFwiXFx1MDQxMlxcdTA0MzhcXHUwNDM0IFxcdTA0M2VcXHUwNDMxXFx1MDQ0MVxcdTA0M2JcXHUwNDM1XFx1MDQzNFxcdTA0M2VcXHUwNDMyXFx1MDQzMFxcdTA0M2RcXHUwNDM4XFx1MDQ0ZiBcXFwiXFx1MDQxY1xcdTA0MjBcXHUwNDIyIFxcdTA0NDFcXHUwNDQyXFx1MDQzZVxcdTA0M2ZcXHUwNDRiXFxcIiBcXHUwNDMxXFx1MDQ0YlxcdTA0M2IgXFx1MDQ0M1xcdTA0NDFcXHUwNDNmXFx1MDQzNVxcdTA0NDhcXHUwNDNkXFx1MDQzZSBcXHUwNDM0XFx1MDQzZVxcdTA0MzFcXHUwNDMwXFx1MDQzMlxcdTA0M2JcXHUwNDM1XFx1MDQzZC5cIl0sW1wiX19qc29uX21lc3NhZ2VcIiwwLDI1LFwiXFx1MDQxMlxcdTA0MzhcXHUwNDM0IFxcdTA0M2VcXHUwNDMxXFx1MDQ0MVxcdTA0M2JcXHUwNDM1XFx1MDQzNFxcdTA0M2VcXHUwNDMyXFx1MDQzMFxcdTA0M2RcXHUwNDM4XFx1MDQ0ZiBcXFwiXFx1MDQxY1xcdTA0MjBcXHUwNDIyIFxcdTA0NDFcXHUwNDNlXFx1MDQ0MVxcdTA0NDNcXHUwNDM0XFx1MDQzZVxcdTA0MzJcXFwiIFxcdTA0MzFcXHUwNDRiXFx1MDQzYiBcXHUwNDQzXFx1MDQ0MVxcdTA0M2ZcXHUwNDM1XFx1MDQ0OFxcdTA0M2RcXHUwNDNlIFxcdTA0MzRcXHUwNDNlXFx1MDQzMVxcdTA0MzBcXHUwNDMyXFx1MDQzYlxcdTA0MzVcXHUwNDNkLlwiXSxbXCJfX2pzb25fbWVzc2FnZVwiLDAsMjUsXCJcXHUwNDEyXFx1MDQzOFxcdTA0MzQgXFx1MDQzZVxcdTA0MzFcXHUwNDQxXFx1MDQzYlxcdTA0MzVcXHUwNDM0XFx1MDQzZVxcdTA0MzJcXHUwNDMwXFx1MDQzZFxcdTA0MzhcXHUwNDRmIFxcXCJcXHUwNDFjXFx1MDQyMFxcdTA0MjIgXFx1MDQ0MVxcdTA0M2VcXHUwNDQxXFx1MDQ0M1xcdTA0MzRcXHUwNDNlXFx1MDQzMiBcXHUwNDMzXFx1MDQzZVxcdTA0M2JcXHUwNDNlXFx1MDQzMlxcdTA0M2RcXHUwNDNlXFx1MDQzM1xcdTA0M2UgXFx1MDQzY1xcdTA0M2VcXHUwNDM3XFx1MDQzM1xcdTA0MzBcXFwiIFxcdTA0MzFcXHUwNDRiXFx1MDQzYiBcXHUwNDQzXFx1MDQ0MVxcdTA0M2ZcXHUwNDM1XFx1MDQ0OFxcdTA0M2RcXHUwNDNlIFxcdTA0MzRcXHUwNDNlXFx1MDQzMVxcdTA0MzBcXHUwNDMyXFx1MDQzYlxcdTA0MzVcXHUwNDNkLlwiXSxbXCJfX2pzb25fbWVzc2FnZVwiLDAsMjUsXCJcXHUwNDEyXFx1MDQzOFxcdTA0MzQgXFx1MDQzZVxcdTA0MzFcXHUwNDQxXFx1MDQzYlxcdTA0MzVcXHUwNDM0XFx1MDQzZVxcdTA0MzJcXHUwNDMwXFx1MDQzZFxcdTA0MzhcXHUwNDRmIFxcXCJcXHUwNDFjXFx1MDQyMFxcdTA0MjIgXFx1MDQ0MVxcdTA0M2VcXHUwNDQxXFx1MDQ0M1xcdTA0MzRcXHUwNDNlXFx1MDQzMiBcXHUwNDQ4XFx1MDQzNVxcdTA0MzhcXFwiIFxcdTA0MzFcXHUwNDRiXFx1MDQzYiBcXHUwNDQzXFx1MDQ0MVxcdTA0M2ZcXHUwNDM1XFx1MDQ0OFxcdTA0M2RcXHUwNDNlIFxcdTA0MzRcXHUwNDNlXFx1MDQzMVxcdTA0MzBcXHUwNDMyXFx1MDQzYlxcdTA0MzVcXHUwNDNkLlwiXSxbXCJfX2pzb25fbWVzc2FnZVwiLDAsMjUsXCJcXHUwNDEyXFx1MDQzOFxcdTA0MzQgXFx1MDQzZVxcdTA0MzFcXHUwNDQxXFx1MDQzYlxcdTA0MzVcXHUwNDM0XFx1MDQzZVxcdTA0MzJcXHUwNDMwXFx1MDQzZFxcdTA0MzhcXHUwNDRmIFxcXCJcXHUwNDFjXFx1MDQyMFxcdTA0MjIgXFx1MDQzZlxcdTA0M2VcXHUwNDM0XFx1MDQzYVxcdTA0M2JcXHUwNDRlXFx1MDQ0N1xcdTA0MzhcXHUwNDQ3XFx1MDQzZFxcdTA0NGJcXHUwNDQ1IFxcdTA0MzBcXHUwNDQwXFx1MDQ0MlxcdTA0MzVcXHUwNDQwXFx1MDQzOFxcdTA0MzlcXFwiIFxcdTA0MzFcXHUwNDRiXFx1MDQzYiBcXHUwNDQzXFx1MDQ0MVxcdTA0M2ZcXHUwNDM1XFx1MDQ0OFxcdTA0M2RcXHUwNDNlIFxcdTA0MzRcXHUwNDNlXFx1MDQzMVxcdTA0MzBcXHUwNDMyXFx1MDQzYlxcdTA0MzVcXHUwNDNkLlwiXSxbXCJfX2pzb25fbWVzc2FnZVwiLDAsMjUsXCJcXHUwNDEyXFx1MDQzOFxcdTA0MzQgXFx1MDQzZVxcdTA0MzFcXHUwNDQxXFx1MDQzYlxcdTA0MzVcXHUwNDM0XFx1MDQzZVxcdTA0MzJcXHUwNDMwXFx1MDQzZFxcdTA0MzhcXHUwNDRmIFxcXCJcXHUwNDFjXFx1MDQyMFxcdTA0MjIgXFx1MDQzM1xcdTA0NDBcXHUwNDQzXFx1MDQzNFxcdTA0M2RcXHUwNDNlXFx1MDQzM1xcdTA0M2UgXFx1MDQzZVxcdTA0NDJcXHUwNDM0XFx1MDQzNVxcdTA0M2JcXHUwNDMwIFxcdTA0MzBcXHUwNDNlXFx1MDQ0MFxcdTA0NDJcXHUwNDRiXFxcIiBcXHUwNDMxXFx1MDQ0YlxcdTA0M2IgXFx1MDQ0M1xcdTA0NDFcXHUwNDNmXFx1MDQzNVxcdTA0NDhcXHUwNDNkXFx1MDQzZSBcXHUwNDM0XFx1MDQzZVxcdTA0MzFcXHUwNDMwXFx1MDQzMlxcdTA0M2JcXHUwNDM1XFx1MDQzZC5cIl0sW1wiX19qc29uX21lc3NhZ2VcIiwwLDI1LFwiXFx1MDQxMlxcdTA0MzhcXHUwNDM0IFxcdTA0M2VcXHUwNDMxXFx1MDQ0MVxcdTA0M2JcXHUwNDM1XFx1MDQzNFxcdTA0M2VcXHUwNDMyXFx1MDQzMFxcdTA0M2RcXHUwNDM4XFx1MDQ0ZiBcXFwiXFx1MDQxY1xcdTA0MjBcXHUwNDIyIFxcdTA0MzFcXHUwNDQwXFx1MDQ0ZVxcdTA0NDhcXHUwNDNkXFx1MDQzZVxcdTA0MzNcXHUwNDNlIFxcdTA0M2VcXHUwNDQyXFx1MDQzNFxcdTA0MzVcXHUwNDNiXFx1MDQzMCBcXHUwNDMwXFx1MDQzZVxcdTA0NDBcXHUwNDQyXFx1MDQ0YlxcXCIgXFx1MDQzMVxcdTA0NGJcXHUwNDNiIFxcdTA0NDNcXHUwNDQxXFx1MDQzZlxcdTA0MzVcXHUwNDQ4XFx1MDQzZFxcdTA0M2UgXFx1MDQzNFxcdTA0M2VcXHUwNDMxXFx1MDQzMFxcdTA0MzJcXHUwNDNiXFx1MDQzNVxcdTA0M2QuXCJdLFtcIl9fanNvbl9tZXNzYWdlXCIsMCwyNSxcIlxcdTA0MTJcXHUwNDM4XFx1MDQzNCBcXHUwNDNlXFx1MDQzMVxcdTA0NDFcXHUwNDNiXFx1MDQzNVxcdTA0MzRcXHUwNDNlXFx1MDQzMlxcdTA0MzBcXHUwNDNkXFx1MDQzOFxcdTA0NGYgXFxcIlxcdTA0MWNcXHUwNDIwXFx1MDQyMiBcXHUwNDQxXFx1MDQzZVxcdTA0NDFcXHUwNDQzXFx1MDQzNFxcdTA0M2VcXHUwNDMyIFxcdTA0M2NcXHUwNDMwXFx1MDQzYlxcdTA0M2VcXHUwNDMzXFx1MDQzZSBcXHUwNDQyXFx1MDQzMFxcdTA0MzdcXHUwNDMwXFxcIiBcXHUwNDMxXFx1MDQ0YlxcdTA0M2IgXFx1MDQ0M1xcdTA0NDFcXHUwNDNmXFx1MDQzNVxcdTA0NDhcXHUwNDNkXFx1MDQzZSBcXHUwNDM0XFx1MDQzZVxcdTA0MzFcXHUwNDMwXFx1MDQzMlxcdTA0M2JcXHUwNDM1XFx1MDQzZC5cIl0sW1wiX19qc29uX21lc3NhZ2VcIiwwLDI1LFwiXFx1MDQxMlxcdTA0MzhcXHUwNDM0IFxcdTA0M2VcXHUwNDMxXFx1MDQ0MVxcdTA0M2JcXHUwNDM1XFx1MDQzNFxcdTA0M2VcXHUwNDMyXFx1MDQzMFxcdTA0M2RcXHUwNDM4XFx1MDQ0ZiBcXFwiXFx1MDQxY1xcdTA0MjBcXHUwNDIyIFxcdTA0NDFcXHUwNDNlXFx1MDQ0MVxcdTA0NDNcXHUwNDM0XFx1MDQzZVxcdTA0MzIgXFx1MDQzYVxcdTA0M2VcXHUwNDNkXFx1MDQzNVxcdTA0NDdcXHUwNDNkXFx1MDQzZVxcdTA0NDFcXHUwNDQyXFx1MDQzNVxcdTA0MzlcXFwiIFxcdTA0MzFcXHUwNDRiXFx1MDQzYiBcXHUwNDQzXFx1MDQ0MVxcdTA0M2ZcXHUwNDM1XFx1MDQ0OFxcdTA0M2RcXHUwNDNlIFxcdTA0MzRcXHUwNDNlXFx1MDQzMVxcdTA0MzBcXHUwNDMyXFx1MDQzYlxcdTA0MzVcXHUwNDNkLlwiXSxbXCJfX2pzb25fbWVzc2FnZVwiLDAsMjUsXCJcXHUwNDIzXFx1MDQ0MVxcdTA0M2ZcXHUwNDM1XFx1MDQ0OFxcdTA0M2RcXHUwNDNlIFxcdTA0NDNcXHUwNDM0XFx1MDQzMFxcdTA0M2JcXHUwNDM1XFx1MDQzZFxcdTA0NGIgMSBcXHUwNDEyXFx1MDQzOFxcdTA0MzQgXFx1MDQzZVxcdTA0MzFcXHUwNDQxXFx1MDQzYlxcdTA0MzVcXHUwNDM0XFx1MDQzZVxcdTA0MzJcXHUwNDMwXFx1MDQzZFxcdTA0MzhcXHUwNDRmLlwiXSxbXCJfX2pzb25fbWVzc2FnZVwiLDAsMjUsXCJcXHUwNDEyXFx1MDQzOFxcdTA0MzQgXFx1MDQzZVxcdTA0MzFcXHUwNDQxXFx1MDQzYlxcdTA0MzVcXHUwNDM0XFx1MDQzZVxcdTA0MzJcXHUwNDMwXFx1MDQzZFxcdTA0MzhcXHUwNDRmIFxcXCJcXHUwNDFhXFx1MDQyMiBcXHUwNDMzXFx1MDQzZVxcdTA0M2JcXHUwNDNlXFx1MDQzMlxcdTA0NGJcXFwiIFxcdTA0MzFcXHUwNDRiXFx1MDQzYiBcXHUwNDQzXFx1MDQ0MVxcdTA0M2ZcXHUwNDM1XFx1MDQ0OFxcdTA0M2RcXHUwNDNlIFxcdTA0MzhcXHUwNDM3XFx1MDQzY1xcdTA0MzVcXHUwNDNkXFx1MDQzNVxcdTA0M2QuXCJdLFtcIl9fanNvbl9tZXNzYWdlXCIsMCwyNSxcIlxcdTA0MTJcXHUwNDM4XFx1MDQzNCBcXHUwNDNlXFx1MDQzMVxcdTA0NDFcXHUwNDNiXFx1MDQzNVxcdTA0MzRcXHUwNDNlXFx1MDQzMlxcdTA0MzBcXHUwNDNkXFx1MDQzOFxcdTA0NGYgXFxcIlxcdTA0MWFcXHUwNDIyIFxcdTA0MzNcXHUwNDNiXFx1MDQzMFxcdTA0MzdcXHUwNDNkXFx1MDQ0YlxcdTA0NDUgXFx1MDQzZVxcdTA0NDBcXHUwNDMxXFx1MDQzOFxcdTA0NDJcXFwiIFxcdTA0MzFcXHUwNDRiXFx1MDQzYiBcXHUwNDQzXFx1MDQ0MVxcdTA0M2ZcXHUwNDM1XFx1MDQ0OFxcdTA0M2RcXHUwNDNlIFxcdTA0MzhcXHUwNDM3XFx1MDQzY1xcdTA0MzVcXHUwNDNkXFx1MDQzNVxcdTA0M2QuXCJdLFtcIl9fanNvbl9tZXNzYWdlXCIsMCwyNSxcIlxcdTA0MTJcXHUwNDM4XFx1MDQzNCBcXHUwNDNlXFx1MDQzMVxcdTA0NDFcXHUwNDNiXFx1MDQzNVxcdTA0MzRcXHUwNDNlXFx1MDQzMlxcdTA0MzBcXHUwNDNkXFx1MDQzOFxcdTA0NGYgXFxcIlxcdTA0MWFcXHUwNDIyIFxcdTA0M2ZcXHUwNDMwXFx1MDQzN1xcdTA0NDNcXHUwNDQ1IFxcdTA0M2RcXHUwNDNlXFx1MDQ0MVxcdTA0MzBcXFwiIFxcdTA0MzFcXHUwNDRiXFx1MDQzYiBcXHUwNDQzXFx1MDQ0MVxcdTA0M2ZcXHUwNDM1XFx1MDQ0OFxcdTA0M2RcXHUwNDNlIFxcdTA0MzhcXHUwNDM3XFx1MDQzY1xcdTA0MzVcXHUwNDNkXFx1MDQzNVxcdTA0M2QuXCJdLFtcIl9fanNvbl9tZXNzYWdlXCIsMCwyNSxcIlxcdTA0MTJcXHUwNDM4XFx1MDQzNCBcXHUwNDNlXFx1MDQzMVxcdTA0NDFcXHUwNDNiXFx1MDQzNVxcdTA0MzRcXHUwNDNlXFx1MDQzMlxcdTA0MzBcXHUwNDNkXFx1MDQzOFxcdTA0NGYgXFxcIlxcdTA0MWFcXHUwNDIyIFxcdTA0MzNcXHUwNDNlXFx1MDQzYlxcdTA0M2VcXHUwNDMyXFx1MDQzZFxcdTA0M2VcXHUwNDMzXFx1MDQzZSBcXHUwNDNjXFx1MDQzZVxcdTA0MzdcXHUwNDMzXFx1MDQzMFxcXCIgXFx1MDQzMVxcdTA0NGJcXHUwNDNiIFxcdTA0NDNcXHUwNDQxXFx1MDQzZlxcdTA0MzVcXHUwNDQ4XFx1MDQzZFxcdTA0M2UgXFx1MDQzNFxcdTA0M2VcXHUwNDMxXFx1MDQzMFxcdTA0MzJcXHUwNDNiXFx1MDQzNVxcdTA0M2QuXCJdLFtcIl9fanNvbl9tZXNzYWdlXCIsMCwyNSxcIlxcdTA0MTJcXHUwNDM4XFx1MDQzNCBcXHUwNDNlXFx1MDQzMVxcdTA0NDFcXHUwNDNiXFx1MDQzNVxcdTA0MzRcXHUwNDNlXFx1MDQzMlxcdTA0MzBcXHUwNDNkXFx1MDQzOFxcdTA0NGYgXFxcIlxcdTA0MWFcXHUwNDIyIFxcdTA0M2ZcXHUwNDNlXFx1MDQzN1xcdTA0MzJcXHUwNDNlXFx1MDQzZFxcdTA0M2VcXHUwNDQ3XFx1MDQzZFxcdTA0MzhcXHUwNDNhXFx1MDQzMFxcXCIgXFx1MDQzMVxcdTA0NGJcXHUwNDNiIFxcdTA0NDNcXHUwNDQxXFx1MDQzZlxcdTA0MzVcXHUwNDQ4XFx1MDQzZFxcdTA0M2UgXFx1MDQzNFxcdTA0M2VcXHUwNDMxXFx1MDQzMFxcdTA0MzJcXHUwNDNiXFx1MDQzNVxcdTA0M2QuXCJdLFtcIl9fanNvbl9tZXNzYWdlXCIsMCwyNSxcIlxcdTA0MTJcXHUwNDM4XFx1MDQzNCBcXHUwNDNlXFx1MDQzMVxcdTA0NDFcXHUwNDNiXFx1MDQzNVxcdTA0MzRcXHUwNDNlXFx1MDQzMlxcdTA0MzBcXHUwNDNkXFx1MDQzOFxcdTA0NGYgXFxcIlxcdTA0MWFcXHUwNDIyIFxcdTA0NDhcXHUwNDM1XFx1MDQzOVxcdTA0M2RcXHUwNDNlXFx1MDQzM1xcdTA0M2UgXFx1MDQzZVxcdTA0NDJcXHUwNDM0XFx1MDQzNVxcdTA0M2JcXHUwNDMwXFxcIiBcXHUwNDMxXFx1MDQ0YlxcdTA0M2IgXFx1MDQ0M1xcdTA0NDFcXHUwNDNmXFx1MDQzNVxcdTA0NDhcXHUwNDNkXFx1MDQzZSBcXHUwNDM0XFx1MDQzZVxcdTA0MzFcXHUwNDMwXFx1MDQzMlxcdTA0M2JcXHUwNDM1XFx1MDQzZC5cIl0sW1wiX19qc29uX21lc3NhZ2VcIiwwLDI1LFwiXFx1MDQxMlxcdTA0MzhcXHUwNDM0IFxcdTA0M2VcXHUwNDMxXFx1MDQ0MVxcdTA0M2JcXHUwNDM1XFx1MDQzNFxcdTA0M2VcXHUwNDMyXFx1MDQzMFxcdTA0M2RcXHUwNDM4XFx1MDQ0ZiBcXFwiXFx1MDQxYVxcdTA0MjIgXFx1MDQzM1xcdTA0NDBcXHUwNDQzXFx1MDQzNFxcdTA0M2RcXHUwNDNlXFx1MDQzM1xcdTA0M2UgXFx1MDQzZVxcdTA0NDJcXHUwNDM0XFx1MDQzNVxcdTA0M2JcXHUwNDMwXFxcIiBcXHUwNDMxXFx1MDQ0YlxcdTA0M2IgXFx1MDQ0M1xcdTA0NDFcXHUwNDNmXFx1MDQzNVxcdTA0NDhcXHUwNDNkXFx1MDQzZSBcXHUwNDM0XFx1MDQzZVxcdTA0MzFcXHUwNDMwXFx1MDQzMlxcdTA0M2JcXHUwNDM1XFx1MDQzZC5cIl0sW1wiX19qc29uX21lc3NhZ2VcIiwwLDI1LFwiXFx1MDQxMlxcdTA0MzhcXHUwNDM0IFxcdTA0M2VcXHUwNDMxXFx1MDQ0MVxcdTA0M2JcXHUwNDM1XFx1MDQzNFxcdTA0M2VcXHUwNDMyXFx1MDQzMFxcdTA0M2RcXHUwNDM4XFx1MDQ0ZiBcXFwiXFx1MDQxYVxcdTA0MjIgXFx1MDQzZlxcdTA0M2VcXHUwNDRmXFx1MDQ0MVxcdTA0M2RcXHUwNDM4XFx1MDQ0N1xcdTA0M2RcXHUwNDNlLVxcdTA0M2FcXHUwNDQwXFx1MDQzNVxcdTA0NDFcXHUwNDQyXFx1MDQ0NlxcdTA0M2VcXHUwNDMyXFx1MDQzZVxcdTA0MzNcXHUwNDNlIFxcdTA0M2VcXHUwNDQyXFx1MDQzNFxcdTA0MzVcXHUwNDNiXFx1MDQzMFxcXCIgXFx1MDQzMVxcdTA0NGJcXHUwNDNiIFxcdTA0NDNcXHUwNDQxXFx1MDQzZlxcdTA0MzVcXHUwNDQ4XFx1MDQzZFxcdTA0M2UgXFx1MDQzNFxcdTA0M2VcXHUwNDMxXFx1MDQzMFxcdTA0MzJcXHUwNDNiXFx1MDQzNVxcdTA0M2QuXCJdLFtcIl9fanNvbl9tZXNzYWdlXCIsMCwyNSxcIlxcdTA0MTJcXHUwNDM4XFx1MDQzNCBcXHUwNDNlXFx1MDQzMVxcdTA0NDFcXHUwNDNiXFx1MDQzNVxcdTA0MzRcXHUwNDNlXFx1MDQzMlxcdTA0MzBcXHUwNDNkXFx1MDQzOFxcdTA0NGYgXFxcIlxcdTA0MWFcXHUwNDIyIFxcdTA0M2FcXHUwNDQwXFx1MDQzNVxcdTA0NDFcXHUwNDQyXFx1MDQ0NlxcdTA0M2VcXHUwNDMyXFx1MDQzZS1cXHUwNDNmXFx1MDQzZVxcdTA0MzJcXHUwNDM3XFx1MDQzNFxcdTA0M2VcXHUwNDQ4XFx1MDQzZFxcdTA0NGJcXHUwNDQ1IFxcdTA0NDFcXHUwNDNlXFx1MDQ0N1xcdTA0M2JcXHUwNDM1XFx1MDQzZFxcdTA0MzVcXHUwNDNkXFx1MDQzOFxcdTA0MzlcXFwiIFxcdTA0MzFcXHUwNDRiXFx1MDQzYiBcXHUwNDQzXFx1MDQ0MVxcdTA0M2ZcXHUwNDM1XFx1MDQ0OFxcdTA0M2RcXHUwNDNlIFxcdTA0MzRcXHUwNDNlXFx1MDQzMVxcdTA0MzBcXHUwNDMyXFx1MDQzYlxcdTA0MzVcXHUwNDNkLlwiXSxbXCJfX2pzb25fbWVzc2FnZVwiLDAsMjUsXCJcXHUwNDEyXFx1MDQzOFxcdTA0MzQgXFx1MDQzZVxcdTA0MzFcXHUwNDQxXFx1MDQzYlxcdTA0MzVcXHUwNDM0XFx1MDQzZVxcdTA0MzJcXHUwNDMwXFx1MDQzZFxcdTA0MzhcXHUwNDRmIFxcXCJcXHUwNDFhXFx1MDQyMiBcXHUwNDNhXFx1MDQzZVxcdTA0M2ZcXHUwNDQ3XFx1MDQzOFxcdTA0M2FcXHUwNDNlXFx1MDQzMlxcdTA0M2VcXHUwNDM5IFxcdTA0M2VcXHUwNDMxXFx1MDQzYlxcdTA0MzBcXHUwNDQxXFx1MDQ0MlxcdTA0MzhcXFwiIFxcdTA0MzFcXHUwNDRiXFx1MDQzYiBcXHUwNDQzXFx1MDQ0MVxcdTA0M2ZcXHUwNDM1XFx1MDQ0OFxcdTA0M2RcXHUwNDNlIFxcdTA0MzRcXHUwNDNlXFx1MDQzMVxcdTA0MzBcXHUwNDMyXFx1MDQzYlxcdTA0MzVcXHUwNDNkLlwiXSxbXCJfX2pzb25fbWVzc2FnZVwiLDAsMjUsXCJcXHUwNDEyXFx1MDQzOFxcdTA0MzQgXFx1MDQzZVxcdTA0MzFcXHUwNDQxXFx1MDQzYlxcdTA0MzVcXHUwNDM0XFx1MDQzZVxcdTA0MzJcXHUwNDMwXFx1MDQzZFxcdTA0MzhcXHUwNDRmIFxcXCJcXHUwNDFhXFx1MDQyMiBcXHUwNDNlXFx1MDQ0MFxcdTA0MzNcXHUwNDMwXFx1MDQzZFxcdTA0M2VcXHUwNDMyIFxcdTA0MzggXFx1MDQzY1xcdTA0NGZcXHUwNDMzXFx1MDQzYVxcdTA0MzhcXHUwNDQ1IFxcdTA0NDJcXHUwNDNhXFx1MDQzMFxcdTA0M2RcXHUwNDM1XFx1MDQzOVxcXCIgXFx1MDQzMVxcdTA0NGJcXHUwNDNiIFxcdTA0NDNcXHUwNDQxXFx1MDQzZlxcdTA0MzVcXHUwNDQ4XFx1MDQzZFxcdTA0M2UgXFx1MDQzNFxcdTA0M2VcXHUwNDMxXFx1MDQzMFxcdTA0MzJcXHUwNDNiXFx1MDQzNVxcdTA0M2QuXCJdLFtcIl9fanNvbl9tZXNzYWdlXCIsMCwyNSxcIlxcdTA0MTJcXHUwNDM4XFx1MDQzNCBcXHUwNDNlXFx1MDQzMVxcdTA0NDFcXHUwNDNiXFx1MDQzNVxcdTA0MzRcXHUwNDNlXFx1MDQzMlxcdTA0MzBcXHUwNDNkXFx1MDQzOFxcdTA0NGYgXFxcIlxcdTA0MWFcXHUwNDIyIFxcdTA0M2NcXHUwNDRmXFx1MDQzM1xcdTA0M2FcXHUwNDM4XFx1MDQ0NSBcXHUwNDQyXFx1MDQzYVxcdTA0MzBcXHUwNDNkXFx1MDQzNVxcdTA0MzkgXFx1MDQzOCBcXHUwNDNlXFx1MDQ0MFxcdTA0MzNcXHUwNDMwXFx1MDQzZFxcdTA0M2VcXHUwNDMyIFxcdTA0NDhcXHUwNDM1XFx1MDQzOFxcXCIgXFx1MDQzMVxcdTA0NGJcXHUwNDNiIFxcdTA0NDNcXHUwNDQxXFx1MDQzZlxcdTA0MzVcXHUwNDQ4XFx1MDQzZFxcdTA0M2UgXFx1MDQzNFxcdTA0M2VcXHUwNDMxXFx1MDQzMFxcdTA0MzJcXHUwNDNiXFx1MDQzNVxcdTA0M2QuXCJdLFtcIl9fanNvbl9tZXNzYWdlXCIsMCwyNSxcIlxcdTA0MTJcXHUwNDM4XFx1MDQzNCBcXHUwNDNlXFx1MDQzMVxcdTA0NDFcXHUwNDNiXFx1MDQzNVxcdTA0MzRcXHUwNDNlXFx1MDQzMlxcdTA0MzBcXHUwNDNkXFx1MDQzOFxcdTA0NGYgXFxcIlxcdTA0MWFcXHUwNDIyIFxcdTA0M2VcXHUwNDQwXFx1MDQzM1xcdTA0MzBcXHUwNDNkXFx1MDQzZVxcdTA0MzIgXFx1MDQzM1xcdTA0NDBcXHUwNDQzXFx1MDQzNFxcdTA0M2RcXHUwNDNlXFx1MDQzOSBcXHUwNDNhXFx1MDQzYlxcdTA0MzVcXHUwNDQyXFx1MDQzYVxcdTA0MzggXFx1MDQzOCBcXHUwNDNiXFx1MDQzNVxcdTA0MzNcXHUwNDNhXFx1MDQzOFxcdTA0NDVcXFwiIFxcdTA0MzFcXHUwNDRiXFx1MDQzYiBcXHUwNDQzXFx1MDQ0MVxcdTA0M2ZcXHUwNDM1XFx1MDQ0OFxcdTA0M2RcXHUwNDNlIFxcdTA0MzRcXHUwNDNlXFx1MDQzMVxcdTA0MzBcXHUwNDMyXFx1MDQzYlxcdTA0MzVcXHUwNDNkLlwiXSxbXCJfX2pzb25fbWVzc2FnZVwiLDAsMjUsXCJcXHUwNDEyXFx1MDQzOFxcdTA0MzQgXFx1MDQzZVxcdTA0MzFcXHUwNDQxXFx1MDQzYlxcdTA0MzVcXHUwNDM0XFx1MDQzZVxcdTA0MzJcXHUwNDMwXFx1MDQzZFxcdTA0MzhcXHUwNDRmIFxcXCJcXHUwNDFhXFx1MDQyMi1cXHUwNDMyXFx1MDQzOFxcdTA0NDBcXHUwNDQyXFx1MDQ0M1xcdTA0MzBcXHUwNDNiXFx1MDQ0Y1xcdTA0M2RcXHUwNDMwXFx1MDQ0ZiBcXHUwNDMxXFx1MDQ0MFxcdTA0M2VcXHUwNDNkXFx1MDQ0NVxcdTA0M2VcXHUwNDQxXFx1MDQzYVxcdTA0M2VcXHUwNDNmXFx1MDQzOFxcdTA0NGZcXFwiIFxcdTA0MzFcXHUwNDRiXFx1MDQzYiBcXHUwNDQzXFx1MDQ0MVxcdTA0M2ZcXHUwNDM1XFx1MDQ0OFxcdTA0M2RcXHUwNDNlIFxcdTA0MzRcXHUwNDNlXFx1MDQzMVxcdTA0MzBcXHUwNDMyXFx1MDQzYlxcdTA0MzVcXHUwNDNkLlwiXSxbXCJfX2pzb25fbWVzc2FnZVwiLDAsMjUsXCJcXHUwNDEyXFx1MDQzOFxcdTA0MzQgXFx1MDQzZVxcdTA0MzFcXHUwNDQxXFx1MDQzYlxcdTA0MzVcXHUwNDM0XFx1MDQzZVxcdTA0MzJcXHUwNDMwXFx1MDQzZFxcdTA0MzhcXHUwNDRmIFxcXCJcXHUwNDFhXFx1MDQyMiBcXHUwNDNlXFx1MDQ0MFxcdTA0MzNcXHUwNDMwXFx1MDQzZFxcdTA0M2VcXHUwNDMyIFxcdTA0MzFcXHUwNDQwXFx1MDQ0ZVxcdTA0NDhcXHUwNDNkXFx1MDQzZVxcdTA0MzkgXFx1MDQzZlxcdTA0M2VcXHUwNDNiXFx1MDQzZVxcdTA0NDFcXHUwNDQyXFx1MDQzOFxcXCIgXFx1MDQzMVxcdTA0NGJcXHUwNDNiIFxcdTA0NDNcXHUwNDQxXFx1MDQzZlxcdTA0MzVcXHUwNDQ4XFx1MDQzZFxcdTA0M2UgXFx1MDQzNFxcdTA0M2VcXHUwNDMxXFx1MDQzMFxcdTA0MzJcXHUwNDNiXFx1MDQzNVxcdTA0M2QuXCJdLFtcIl9fanNvbl9tZXNzYWdlXCIsMCwyNSxcIlxcdTA0MTJcXHUwNDM4XFx1MDQzNCBcXHUwNDNlXFx1MDQzMVxcdTA0NDFcXHUwNDNiXFx1MDQzNVxcdTA0MzRcXHUwNDNlXFx1MDQzMlxcdTA0MzBcXHUwNDNkXFx1MDQzOFxcdTA0NGYgXFxcIlxcdTA0MWFcXHUwNDIyIFxcdTA0M2VcXHUwNDQwXFx1MDQzM1xcdTA0MzBcXHUwNDNkXFx1MDQzZVxcdTA0MzIgXFx1MDQzN1xcdTA0MzBcXHUwNDMxXFx1MDQ0MFxcdTA0NGVcXHUwNDQ4XFx1MDQzOFxcdTA0M2RcXHUwNDNkXFx1MDQzZVxcdTA0MzNcXHUwNDNlIFxcdTA0M2ZcXHUwNDQwXFx1MDQzZVxcdTA0NDFcXHUwNDQyXFx1MDQ0MFxcdTA0MzBcXHUwNDNkXFx1MDQ0MVxcdTA0NDJcXHUwNDMyXFx1MDQzMFxcXCIgXFx1MDQzMVxcdTA0NGJcXHUwNDNiIFxcdTA0NDNcXHUwNDQxXFx1MDQzZlxcdTA0MzVcXHUwNDQ4XFx1MDQzZFxcdTA0M2UgXFx1MDQzNFxcdTA0M2VcXHUwNDMxXFx1MDQzMFxcdTA0MzJcXHUwNDNiXFx1MDQzNVxcdTA0M2QuXCJdLFtcIl9fanNvbl9tZXNzYWdlXCIsMCwyNSxcIlxcdTA0MTJcXHUwNDM4XFx1MDQzNCBcXHUwNDNlXFx1MDQzMVxcdTA0NDFcXHUwNDNiXFx1MDQzNVxcdTA0MzRcXHUwNDNlXFx1MDQzMlxcdTA0MzBcXHUwNDNkXFx1MDQzOFxcdTA0NGYgXFxcIlxcdTA0MWFcXHUwNDIyIFxcdTA0M2VcXHUwNDQwXFx1MDQzM1xcdTA0MzBcXHUwNDNkXFx1MDQzZVxcdTA0MzIgXFx1MDQzY1xcdTA0MzBcXHUwNDNiXFx1MDQzZVxcdTA0MzNcXHUwNDNlIFxcdTA0NDJcXHUwNDMwXFx1MDQzN1xcdTA0MzBcXFwiIFxcdTA0MzFcXHUwNDRiXFx1MDQzYiBcXHUwNDQzXFx1MDQ0MVxcdTA0M2ZcXHUwNDM1XFx1MDQ0OFxcdTA0M2RcXHUwNDNlIFxcdTA0MzRcXHUwNDNlXFx1MDQzMVxcdTA0MzBcXHUwNDMyXFx1MDQzYlxcdTA0MzVcXHUwNDNkLlwiXSxbXCJfX2pzb25fbWVzc2FnZVwiLDAsMjUsXCJcXHUwNDEyXFx1MDQzOFxcdTA0MzQgXFx1MDQzZVxcdTA0MzFcXHUwNDQxXFx1MDQzYlxcdTA0MzVcXHUwNDM0XFx1MDQzZVxcdTA0MzJcXHUwNDMwXFx1MDQzZFxcdTA0MzhcXHUwNDRmIFxcXCJcXHUwNDFhXFx1MDQyMi1cXHUwNDMyXFx1MDQzOFxcdTA0NDBcXHUwNDQyXFx1MDQ0M1xcdTA0MzBcXHUwNDNiXFx1MDQ0Y1xcdTA0M2RcXHUwNDMwXFx1MDQ0ZiBcXHUwNDNhXFx1MDQzZVxcdTA0M2JcXHUwNDNlXFx1MDQzZFxcdTA0M2VcXHUwNDQxXFx1MDQzYVxcdTA0M2VcXHUwNDNmXFx1MDQzOFxcdTA0NGZcXFwiIFxcdTA0MzFcXHUwNDRiXFx1MDQzYiBcXHUwNDQzXFx1MDQ0MVxcdTA0M2ZcXHUwNDM1XFx1MDQ0OFxcdTA0M2RcXHUwNDNlIFxcdTA0MzRcXHUwNDNlXFx1MDQzMVxcdTA0MzBcXHUwNDMyXFx1MDQzYlxcdTA0MzVcXHUwNDNkLlwiXSxbXCJfX2pzb25fbWVzc2FnZVwiLDAsMjUsXCJcXHUwNDEyXFx1MDQzOFxcdTA0MzQgXFx1MDQzZVxcdTA0MzFcXHUwNDQxXFx1MDQzYlxcdTA0MzVcXHUwNDM0XFx1MDQzZVxcdTA0MzJcXHUwNDMwXFx1MDQzZFxcdTA0MzhcXHUwNDRmIFxcXCJcXHUwNDFhXFx1MDQyMiBcXHUwNDQxXFx1MDQ0M1xcdTA0NDFcXHUwNDQyXFx1MDQzMFxcdTA0MzJcXHUwNDNlXFx1MDQzMiBcXHUwNDM4IFxcdTA0M2FcXHUwNDNlXFx1MDQ0MVxcdTA0NDJcXHUwNDM1XFx1MDQzOVxcXCIgXFx1MDQzMVxcdTA0NGJcXHUwNDNiIFxcdTA0NDNcXHUwNDQxXFx1MDQzZlxcdTA0MzVcXHUwNDQ4XFx1MDQzZFxcdTA0M2UgXFx1MDQzNFxcdTA0M2VcXHUwNDMxXFx1MDQzMFxcdTA0MzJcXHUwNDNiXFx1MDQzNVxcdTA0M2QuXCJdLFtcIl9fanNvbl9tZXNzYWdlXCIsMCwyNSxcIlxcdTA0MTJcXHUwNDM4XFx1MDQzNCBcXHUwNDNlXFx1MDQzMVxcdTA0NDFcXHUwNDNiXFx1MDQzNVxcdTA0MzRcXHUwNDNlXFx1MDQzMlxcdTA0MzBcXHUwNDNkXFx1MDQzOFxcdTA0NGYgXFxcIlxcdTA0MWFcXHUwNDIyIFxcdTA0NDdcXHUwNDM1XFx1MDQzYlxcdTA0NGVcXHUwNDQxXFx1MDQ0MlxcdTA0MzhcXFwiIFxcdTA0MzFcXHUwNDRiXFx1MDQzYiBcXHUwNDQzXFx1MDQ0MVxcdTA0M2ZcXHUwNDM1XFx1MDQ0OFxcdTA0M2RcXHUwNDNlIFxcdTA0MzRcXHUwNDNlXFx1MDQzMVxcdTA0MzBcXHUwNDMyXFx1MDQzYlxcdTA0MzVcXHUwNDNkLlwiXSxbXCJfX2pzb25fbWVzc2FnZVwiLDAsMjUsXCJcXHUwNDEyXFx1MDQzOFxcdTA0MzQgXFx1MDQzZVxcdTA0MzFcXHUwNDQxXFx1MDQzYlxcdTA0MzVcXHUwNDM0XFx1MDQzZVxcdTA0MzJcXHUwNDMwXFx1MDQzZFxcdTA0MzhcXHUwNDRmIFxcXCJcXHUwNDFhXFx1MDQyMiBcXHUwNDNmXFx1MDQzYlxcdTA0MzVcXHUwNDQ3XFx1MDQzNVxcdTA0MzJcXHUwNDNlXFx1MDQzM1xcdTA0M2UgXFx1MDQ0MVxcdTA0NDNcXHUwNDQxXFx1MDQ0MlxcdTA0MzBcXHUwNDMyXFx1MDQzMFxcXCIgXFx1MDQzMVxcdTA0NGJcXHUwNDNiIFxcdTA0NDNcXHUwNDQxXFx1MDQzZlxcdTA0MzVcXHUwNDQ4XFx1MDQzZFxcdTA0M2UgXFx1MDQzNFxcdTA0M2VcXHUwNDMxXFx1MDQzMFxcdTA0MzJcXHUwNDNiXFx1MDQzNVxcdTA0M2QuXCJdLFtcIl9fanNvbl9tZXNzYWdlXCIsMCwyNSxcIlxcdTA0MTJcXHUwNDM4XFx1MDQzNCBcXHUwNDNlXFx1MDQzMVxcdTA0NDFcXHUwNDNiXFx1MDQzNVxcdTA0MzRcXHUwNDNlXFx1MDQzMlxcdTA0MzBcXHUwNDNkXFx1MDQzOFxcdTA0NGYgXFxcIlxcdTA0MWFcXHUwNDIyIFxcdTA0M2FcXHUwNDNiXFx1MDQ0ZVxcdTA0NDdcXHUwNDM4XFx1MDQ0NlxcdTA0NGJcXFwiIFxcdTA0MzFcXHUwNDRiXFx1MDQzYiBcXHUwNDQzXFx1MDQ0MVxcdTA0M2ZcXHUwNDM1XFx1MDQ0OFxcdTA0M2RcXHUwNDNlIFxcdTA0MzRcXHUwNDNlXFx1MDQzMVxcdTA0MzBcXHUwNDMyXFx1MDQzYlxcdTA0MzVcXHUwNDNkLlwiXSxbXCJfX2pzb25fbWVzc2FnZVwiLDAsMjUsXCJcXHUwNDEyXFx1MDQzOFxcdTA0MzQgXFx1MDQzZVxcdTA0MzFcXHUwNDQxXFx1MDQzYlxcdTA0MzVcXHUwNDM0XFx1MDQzZVxcdTA0MzJcXHUwNDMwXFx1MDQzZFxcdTA0MzhcXHUwNDRmIFxcXCJcXHUwNDFhXFx1MDQyMiBcXHUwNDNiXFx1MDQzZVxcdTA0M2FcXHUwNDQyXFx1MDQzNVxcdTA0MzJcXHUwNDNlXFx1MDQzM1xcdTA0M2UgXFx1MDQ0MVxcdTA0NDNcXHUwNDQxXFx1MDQ0MlxcdTA0MzBcXHUwNDMyXFx1MDQzMFxcXCIgXFx1MDQzMVxcdTA0NGJcXHUwNDNiIFxcdTA0NDNcXHUwNDQxXFx1MDQzZlxcdTA0MzVcXHUwNDQ4XFx1MDQzZFxcdTA0M2UgXFx1MDQzNFxcdTA0M2VcXHUwNDMxXFx1MDQzMFxcdTA0MzJcXHUwNDNiXFx1MDQzNVxcdTA0M2QuXCJdLFtcIl9fanNvbl9tZXNzYWdlXCIsMCwyNSxcIlxcdTA0MTJcXHUwNDM4XFx1MDQzNCBcXHUwNDNlXFx1MDQzMVxcdTA0NDFcXHUwNDNiXFx1MDQzNVxcdTA0MzRcXHUwNDNlXFx1MDQzMlxcdTA0MzBcXHUwNDNkXFx1MDQzOFxcdTA0NGYgXFxcIlxcdTA0MWFcXHUwNDIyIFxcdTA0M2JcXHUwNDQzXFx1MDQ0N1xcdTA0MzVcXHUwNDM3XFx1MDQzMFxcdTA0M2ZcXHUwNDRmXFx1MDQ0MVxcdTA0NDJcXHUwNDNkXFx1MDQzZVxcdTA0MzNcXHUwNDNlIFxcdTA0NDFcXHUwNDQzXFx1MDQ0MVxcdTA0NDJcXHUwNDMwXFx1MDQzMlxcdTA0MzBcXFwiIFxcdTA0MzFcXHUwNDRiXFx1MDQzYiBcXHUwNDQzXFx1MDQ0MVxcdTA0M2ZcXHUwNDM1XFx1MDQ0OFxcdTA0M2RcXHUwNDNlIFxcdTA0MzRcXHUwNDNlXFx1MDQzMVxcdTA0MzBcXHUwNDMyXFx1MDQzYlxcdTA0MzVcXHUwNDNkLlwiXSxbXCJfX2pzb25fbWVzc2FnZVwiLDAsMjUsXCJcXHUwNDEyXFx1MDQzOFxcdTA0MzQgXFx1MDQzZVxcdTA0MzFcXHUwNDQxXFx1MDQzYlxcdTA0MzVcXHUwNDM0XFx1MDQzZVxcdTA0MzJcXHUwNDMwXFx1MDQzZFxcdTA0MzhcXHUwNDRmIFxcXCJcXHUwNDFhXFx1MDQyMiBcXHUwNDNhXFx1MDQzOFxcdTA0NDFcXHUwNDQyXFx1MDQzOFxcXCIgXFx1MDQzMVxcdTA0NGJcXHUwNDNiIFxcdTA0NDNcXHUwNDQxXFx1MDQzZlxcdTA0MzVcXHUwNDQ4XFx1MDQzZFxcdTA0M2UgXFx1MDQzNFxcdTA0M2VcXHUwNDMxXFx1MDQzMFxcdTA0MzJcXHUwNDNiXFx1MDQzNVxcdTA0M2QuXCJdLFtcIl9fanNvbl9tZXNzYWdlXCIsMCwyNSxcIlxcdTA0MTJcXHUwNDM4XFx1MDQzNCBcXHUwNDNlXFx1MDQzMVxcdTA0NDFcXHUwNDNiXFx1MDQzNVxcdTA0MzRcXHUwNDNlXFx1MDQzMlxcdTA0MzBcXHUwNDNkXFx1MDQzOFxcdTA0NGYgXFxcIlxcdTA0MWFcXHUwNDIyIFxcdTA0NDJcXHUwNDMwXFx1MDQzN1xcdTA0M2VcXHUwNDMxXFx1MDQzNVxcdTA0MzRcXHUwNDQwXFx1MDQzNVxcdTA0M2RcXHUwNDNkXFx1MDQzZVxcdTA0MzNcXHUwNDNlIFxcdTA0NDFcXHUwNDQzXFx1MDQ0MVxcdTA0NDJcXHUwNDMwXFx1MDQzMlxcdTA0MzBcXFwiIFxcdTA0MzFcXHUwNDRiXFx1MDQzYiBcXHUwNDQzXFx1MDQ0MVxcdTA0M2ZcXHUwNDM1XFx1MDQ0OFxcdTA0M2RcXHUwNDNlIFxcdTA0MzRcXHUwNDNlXFx1MDQzMVxcdTA0MzBcXHUwNDMyXFx1MDQzYlxcdTA0MzVcXHUwNDNkLlwiXSxbXCJfX2pzb25fbWVzc2FnZVwiLDAsMjUsXCJcXHUwNDEyXFx1MDQzOFxcdTA0MzQgXFx1MDQzZVxcdTA0MzFcXHUwNDQxXFx1MDQzYlxcdTA0MzVcXHUwNDM0XFx1MDQzZVxcdTA0MzJcXHUwNDMwXFx1MDQzZFxcdTA0MzhcXHUwNDRmIFxcXCJcXHUwNDFhXFx1MDQyMiBcXHUwNDNhXFx1MDQzZVxcdTA0M2JcXHUwNDM1XFx1MDQzZFxcdTA0M2RcXHUwNDNlXFx1MDQzM1xcdTA0M2UgXFx1MDQ0MVxcdTA0NDNcXHUwNDQxXFx1MDQ0MlxcdTA0MzBcXHUwNDMyXFx1MDQzMFxcXCIgXFx1MDQzMVxcdTA0NGJcXHUwNDNiIFxcdTA0NDNcXHUwNDQxXFx1MDQzZlxcdTA0MzVcXHUwNDQ4XFx1MDQzZFxcdTA0M2UgXFx1MDQzNFxcdTA0M2VcXHUwNDMxXFx1MDQzMFxcdTA0MzJcXHUwNDNiXFx1MDQzNVxcdTA0M2QuXCJdLFtcIl9fanNvbl9tZXNzYWdlXCIsMCwyNSxcIlxcdTA0MTJcXHUwNDM4XFx1MDQzNCBcXHUwNDNlXFx1MDQzMVxcdTA0NDFcXHUwNDNiXFx1MDQzNVxcdTA0MzRcXHUwNDNlXFx1MDQzMlxcdTA0MzBcXHUwNDNkXFx1MDQzOFxcdTA0NGYgXFxcIlxcdTA0MWFcXHUwNDIyIFxcdTA0MzNcXHUwNDNlXFx1MDQzYlxcdTA0MzVcXHUwNDNkXFx1MDQzZVxcdTA0NDFcXHUwNDQyXFx1MDQzZVxcdTA0M2ZcXHUwNDNkXFx1MDQzZVxcdTA0MzNcXHUwNDNlIFxcdTA0NDFcXHUwNDQzXFx1MDQ0MVxcdTA0NDJcXHUwNDMwXFx1MDQzMlxcdTA0MzBcXFwiIFxcdTA0MzFcXHUwNDRiXFx1MDQzYiBcXHUwNDQzXFx1MDQ0MVxcdTA0M2ZcXHUwNDM1XFx1MDQ0OFxcdTA0M2RcXHUwNDNlIFxcdTA0MzRcXHUwNDNlXFx1MDQzMVxcdTA0MzBcXHUwNDMyXFx1MDQzYlxcdTA0MzVcXHUwNDNkLlwiXSxbXCJfX2pzb25fbWVzc2FnZVwiLDAsMjUsXCJcXHUwNDEyXFx1MDQzOFxcdTA0MzQgXFx1MDQzZVxcdTA0MzFcXHUwNDQxXFx1MDQzYlxcdTA0MzVcXHUwNDM0XFx1MDQzZVxcdTA0MzJcXHUwNDMwXFx1MDQzZFxcdTA0MzhcXHUwNDRmIFxcXCJcXHUwNDFhXFx1MDQyMiBcXHUwNDQxXFx1MDQ0MlxcdTA0M2VcXHUwNDNmXFx1MDQ0YlxcXCIgXFx1MDQzMVxcdTA0NGJcXHUwNDNiIFxcdTA0NDNcXHUwNDQxXFx1MDQzZlxcdTA0MzVcXHUwNDQ4XFx1MDQzZFxcdTA0M2UgXFx1MDQzNFxcdTA0M2VcXHUwNDMxXFx1MDQzMFxcdTA0MzJcXHUwNDNiXFx1MDQzNVxcdTA0M2QuXCJdLFtcIl9fanNvbl9tZXNzYWdlXCIsMCwyNSxcIlxcdTA0MTJcXHUwNDM4XFx1MDQzNCBcXHUwNDNlXFx1MDQzMVxcdTA0NDFcXHUwNDNiXFx1MDQzNVxcdTA0MzRcXHUwNDNlXFx1MDQzMlxcdTA0MzBcXHUwNDNkXFx1MDQzOFxcdTA0NGYgXFxcIlxcdTA0MWFcXHUwNDIyIFxcdTA0MzJcXHUwNDM4XFx1MDQ0MVxcdTA0M2VcXHUwNDQ3XFx1MDQzZFxcdTA0NGJcXHUwNDQ1IFxcdTA0M2FcXHUwNDNlXFx1MDQ0MVxcdTA0NDJcXHUwNDM1XFx1MDQzOVxcXCIgXFx1MDQzMVxcdTA0NGJcXHUwNDNiIFxcdTA0NDNcXHUwNDQxXFx1MDQzZlxcdTA0MzVcXHUwNDQ4XFx1MDQzZFxcdTA0M2UgXFx1MDQzNFxcdTA0M2VcXHUwNDMxXFx1MDQzMFxcdTA0MzJcXHUwNDNiXFx1MDQzNVxcdTA0M2QuXCJdLFtcIl9fanNvbl9tZXNzYWdlXCIsMCwyNSxcIlxcdTA0MTJcXHUwNDM4XFx1MDQzNCBcXHUwNDNlXFx1MDQzMVxcdTA0NDFcXHUwNDNiXFx1MDQzNVxcdTA0MzRcXHUwNDNlXFx1MDQzMlxcdTA0MzBcXHUwNDNkXFx1MDQzOFxcdTA0NGYgXFxcIlxcdTA0MWFcXHUwNDIyIFxcdTA0M2FcXHUwNDNlXFx1MDQ0MVxcdTA0NDJcXHUwNDM1XFx1MDQzOSBcXHUwNDNiXFx1MDQzOFxcdTA0NDZcXHUwNDM1XFx1MDQzMlxcdTA0M2VcXHUwNDMzXFx1MDQzZSBcXHUwNDQ3XFx1MDQzNVxcdTA0NDBcXHUwNDM1XFx1MDQzZlxcdTA0MzBcXFwiIFxcdTA0MzFcXHUwNDRiXFx1MDQzYiBcXHUwNDQzXFx1MDQ0MVxcdTA0M2ZcXHUwNDM1XFx1MDQ0OFxcdTA0M2RcXHUwNDNlIFxcdTA0MzRcXHUwNDNlXFx1MDQzMVxcdTA0MzBcXHUwNDMyXFx1MDQzYlxcdTA0MzVcXHUwNDNkLlwiXSxbXCJfX2pzb25fbWVzc2FnZVwiLDAsMjUsXCJcXHUwNDEyXFx1MDQzOFxcdTA0MzQgXFx1MDQzZVxcdTA0MzFcXHUwNDQxXFx1MDQzYlxcdTA0MzVcXHUwNDM0XFx1MDQzZVxcdTA0MzJcXHUwNDMwXFx1MDQzZFxcdTA0MzhcXHUwNDRmIFxcXCJcXHUwNDFhXFx1MDQyMiBcXHUwNDNmXFx1MDQzYlxcdTA0MzVcXHUwNDQ3XFx1MDQzNVxcdTA0MzJcXHUwNDNlXFx1MDQzOSBcXHUwNDNhXFx1MDQzZVxcdTA0NDFcXHUwNDQyXFx1MDQzOFxcXCIgXFx1MDQzMVxcdTA0NGJcXHUwNDNiIFxcdTA0NDNcXHUwNDQxXFx1MDQzZlxcdTA0MzVcXHUwNDQ4XFx1MDQzZFxcdTA0M2UgXFx1MDQzNFxcdTA0M2VcXHUwNDMxXFx1MDQzMFxcdTA0MzJcXHUwNDNiXFx1MDQzNVxcdTA0M2QuXCJdLFtcIl9fanNvbl9tZXNzYWdlXCIsMCwyNSxcIlxcdTA0MTJcXHUwNDM4XFx1MDQzNCBcXHUwNDNlXFx1MDQzMVxcdTA0NDFcXHUwNDNiXFx1MDQzNVxcdTA0MzRcXHUwNDNlXFx1MDQzMlxcdTA0MzBcXHUwNDNkXFx1MDQzOFxcdTA0NGYgXFxcIlxcdTA0MWFcXHUwNDIyIFxcdTA0M2FcXHUwNDNlXFx1MDQ0MVxcdTA0NDJcXHUwNDM1XFx1MDQzOSBcXHUwNDQyXFx1MDQzMFxcdTA0MzdcXHUwNDMwXFxcIiBcXHUwNDMxXFx1MDQ0YlxcdTA0M2IgXFx1MDQ0M1xcdTA0NDFcXHUwNDNmXFx1MDQzNVxcdTA0NDhcXHUwNDNkXFx1MDQzZSBcXHUwNDM0XFx1MDQzZVxcdTA0MzFcXHUwNDMwXFx1MDQzMlxcdTA0M2JcXHUwNDM1XFx1MDQzZC5cIl0sW1wiX19qc29uX21lc3NhZ2VcIiwwLDI1LFwiXFx1MDQxMlxcdTA0MzhcXHUwNDM0IFxcdTA0M2VcXHUwNDMxXFx1MDQ0MVxcdTA0M2JcXHUwNDM1XFx1MDQzNFxcdTA0M2VcXHUwNDMyXFx1MDQzMFxcdTA0M2RcXHUwNDM4XFx1MDQ0ZiBcXFwiXFx1MDQxYVxcdTA0MjIgXFx1MDQzMVxcdTA0MzVcXHUwNDM0XFx1MDQ0MFxcdTA0MzVcXHUwNDNkXFx1MDQzZFxcdTA0M2VcXHUwNDM5IFxcdTA0M2FcXHUwNDNlXFx1MDQ0MVxcdTA0NDJcXHUwNDM4XFxcIiBcXHUwNDMxXFx1MDQ0YlxcdTA0M2IgXFx1MDQ0M1xcdTA0NDFcXHUwNDNmXFx1MDQzNVxcdTA0NDhcXHUwNDNkXFx1MDQzZSBcXHUwNDM0XFx1MDQzZVxcdTA0MzFcXHUwNDMwXFx1MDQzMlxcdTA0M2JcXHUwNDM1XFx1MDQzZC5cIl0sW1wiX19qc29uX21lc3NhZ2VcIiwwLDI1LFwiXFx1MDQxMlxcdTA0MzhcXHUwNDM0IFxcdTA0M2VcXHUwNDMxXFx1MDQ0MVxcdTA0M2JcXHUwNDM1XFx1MDQzNFxcdTA0M2VcXHUwNDMyXFx1MDQzMFxcdTA0M2RcXHUwNDM4XFx1MDQ0ZiBcXFwiXFx1MDQxYVxcdTA0MjIgXFx1MDQzYVxcdTA0M2VcXHUwNDQxXFx1MDQ0MlxcdTA0MzVcXHUwNDM5IFxcdTA0MzNcXHUwNDNlXFx1MDQzYlxcdTA0MzVcXHUwNDNkXFx1MDQzOFxcXCIgXFx1MDQzMVxcdTA0NGJcXHUwNDNiIFxcdTA0NDNcXHUwNDQxXFx1MDQzZlxcdTA0MzVcXHUwNDQ4XFx1MDQzZFxcdTA0M2UgXFx1MDQzNFxcdTA0M2VcXHUwNDMxXFx1MDQzMFxcdTA0MzJcXHUwNDNiXFx1MDQzNVxcdTA0M2QuXCJdLFtcIl9fanNvbl9tZXNzYWdlXCIsMCwyNSxcIlxcdTA0MTJcXHUwNDM4XFx1MDQzNCBcXHUwNDNlXFx1MDQzMVxcdTA0NDFcXHUwNDNiXFx1MDQzNVxcdTA0MzRcXHUwNDNlXFx1MDQzMlxcdTA0MzBcXHUwNDNkXFx1MDQzOFxcdTA0NGYgXFxcIlxcdTA0MWFcXHUwNDIyIFxcdTA0NDFcXHUwNDNlXFx1MDQ0MVxcdTA0NDNcXHUwNDM0XFx1MDQzZVxcdTA0MzJcXFwiIFxcdTA0MzFcXHUwNDRiXFx1MDQzYiBcXHUwNDQzXFx1MDQ0MVxcdTA0M2ZcXHUwNDM1XFx1MDQ0OFxcdTA0M2RcXHUwNDNlIFxcdTA0MzRcXHUwNDNlXFx1MDQzMVxcdTA0MzBcXHUwNDMyXFx1MDQzYlxcdTA0MzVcXHUwNDNkLlwiXSxbXCJfX2pzb25fbWVzc2FnZVwiLDAsMjUsXCJcXHUwNDEyXFx1MDQzOFxcdTA0MzQgXFx1MDQzZVxcdTA0MzFcXHUwNDQxXFx1MDQzYlxcdTA0MzVcXHUwNDM0XFx1MDQzZVxcdTA0MzJcXHUwNDMwXFx1MDQzZFxcdTA0MzhcXHUwNDRmIFxcXCJcXHUwNDFhXFx1MDQyMi1cXHUwNDMwXFx1MDQzZFxcdTA0MzNcXHUwNDM4XFx1MDQzZVxcdTA0MzNcXHUwNDQwXFx1MDQzMFxcdTA0NDRcXHUwNDM4XFx1MDQ0ZiBcXHUwNDQxXFx1MDQzZVxcdTA0NDFcXHUwNDQzXFx1MDQzNFxcdTA0M2VcXHUwNDMyIFxcdTA0MzNcXHUwNDNlXFx1MDQzYlxcdTA0M2VcXHUwNDMyXFx1MDQzZFxcdTA0M2VcXHUwNDMzXFx1MDQzZSBcXHUwNDNjXFx1MDQzZVxcdTA0MzdcXHUwNDMzXFx1MDQzMFxcXCIgXFx1MDQzMVxcdTA0NGJcXHUwNDNiIFxcdTA0NDNcXHUwNDQxXFx1MDQzZlxcdTA0MzVcXHUwNDQ4XFx1MDQzZFxcdTA0M2UgXFx1MDQzNFxcdTA0M2VcXHUwNDMxXFx1MDQzMFxcdTA0MzJcXHUwNDNiXFx1MDQzNVxcdTA0M2QuXCJdLFtcIl9fanNvbl9tZXNzYWdlXCIsMCwyNSxcIlxcdTA0MTJcXHUwNDM4XFx1MDQzNCBcXHUwNDNlXFx1MDQzMVxcdTA0NDFcXHUwNDNiXFx1MDQzNVxcdTA0MzRcXHUwNDNlXFx1MDQzMlxcdTA0MzBcXHUwNDNkXFx1MDQzOFxcdTA0NGYgXFxcIlxcdTA0MWFcXHUwNDIyLVxcdTA0MzBcXHUwNDNkXFx1MDQzM1xcdTA0MzhcXHUwNDNlXFx1MDQzM1xcdTA0NDBcXHUwNDMwXFx1MDQ0NFxcdTA0MzhcXHUwNDRmIFxcdTA0NDFcXHUwNDNlXFx1MDQ0MVxcdTA0NDNcXHUwNDM0XFx1MDQzZVxcdTA0MzIgXFx1MDQ0OFxcdTA0MzVcXHUwNDM4XFxcIiBcXHUwNDMxXFx1MDQ0YlxcdTA0M2IgXFx1MDQ0M1xcdTA0NDFcXHUwNDNmXFx1MDQzNVxcdTA0NDhcXHUwNDNkXFx1MDQzZSBcXHUwNDM0XFx1MDQzZVxcdTA0MzFcXHUwNDMwXFx1MDQzMlxcdTA0M2JcXHUwNDM1XFx1MDQzZC5cIl0sW1wiX19qc29uX21lc3NhZ2VcIiwwLDI1LFwiXFx1MDQxMlxcdTA0MzhcXHUwNDM0IFxcdTA0M2VcXHUwNDMxXFx1MDQ0MVxcdTA0M2JcXHUwNDM1XFx1MDQzNFxcdTA0M2VcXHUwNDMyXFx1MDQzMFxcdTA0M2RcXHUwNDM4XFx1MDQ0ZiBcXFwiXFx1MDQxYVxcdTA0MjIgXFx1MDQzM1xcdTA0NDBcXHUwNDQzXFx1MDQzNFxcdTA0M2RcXHUwNDNlXFx1MDQzOSBcXHUwNDMwXFx1MDQzZVxcdTA0NDBcXHUwNDQyXFx1MDQ0YiBcXHUwNDM4IFxcdTA0M2JcXHUwNDM1XFx1MDQzM1xcdTA0M2VcXHUwNDQ3XFx1MDQzZFxcdTA0M2VcXHUwNDM5IFxcdTA0MzBcXHUwNDQwXFx1MDQ0MlxcdTA0MzVcXHUwNDQwXFx1MDQzOFxcdTA0MzhcXFwiIFxcdTA0MzFcXHUwNDRiXFx1MDQzYiBcXHUwNDQzXFx1MDQ0MVxcdTA0M2ZcXHUwNDM1XFx1MDQ0OFxcdTA0M2RcXHUwNDNlIFxcdTA0MzRcXHUwNDNlXFx1MDQzMVxcdTA0MzBcXHUwNDMyXFx1MDQzYlxcdTA0MzVcXHUwNDNkLlwiXSxbXCJfX2pzb25fbWVzc2FnZVwiLDAsMjUsXCJcXHUwNDEyXFx1MDQzOFxcdTA0MzQgXFx1MDQzZVxcdTA0MzFcXHUwNDQxXFx1MDQzYlxcdTA0MzVcXHUwNDM0XFx1MDQzZVxcdTA0MzJcXHUwNDMwXFx1MDQzZFxcdTA0MzhcXHUwNDRmIFxcXCJcXHUwNDFhXFx1MDQyMi1cXHUwNDNhXFx1MDQzZVxcdTA0NDBcXHUwNDNlXFx1MDQzZFxcdTA0MzBcXHUwNDQwXFx1MDQzZVxcdTA0MzNcXHUwNDQwXFx1MDQzMFxcdTA0NDRcXHUwNDM4XFx1MDQ0ZlxcXCIgXFx1MDQzMVxcdTA0NGJcXHUwNDNiIFxcdTA0NDNcXHUwNDQxXFx1MDQzZlxcdTA0MzVcXHUwNDQ4XFx1MDQzZFxcdTA0M2UgXFx1MDQzNFxcdTA0M2VcXHUwNDMxXFx1MDQzMFxcdTA0MzJcXHUwNDNiXFx1MDQzNVxcdTA0M2QuXCJdLFtcIl9fanNvbl9tZXNzYWdlXCIsMCwyNSxcIlxcdTA0MTJcXHUwNDM4XFx1MDQzNCBcXHUwNDNlXFx1MDQzMVxcdTA0NDFcXHUwNDNiXFx1MDQzNVxcdTA0MzRcXHUwNDNlXFx1MDQzMlxcdTA0MzBcXHUwNDNkXFx1MDQzOFxcdTA0NGYgXFxcIlxcdTA0MWFcXHUwNDIyIFxcdTA0MzFcXHUwNDQwXFx1MDQ0ZVxcdTA0NDhcXHUwNDNkXFx1MDQzZVxcdTA0MzkgXFx1MDQzMFxcdTA0M2VcXHUwNDQwXFx1MDQ0MlxcdTA0NGIgXFx1MDQzOCBcXHUwNDM1XFx1MDQzNSBcXHUwNDMyXFx1MDQzNVxcdTA0NDJcXHUwNDMyXFx1MDQzNVxcdTA0MzlcXFwiIFxcdTA0MzFcXHUwNDRiXFx1MDQzYiBcXHUwNDQzXFx1MDQ0MVxcdTA0M2ZcXHUwNDM1XFx1MDQ0OFxcdTA0M2RcXHUwNDNlIFxcdTA0MzRcXHUwNDNlXFx1MDQzMVxcdTA0MzBcXHUwNDMyXFx1MDQzYlxcdTA0MzVcXHUwNDNkLlwiXSxbXCJfX2pzb25fbWVzc2FnZVwiLDAsMjUsXCJcXHUwNDEyXFx1MDQzOFxcdTA0MzQgXFx1MDQzZVxcdTA0MzFcXHUwNDQxXFx1MDQzYlxcdTA0MzVcXHUwNDM0XFx1MDQzZVxcdTA0MzJcXHUwNDMwXFx1MDQzZFxcdTA0MzhcXHUwNDRmIFxcXCJcXHUwNDFhXFx1MDQyMi1cXHUwNDMwXFx1MDQzZFxcdTA0MzNcXHUwNDM4XFx1MDQzZVxcdTA0MzNcXHUwNDQwXFx1MDQzMFxcdTA0NDRcXHUwNDM4XFx1MDQ0ZiBcXHUwNDQxXFx1MDQzZVxcdTA0NDFcXHUwNDQzXFx1MDQzNFxcdTA0M2VcXHUwNDMyIFxcdTA0M2FcXHUwNDNlXFx1MDQzZFxcdTA0MzVcXHUwNDQ3XFx1MDQzZFxcdTA0M2VcXHUwNDQxXFx1MDQ0MlxcdTA0MzVcXHUwNDM5XFxcIiBcXHUwNDMxXFx1MDQ0YlxcdTA0M2IgXFx1MDQ0M1xcdTA0NDFcXHUwNDNmXFx1MDQzNVxcdTA0NDhcXHUwNDNkXFx1MDQzZSBcXHUwNDM0XFx1MDQzZVxcdTA0MzFcXHUwNDMwXFx1MDQzMlxcdTA0M2JcXHUwNDM1XFx1MDQzZC5cIl0sW1wiX19qc29uX21lc3NhZ2VcIiwwLDI1LFwiXFx1MDQxOFxcdTA0NDFcXHUwNDQxXFx1MDQzYlxcdTA0MzVcXHUwNDM0XFx1MDQzZVxcdTA0MzJcXHUwNDMwXFx1MDQ0MlxcdTA0MzVcXHUwNDNiXFx1MDQ0Y1xcdTA0NDFcXHUwNDNhXFx1MDQzOFxcdTA0MzkgXFx1MDQ0NlxcdTA0MzVcXHUwNDNkXFx1MDQ0MlxcdTA0NDAgXFxcIkFkdmFuY2VkIEZlcnRpbGl0eSBDbGluaWNcXFwiIFxcdTA0MzFcXHUwNDRiXFx1MDQzYiBcXHUwNDQzXFx1MDQ0MVxcdTA0M2ZcXHUwNDM1XFx1MDQ0OFxcdTA0M2RcXHUwNDNlIFxcdTA0MzhcXHUwNDM3XFx1MDQzY1xcdTA0MzVcXHUwNDNkXFx1MDQzNVxcdTA0M2QuXCJdLFtcIl9fanNvbl9tZXNzYWdlXCIsMCwyNSxcIlxcdTA0MThcXHUwNDQxXFx1MDQ0MVxcdTA0M2JcXHUwNDM1XFx1MDQzNFxcdTA0M2VcXHUwNDMyXFx1MDQzMFxcdTA0NDJcXHUwNDM1XFx1MDQzYlxcdTA0NGNcXHUwNDQxXFx1MDQzYVxcdTA0MzhcXHUwNDM5IFxcdTA0NDZcXHUwNDM1XFx1MDQzZFxcdTA0NDJcXHUwNDQwIFxcXCJBZHZhbmNlZCBGZXJ0aWxpdHkgQ2xpbmljXFxcIiBcXHUwNDMxXFx1MDQ0YlxcdTA0M2IgXFx1MDQ0M1xcdTA0NDFcXHUwNDNmXFx1MDQzNVxcdTA0NDhcXHUwNDNkXFx1MDQzZSBcXHUwNDM4XFx1MDQzN1xcdTA0M2NcXHUwNDM1XFx1MDQzZFxcdTA0MzVcXHUwNDNkLlwiXSxbXCJfX2pzb25fbWVzc2FnZVwiLDAsMjUsXCJcXHUwNDE4XFx1MDQ0MVxcdTA0NDFcXHUwNDNiXFx1MDQzNVxcdTA0MzRcXHUwNDNlXFx1MDQzMlxcdTA0MzBcXHUwNDQyXFx1MDQzNVxcdTA0M2JcXHUwNDRjXFx1MDQ0MVxcdTA0M2FcXHUwNDM4XFx1MDQzOSBcXHUwNDQ2XFx1MDQzNVxcdTA0M2RcXHUwNDQyXFx1MDQ0MCBcXFwiQWR2YW5jZWQgRmVydGlsaXR5IENsaW5pY1xcXCIgXFx1MDQzMVxcdTA0NGJcXHUwNDNiIFxcdTA0NDNcXHUwNDQxXFx1MDQzZlxcdTA0MzVcXHUwNDQ4XFx1MDQzZFxcdTA0M2UgXFx1MDQzOFxcdTA0MzdcXHUwNDNjXFx1MDQzNVxcdTA0M2RcXHUwNDM1XFx1MDQzZC5cIl0sW1wiX19qc29uX21lc3NhZ2VcIiwwLDI1LFwiXFx1MDQxOFxcdTA0NDFcXHUwNDQxXFx1MDQzYlxcdTA0MzVcXHUwNDM0XFx1MDQzZVxcdTA0MzJcXHUwNDMwXFx1MDQ0MlxcdTA0MzVcXHUwNDNiXFx1MDQ0Y1xcdTA0NDFcXHUwNDNhXFx1MDQzOFxcdTA0MzkgXFx1MDQ0NlxcdTA0MzVcXHUwNDNkXFx1MDQ0MlxcdTA0NDAgXFxcIkFkdmFuY2VkIEZlcnRpbGl0eSBDbGluaWNcXFwiIFxcdTA0MzFcXHUwNDRiXFx1MDQzYiBcXHUwNDQzXFx1MDQ0MVxcdTA0M2ZcXHUwNDM1XFx1MDQ0OFxcdTA0M2RcXHUwNDNlIFxcdTA0MzhcXHUwNDM3XFx1MDQzY1xcdTA0MzVcXHUwNDNkXFx1MDQzNVxcdTA0M2QuXCJdLFtcIl9fanNvbl9tZXNzYWdlXCIsMCwyNSxcIlxcdTA0MThcXHUwNDQxXFx1MDQ0MVxcdTA0M2JcXHUwNDM1XFx1MDQzNFxcdTA0M2VcXHUwNDMyXFx1MDQzMFxcdTA0NDJcXHUwNDM1XFx1MDQzYlxcdTA0NGNcXHUwNDQxXFx1MDQzYVxcdTA0MzhcXHUwNDM5IFxcdTA0NDZcXHUwNDM1XFx1MDQzZFxcdTA0NDJcXHUwNDQwIFxcXCJBZHZhbmNlZCBGZXJ0aWxpdHkgQ2xpbmljXFxcIiBcXHUwNDMxXFx1MDQ0YlxcdTA0M2IgXFx1MDQ0M1xcdTA0NDFcXHUwNDNmXFx1MDQzNVxcdTA0NDhcXHUwNDNkXFx1MDQzZSBcXHUwNDM4XFx1MDQzN1xcdTA0M2NcXHUwNDM1XFx1MDQzZFxcdTA0MzVcXHUwNDNkLlwiXSxbXCJfX2pzb25fbWVzc2FnZVwiLDAsMjUsXCJcXHUwNDE4XFx1MDQ0MVxcdTA0NDFcXHUwNDNiXFx1MDQzNVxcdTA0MzRcXHUwNDNlXFx1MDQzMlxcdTA0MzBcXHUwNDQyXFx1MDQzNVxcdTA0M2JcXHUwNDRjXFx1MDQ0MVxcdTA0M2FcXHUwNDM4XFx1MDQzOSBcXHUwNDQ2XFx1MDQzNVxcdTA0M2RcXHUwNDQyXFx1MDQ0MCBcXFwiQWR2YW5jZWQgRmVydGlsaXR5IENsaW5pY1xcXCIgXFx1MDQzMVxcdTA0NGJcXHUwNDNiIFxcdTA0NDNcXHUwNDQxXFx1MDQzZlxcdTA0MzVcXHUwNDQ4XFx1MDQzZFxcdTA0M2UgXFx1MDQzOFxcdTA0MzdcXHUwNDNjXFx1MDQzNVxcdTA0M2RcXHUwNDM1XFx1MDQzZC5cIl0sW1wiX19qc29uX21lc3NhZ2VcIiwwLDI1LFwiXFx1MDQxOFxcdTA0NDFcXHUwNDQxXFx1MDQzYlxcdTA0MzVcXHUwNDM0XFx1MDQzZVxcdTA0MzJcXHUwNDMwXFx1MDQ0MlxcdTA0MzVcXHUwNDNiXFx1MDQ0Y1xcdTA0NDFcXHUwNDNhXFx1MDQzOFxcdTA0MzkgXFx1MDQ0NlxcdTA0MzVcXHUwNDNkXFx1MDQ0MlxcdTA0NDAgXFxcIkFkdmFuY2VkIEZlcnRpbGl0eSBDbGluaWNcXFwiIFxcdTA0MzFcXHUwNDRiXFx1MDQzYiBcXHUwNDQzXFx1MDQ0MVxcdTA0M2ZcXHUwNDM1XFx1MDQ0OFxcdTA0M2RcXHUwNDNlIFxcdTA0MzhcXHUwNDM3XFx1MDQzY1xcdTA0MzVcXHUwNDNkXFx1MDQzNVxcdTA0M2QuXCJdXSIsIl9hdXRoX3VzZXJfaWQiOjEsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=','2014-06-13 15:43:07'),('o69bc6pzqg9cdgaxlb4zd93ghjzahbhg','MWM5NTU1MWY1MmYxOWI3MTg5YTFkOWY3NmU3MDk5YTU5ZGI2YzY5ZTp7fQ==','2014-06-13 14:16:40'),('cne2xxqoviyhqs03vvzje19ekwcfoqqk','MWM5NTU1MWY1MmYxOWI3MTg5YTFkOWY3NmU3MDk5YTU5ZGI2YzY5ZTp7fQ==','2014-06-13 22:29:32');
/*!40000 ALTER TABLE `django_session` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_site`
--

DROP TABLE IF EXISTS `django_site`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_site` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `domain` varchar(100) NOT NULL,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_site`
--

LOCK TABLES `django_site` WRITE;
/*!40000 ALTER TABLE `django_site` DISABLE KEYS */;
INSERT INTO `django_site` VALUES (1,'example.com','example.com');
/*!40000 ALTER TABLE `django_site` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `easy_thumbnails_source`
--

DROP TABLE IF EXISTS `easy_thumbnails_source`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `easy_thumbnails_source` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `modified` datetime NOT NULL,
  `storage_hash` varchar(40) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `easy_thumbnails_source_name_39229697_uniq` (`name`,`storage_hash`),
  KEY `easy_thumbnails_source_4da47e07` (`name`),
  KEY `easy_thumbnails_source_3f0464e5` (`storage_hash`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `easy_thumbnails_source`
--

LOCK TABLES `easy_thumbnails_source` WRITE;
/*!40000 ALTER TABLE `easy_thumbnails_source` DISABLE KEYS */;
/*!40000 ALTER TABLE `easy_thumbnails_source` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `easy_thumbnails_thumbnail`
--

DROP TABLE IF EXISTS `easy_thumbnails_thumbnail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `easy_thumbnails_thumbnail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `modified` datetime NOT NULL,
  `source_id` int(11) NOT NULL,
  `storage_hash` varchar(40) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `easy_thumbnails_thumbnail_source_id_47e6eb80_uniq` (`source_id`,`name`,`storage_hash`),
  KEY `easy_thumbnails_thumbnail_a34b03a6` (`source_id`),
  KEY `easy_thumbnails_thumbnail_4da47e07` (`name`),
  KEY `easy_thumbnails_thumbnail_3f0464e5` (`storage_hash`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `easy_thumbnails_thumbnail`
--

LOCK TABLES `easy_thumbnails_thumbnail` WRITE;
/*!40000 ALTER TABLE `easy_thumbnails_thumbnail` DISABLE KEYS */;
/*!40000 ALTER TABLE `easy_thumbnails_thumbnail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `easy_thumbnails_thumbnaildimensions`
--

DROP TABLE IF EXISTS `easy_thumbnails_thumbnaildimensions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `easy_thumbnails_thumbnaildimensions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `thumbnail_id` int(11) NOT NULL,
  `width` int(10) unsigned DEFAULT NULL,
  `height` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `thumbnail_id` (`thumbnail_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `easy_thumbnails_thumbnaildimensions`
--

LOCK TABLES `easy_thumbnails_thumbnaildimensions` WRITE;
/*!40000 ALTER TABLE `easy_thumbnails_thumbnaildimensions` DISABLE KEYS */;
/*!40000 ALTER TABLE `easy_thumbnails_thumbnaildimensions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `filer_clipboard`
--

DROP TABLE IF EXISTS `filer_clipboard`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `filer_clipboard` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `filer_clipboard_6340c63c` (`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `filer_clipboard`
--

LOCK TABLES `filer_clipboard` WRITE;
/*!40000 ALTER TABLE `filer_clipboard` DISABLE KEYS */;
INSERT INTO `filer_clipboard` VALUES (1,1);
/*!40000 ALTER TABLE `filer_clipboard` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `filer_clipboarditem`
--

DROP TABLE IF EXISTS `filer_clipboarditem`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `filer_clipboarditem` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `file_id` int(11) NOT NULL,
  `clipboard_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `filer_clipboarditem_4e9667b7` (`file_id`),
  KEY `filer_clipboarditem_8d9ffea2` (`clipboard_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `filer_clipboarditem`
--

LOCK TABLES `filer_clipboarditem` WRITE;
/*!40000 ALTER TABLE `filer_clipboarditem` DISABLE KEYS */;
/*!40000 ALTER TABLE `filer_clipboarditem` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `filer_file`
--

DROP TABLE IF EXISTS `filer_file`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `filer_file` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `folder_id` int(11) DEFAULT NULL,
  `file` varchar(255) DEFAULT NULL,
  `_file_size` int(11) DEFAULT NULL,
  `has_all_mandatory_data` tinyint(1) NOT NULL,
  `original_filename` varchar(255) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `owner_id` int(11) DEFAULT NULL,
  `uploaded_at` datetime NOT NULL,
  `modified_at` datetime NOT NULL,
  `description` longtext,
  `is_public` tinyint(1) NOT NULL,
  `sha1` varchar(40) NOT NULL,
  `polymorphic_ctype_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `filer_file_3aef490b` (`folder_id`),
  KEY `filer_file_cb902d83` (`owner_id`),
  KEY `filer_file_0022f76a` (`polymorphic_ctype_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `filer_file`
--

LOCK TABLES `filer_file` WRITE;
/*!40000 ALTER TABLE `filer_file` DISABLE KEYS */;
/*!40000 ALTER TABLE `filer_file` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `filer_folder`
--

DROP TABLE IF EXISTS `filer_folder`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `filer_folder` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `owner_id` int(11) DEFAULT NULL,
  `uploaded_at` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  `modified_at` datetime NOT NULL,
  `lft` int(10) unsigned NOT NULL,
  `rght` int(10) unsigned NOT NULL,
  `tree_id` int(10) unsigned NOT NULL,
  `level` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `filer_folder_parent_id_4d901e49_uniq` (`parent_id`,`name`),
  KEY `filer_folder_410d0aac` (`parent_id`),
  KEY `filer_folder_cb902d83` (`owner_id`),
  KEY `filer_folder_f777e2bb` (`lft`),
  KEY `filer_folder_76886718` (`rght`),
  KEY `filer_folder_f391089a` (`tree_id`),
  KEY `filer_folder_b8f3f94a` (`level`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `filer_folder`
--

LOCK TABLES `filer_folder` WRITE;
/*!40000 ALTER TABLE `filer_folder` DISABLE KEYS */;
/*!40000 ALTER TABLE `filer_folder` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `filer_folderpermission`
--

DROP TABLE IF EXISTS `filer_folderpermission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `filer_folderpermission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `folder_id` int(11) DEFAULT NULL,
  `type` smallint(6) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `group_id` int(11) DEFAULT NULL,
  `everybody` tinyint(1) NOT NULL,
  `can_edit` smallint(6) DEFAULT NULL,
  `can_read` smallint(6) DEFAULT NULL,
  `can_add_children` smallint(6) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `filer_folderpermission_3aef490b` (`folder_id`),
  KEY `filer_folderpermission_6340c63c` (`user_id`),
  KEY `filer_folderpermission_5f412f9a` (`group_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `filer_folderpermission`
--

LOCK TABLES `filer_folderpermission` WRITE;
/*!40000 ALTER TABLE `filer_folderpermission` DISABLE KEYS */;
/*!40000 ALTER TABLE `filer_folderpermission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `filer_image`
--

DROP TABLE IF EXISTS `filer_image`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `filer_image` (
  `file_ptr_id` int(11) NOT NULL,
  `_height` int(11) DEFAULT NULL,
  `_width` int(11) DEFAULT NULL,
  `date_taken` datetime DEFAULT NULL,
  `default_alt_text` varchar(255) DEFAULT NULL,
  `default_caption` varchar(255) DEFAULT NULL,
  `author` varchar(255) DEFAULT NULL,
  `must_always_publish_author_credit` tinyint(1) NOT NULL,
  `must_always_publish_copyright` tinyint(1) NOT NULL,
  `subject_location` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`file_ptr_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `filer_image`
--

LOCK TABLES `filer_image` WRITE;
/*!40000 ALTER TABLE `filer_image` DISABLE KEYS */;
/*!40000 ALTER TABLE `filer_image` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `forms_field`
--

DROP TABLE IF EXISTS `forms_field`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `forms_field` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `label` varchar(200) NOT NULL,
  `field_type` int(11) NOT NULL,
  `required` tinyint(1) NOT NULL,
  `visible` tinyint(1) NOT NULL,
  `choices` varchar(1000) NOT NULL,
  `default` varchar(2000) NOT NULL,
  `placeholder_text` varchar(100) DEFAULT NULL,
  `help_text` varchar(100) NOT NULL,
  `form_id` int(11) NOT NULL,
  `order` int(11) DEFAULT NULL,
  `slug` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `forms_field_c3d79a6c` (`form_id`),
  KEY `forms_field_f52cfca0` (`slug`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `forms_field`
--

LOCK TABLES `forms_field` WRITE;
/*!40000 ALTER TABLE `forms_field` DISABLE KEYS */;
/*!40000 ALTER TABLE `forms_field` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `forms_fieldentry`
--

DROP TABLE IF EXISTS `forms_fieldentry`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `forms_fieldentry` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `field_id` int(11) NOT NULL,
  `value` varchar(2000) DEFAULT NULL,
  `entry_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `forms_fieldentry_e8d920b6` (`entry_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `forms_fieldentry`
--

LOCK TABLES `forms_fieldentry` WRITE;
/*!40000 ALTER TABLE `forms_fieldentry` DISABLE KEYS */;
/*!40000 ALTER TABLE `forms_fieldentry` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `forms_form`
--

DROP TABLE IF EXISTS `forms_form`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `forms_form` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) NOT NULL,
  `slug` varchar(100) NOT NULL,
  `intro` longtext NOT NULL,
  `button_text` varchar(50) NOT NULL,
  `response` longtext NOT NULL,
  `status` int(11) NOT NULL,
  `publish_date` datetime DEFAULT NULL,
  `expiry_date` datetime DEFAULT NULL,
  `login_required` tinyint(1) NOT NULL,
  `send_email` tinyint(1) NOT NULL,
  `email_from` varchar(75) NOT NULL,
  `email_copies` varchar(200) NOT NULL,
  `email_subject` varchar(200) NOT NULL,
  `email_message` longtext NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `slug` (`slug`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `forms_form`
--

LOCK TABLES `forms_form` WRITE;
/*!40000 ALTER TABLE `forms_form` DISABLE KEYS */;
/*!40000 ALTER TABLE `forms_form` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `forms_form_sites`
--

DROP TABLE IF EXISTS `forms_form_sites`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `forms_form_sites` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `form_id` int(11) NOT NULL,
  `site_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `forms_form_sites_form_id_66edaea_uniq` (`form_id`,`site_id`),
  KEY `forms_form_sites_c3d79a6c` (`form_id`),
  KEY `forms_form_sites_99732b5c` (`site_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `forms_form_sites`
--

LOCK TABLES `forms_form_sites` WRITE;
/*!40000 ALTER TABLE `forms_form_sites` DISABLE KEYS */;
/*!40000 ALTER TABLE `forms_form_sites` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `forms_formentry`
--

DROP TABLE IF EXISTS `forms_formentry`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `forms_formentry` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `entry_time` datetime NOT NULL,
  `form_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `forms_formentry_c3d79a6c` (`form_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `forms_formentry`
--

LOCK TABLES `forms_formentry` WRITE;
/*!40000 ALTER TABLE `forms_formentry` DISABLE KEYS */;
/*!40000 ALTER TABLE `forms_formentry` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gblocks_banner`
--

DROP TABLE IF EXISTS `gblocks_banner`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gblocks_banner` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `image` varchar(100) NOT NULL,
  `link` varchar(255) NOT NULL,
  `text` longtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gblocks_banner`
--

LOCK TABLES `gblocks_banner` WRITE;
/*!40000 ALTER TABLE `gblocks_banner` DISABLE KEYS */;
/*!40000 ALTER TABLE `gblocks_banner` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gblocks_image`
--

DROP TABLE IF EXISTS `gblocks_image`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gblocks_image` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gblocks_image`
--

LOCK TABLES `gblocks_image` WRITE;
/*!40000 ALTER TABLE `gblocks_image` DISABLE KEYS */;
/*!40000 ALTER TABLE `gblocks_image` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gblocks_text`
--

DROP TABLE IF EXISTS `gblocks_text`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gblocks_text` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `text` longtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gblocks_text`
--

LOCK TABLES `gblocks_text` WRITE;
/*!40000 ALTER TABLE `gblocks_text` DISABLE KEYS */;
INSERT INTO `gblocks_text` VALUES (1,'<a href=\"tel:+78129178080\"><span>(812)</span> 917-80-80</a></div>\r\n				<div class=\"time\">КРУГЛОСУТОЧНО <b>без выходных</b></div>');
/*!40000 ALTER TABLE `gblocks_text` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gblocks_title`
--

DROP TABLE IF EXISTS `gblocks_title`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gblocks_title` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gblocks_title`
--

LOCK TABLES `gblocks_title` WRITE;
/*!40000 ALTER TABLE `gblocks_title` DISABLE KEYS */;
/*!40000 ALTER TABLE `gblocks_title` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gblocks_titleandtext`
--

DROP TABLE IF EXISTS `gblocks_titleandtext`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gblocks_titleandtext` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `text` longtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gblocks_titleandtext`
--

LOCK TABLES `gblocks_titleandtext` WRITE;
/*!40000 ALTER TABLE `gblocks_titleandtext` DISABLE KEYS */;
INSERT INTO `gblocks_titleandtext` VALUES (1,'Текст вставлен через админку','						<p>Основное отличие КТ и МРТ состоит в разных физических явлениях, которые используются в аппаратах. В случае КТ — это рентгеновское излучение, которое дает представление о физическом состоянии вещества, а при МРТ — постоянное и пульсирующее магнитные поля, а также радиочастотное излучение, дающее информацию о распределении протонов (атомов водорода), т.е. о химическом строении тканей. </p>\r\n					    <p>В случае КТ врач не просто видит ткани, но может изучать их рентгеновскую плотность, которая меняется при заболеваниях; в случае же МРТ врач оценивает изображения лишь визуально. Довольно часто МРТ или КТ-исследование назначает лечащий врач, но, как правило, лучше бы он делал это, посоветовавшись с лучевым диагностом: в целом ряде случаев вместо дорогой МРТ можно использовать более дешевую, но не менее информативную компьютерную томографию. </p>\r\n					    <p>В целом, МРТ лучше различает мягкие ткани. Кости при этом не могут быть видны — резонанс от кальция отсутствует и костная ткань на МР-томограммах видна лишь опосредованно. Можно констатировать, что на сегодняшний день МРТ более информативна при диффузном и очаговом поражении структур головного мозга, патологии спинного мозга и краниоспинального стыка (здесь КТ вовсе неинформативна), поражении хрящевой ткани. КТ предпочтительная при заболеваниях грудной клетки, живота, таза. основания черепа. В ряде случаев, для установления правильного диагноза, приходится прибегать одновременно к МРТ и КТ.'),(2,'Почему МРТ?','Это исследование отличается максимальной проникающей способностью, хотя оно не слишком хорошо визуализирует костную ткань. Плотные структуры не становятся для радиоволн непроницаемым экраном, следовательно, кости не затрудняют визуализацию органов, расположенных позади них. Благодаря этому магнитно-резонансная томография позволяет получить отчетливую картину труднодоступных зон: головного мозга, средостения, глубоких лимфатических узлов и т.д.'),(3,'Что покажет КТ?','Как и рентген, компьютерная томография показывает состояние плотных образований (костей), полых структур (желудочно-кишечный тракт) и органов, заполненных воздухом (легкие). Однако в современных томографах можно легко управлять режимами подачи излучения и регистрации сигналов, благодаря чему методика КТ способна визуализировать и мягкотканные образования, «невидимые» для рентгенографии, например, головной мозг. Несмотря на известное сходство, КТ имеет множество преимуществ, в первую очередь, возможность послойного сканирования тканей, благодаря чему на полученных снимках можно подробно рассмотреть тонкие «срезы» тела, а не наслоение теней.');
/*!40000 ALTER TABLE `gblocks_titleandtext` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gblocks_titletextandimage`
--

DROP TABLE IF EXISTS `gblocks_titletextandimage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gblocks_titletextandimage` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `text` longtext NOT NULL,
  `image` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gblocks_titletextandimage`
--

LOCK TABLES `gblocks_titletextandimage` WRITE;
/*!40000 ALTER TABLE `gblocks_titletextandimage` DISABLE KEYS */;
/*!40000 ALTER TABLE `gblocks_titletextandimage` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `news_news`
--

DROP TABLE IF EXISTS `news_news`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `news_news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `slug` varchar(50) NOT NULL,
  `body` longtext NOT NULL,
  `short` longtext NOT NULL,
  `image` varchar(100) NOT NULL,
  `is_share` tinyint(1) NOT NULL,
  `display` tinyint(1) NOT NULL,
  `due_date` date DEFAULT NULL,
  `creation_date` datetime NOT NULL,
  `last_modification` datetime NOT NULL,
  `is_sended` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `slug` (`slug`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `news_news`
--

LOCK TABLES `news_news` WRITE;
/*!40000 ALTER TABLE `news_news` DISABLE KEYS */;
INSERT INTO `news_news` VALUES (1,'Главная','glavnaya','<p><span style=\"font-family: Helvetica, arial, freesans, clean, sans-serif; font-size: 15px; line-height: 25.5px;\">The quickest way to add rich text editing capabilities to your models is to use the included&nbsp;</span><tt style=\"box-sizing: border-box; font-family: Consolas, \'Liberation Mono\', Courier, monospace; font-size: 12px; margin: 0px; border: 1px solid rgb(221, 221, 221); background-color: rgb(248, 248, 248); border-top-left-radius: 3px; border-top-right-radius: 3px; border-bottom-right-radius: 3px; border-bottom-left-radius: 3px; padding: 0px;\">RichTextField</tt><span style=\"font-family: Helvetica, arial, freesans, clean, sans-serif; font-size: 15px; line-height: 25.5px;\">model field type. A CKEditor widget is rendered as the form field but in all other regards the field behaves as the standard Django&nbsp;</span><tt style=\"box-sizing: border-box; font-family: Consolas, \'Liberation Mono\', Courier, monospace; font-size: 12px; margin: 0px; border: 1px solid rgb(221, 221, 221); background-color: rgb(248, 248, 248); border-top-left-radius: 3px; border-top-right-radius: 3px; border-bottom-right-radius: 3px; border-bottom-left-radius: 3px; padding: 0px;\">TextField</tt><span style=\"font-family: Helvetica, arial, freesans, clean, sans-serif; font-size: 15px; line-height: 25.5px;\">. For example:</span></p>\r\n','<p><span style=\"font-family: Helvetica, arial, freesans, clean, sans-serif; font-size: 15px; line-height: 25.5px;\">The quickest way to add rich text editing capabilities to your models is to use the included&nbsp;</span><tt style=\"box-sizing: border-box; font-family: Consolas, \'Liberation Mono\', Courier, monospace; font-size: 12px; margin: 0px; border: 1px solid rgb(221, 221, 221); background-color: rgb(248, 248, 248); border-top-left-radius: 3px; border-top-right-radius: 3px; border-bottom-right-radius: 3px; border-bottom-left-radius: 3px; padding: 0px;\">RichTextField</tt><span style=\"font-family: Helvetica, arial, freesans, clean, sans-serif; font-size: 15px; line-height: 25.5px;\">model field type. A CKEditor widget is rendered as the form field but in all other regards the field behaves as the standard Django&nbsp;</span><tt style=\"box-sizing: border-box; font-family: Consolas, \'Liberation Mono\', Courier, monospace; font-size: 12px; margin: 0px; border: 1px solid rgb(221, 221, 221); background-color: rgb(248, 248, 248); border-top-left-radius: 3px; border-top-right-radius: 3px; border-bottom-right-radius: 3px; border-bottom-left-radius: 3px; padding: 0px;\">TextField</tt><span style=\"font-family: Helvetica, arial, freesans, clean, sans-serif; font-size: 15px; line-height: 25.5px;\">. For example:</span></p>\r\n','uploads/news/tomograph.jpg',0,1,'2014-05-31','2014-05-31 10:35:25','2014-05-31 21:42:57',0),(2,'Счастливые часы на МРТ!','schastlivye-chasy-na-mrt','<p><span style=\"font-family: Helvetica, arial, freesans, clean, sans-serif; font-size: 15px; line-height: 25.5px;\">The quickest way to add rich text editing capabilities to your models is to use the included&nbsp;</span><tt style=\"box-sizing: border-box; font-family: Consolas, \'Liberation Mono\', Courier, monospace; font-size: 12px; margin: 0px; border: 1px solid rgb(221, 221, 221); background-color: rgb(248, 248, 248); border-top-left-radius: 3px; border-top-right-radius: 3px; border-bottom-right-radius: 3px; border-bottom-left-radius: 3px; padding: 0px;\">RichTextField</tt><span style=\"font-family: Helvetica, arial, freesans, clean, sans-serif; font-size: 15px; line-height: 25.5px;\">model field type. A CKEditor widget is rendered as the form field but in all other regards the field behaves as the standard Django&nbsp;</span><tt style=\"box-sizing: border-box; font-family: Consolas, \'Liberation Mono\', Courier, monospace; font-size: 12px; margin: 0px; border: 1px solid rgb(221, 221, 221); background-color: rgb(248, 248, 248); border-top-left-radius: 3px; border-top-right-radius: 3px; border-bottom-right-radius: 3px; border-bottom-left-radius: 3px; padding: 0px;\">TextField</tt><span style=\"font-family: Helvetica, arial, freesans, clean, sans-serif; font-size: 15px; line-height: 25.5px;\">. For example:</span></p>\r\n','<p><span style=\"font-family: Helvetica, arial, freesans, clean, sans-serif; font-size: 15px; line-height: 25.5px;\">The quickest way to add rich text editing capabilities to your models is to use the included&nbsp;</span><tt style=\"box-sizing: border-box; font-family: Consolas, \'Liberation Mono\', Courier, monospace; font-size: 12px; margin: 0px; border: 1px solid rgb(221, 221, 221); background-color: rgb(248, 248, 248); border-top-left-radius: 3px; border-top-right-radius: 3px; border-bottom-right-radius: 3px; border-bottom-left-radius: 3px; padding: 0px;\">RichTextField</tt><span style=\"font-family: Helvetica, arial, freesans, clean, sans-serif; font-size: 15px; line-height: 25.5px;\">model field type. A CKEditor widget is rendered as the form field but in all other regards the field behaves as the standard Django&nbsp;</span><tt style=\"box-sizing: border-box; font-family: Consolas, \'Liberation Mono\', Courier, monospace; font-size: 12px; margin: 0px; border: 1px solid rgb(221, 221, 221); background-color: rgb(248, 248, 248); border-top-left-radius: 3px; border-top-right-radius: 3px; border-bottom-right-radius: 3px; border-bottom-left-radius: 3px; padding: 0px;\">TextField</tt><span style=\"font-family: Helvetica, arial, freesans, clean, sans-serif; font-size: 15px; line-height: 25.5px;\">. For example:</span></p>\r\n','uploads/news/tomograph_1.jpg',0,1,'2014-05-31','2014-05-31 10:45:28','2014-05-31 21:42:53',0),(3,'Прием врача до и после МРТ со скидкой 25%! ','priem-vracha-do-i-posle-mrt-so-skidkoj-25','<p><span style=\"font-family: Helvetica, arial, freesans, clean, sans-serif; font-size: 15px; line-height: 25.5px;\">The quickest way to add rich text editing capabilities to your models is to use the included&nbsp;</span><tt style=\"box-sizing: border-box; font-family: Consolas, \'Liberation Mono\', Courier, monospace; font-size: 12px; margin: 0px; border: 1px solid rgb(221, 221, 221); background-color: rgb(248, 248, 248); border-top-left-radius: 3px; border-top-right-radius: 3px; border-bottom-right-radius: 3px; border-bottom-left-radius: 3px; padding: 0px;\">RichTextField</tt><span style=\"font-family: Helvetica, arial, freesans, clean, sans-serif; font-size: 15px; line-height: 25.5px;\">model field type. A CKEditor widget is rendered as the form field but in all other regards the field behaves as the standard Django&nbsp;</span><tt style=\"box-sizing: border-box; font-family: Consolas, \'Liberation Mono\', Courier, monospace; font-size: 12px; margin: 0px; border: 1px solid rgb(221, 221, 221); background-color: rgb(248, 248, 248); border-top-left-radius: 3px; border-top-right-radius: 3px; border-bottom-right-radius: 3px; border-bottom-left-radius: 3px; padding: 0px;\">TextField</tt><span style=\"font-family: Helvetica, arial, freesans, clean, sans-serif; font-size: 15px; line-height: 25.5px;\">. For example:</span></p>\r\n','<p><span style=\"font-family: Helvetica, arial, freesans, clean, sans-serif; font-size: 15px; line-height: 25.5px;\">The quickest way to add rich text editing capabilities to your models is to use the included&nbsp;</span><tt style=\"box-sizing: border-box; font-family: Consolas, \'Liberation Mono\', Courier, monospace; font-size: 12px; margin: 0px; border: 1px solid rgb(221, 221, 221); background-color: rgb(248, 248, 248); border-top-left-radius: 3px; border-top-right-radius: 3px; border-bottom-right-radius: 3px; border-bottom-left-radius: 3px; padding: 0px;\">RichTextField</tt><span style=\"font-family: Helvetica, arial, freesans, clean, sans-serif; font-size: 15px; line-height: 25.5px;\">model field type. A CKEditor widget is rendered as the form field but in all other regards the field behaves as the standard Django&nbsp;</span><tt style=\"box-sizing: border-box; font-family: Consolas, \'Liberation Mono\', Courier, monospace; font-size: 12px; margin: 0px; border: 1px solid rgb(221, 221, 221); background-color: rgb(248, 248, 248); border-top-left-radius: 3px; border-top-right-radius: 3px; border-bottom-right-radius: 3px; border-bottom-left-radius: 3px; padding: 0px;\">TextField</tt><span style=\"font-family: Helvetica, arial, freesans, clean, sans-serif; font-size: 15px; line-height: 25.5px;\">. For example:</span></p>\r\n','uploads/news/tomograph_2.jpg',0,1,'2014-05-02','2014-05-31 10:45:39','2014-05-31 21:38:25',0);
/*!40000 ALTER TABLE `news_news` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `page_action`
--

DROP TABLE IF EXISTS `page_action`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `page_action` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `title` varchar(40) NOT NULL,
  `link` varchar(100) NOT NULL,
  `active` tinyint(1) NOT NULL,
  `position` int(11) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `page_id` int(11) DEFAULT NULL,
  `lft` int(10) unsigned NOT NULL,
  `rght` int(10) unsigned NOT NULL,
  `tree_id` int(10) unsigned NOT NULL,
  `level` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `page_action_5f412f9a` (`group_id`),
  KEY `page_action_410d0aac` (`parent_id`),
  KEY `page_action_3fb9c94f` (`page_id`),
  KEY `page_action_329f6fb3` (`lft`),
  KEY `page_action_e763210f` (`rght`),
  KEY `page_action_ba470c4a` (`tree_id`),
  KEY `page_action_20e079f4` (`level`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `page_action`
--

LOCK TABLES `page_action` WRITE;
/*!40000 ALTER TABLE `page_action` DISABLE KEYS */;
/*!40000 ALTER TABLE `page_action` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `page_actiongroup`
--

DROP TABLE IF EXISTS `page_actiongroup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `page_actiongroup` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `page_actiongroup`
--

LOCK TABLES `page_actiongroup` WRITE;
/*!40000 ALTER TABLE `page_actiongroup` DISABLE KEYS */;
/*!40000 ALTER TABLE `page_actiongroup` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `page_page`
--

DROP TABLE IF EXISTS `page_page`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `page_page` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `slug` varchar(100) NOT NULL,
  `short_text` longtext NOT NULL,
  `body` longtext NOT NULL,
  `active` tinyint(1) NOT NULL,
  `exclude_from_navigation` tinyint(1) NOT NULL,
  `position` int(11) NOT NULL,
  `file` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `page_page`
--

LOCK TABLES `page_page` WRITE;
/*!40000 ALTER TABLE `page_page` DISABLE KEYS */;
/*!40000 ALTER TABLE `page_page` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `seo_seometadatamodel`
--

DROP TABLE IF EXISTS `seo_seometadatamodel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `seo_seometadatamodel` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(250) NOT NULL,
  `description` varchar(250) NOT NULL,
  `keywords` varchar(511) NOT NULL,
  `_content_type_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `_content_type_id` (`_content_type_id`),
  KEY `seo_seometadatamodel_d674e98a` (`_content_type_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `seo_seometadatamodel`
--

LOCK TABLES `seo_seometadatamodel` WRITE;
/*!40000 ALTER TABLE `seo_seometadatamodel` DISABLE KEYS */;
/*!40000 ALTER TABLE `seo_seometadatamodel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `seo_seometadatamodelinstance`
--

DROP TABLE IF EXISTS `seo_seometadatamodelinstance`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `seo_seometadatamodelinstance` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(250) NOT NULL,
  `description` varchar(250) NOT NULL,
  `keywords` varchar(511) NOT NULL,
  `_path` varchar(255) NOT NULL,
  `_content_type_id` int(11) NOT NULL,
  `_object_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `_path` (`_path`),
  UNIQUE KEY `_path_2` (`_path`),
  UNIQUE KEY `_content_type_id` (`_content_type_id`,`_object_id`),
  KEY `seo_seometadatamodelinstance_d674e98a` (`_content_type_id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `seo_seometadatamodelinstance`
--

LOCK TABLES `seo_seometadatamodelinstance` WRITE;
/*!40000 ALTER TABLE `seo_seometadatamodelinstance` DISABLE KEYS */;
INSERT INTO `seo_seometadatamodelinstance` VALUES (1,'','','','/event/glavnaya',30,1),(2,'','','','/event/schastlivye-chasy-na-mrt',30,2),(3,'','','','/event/priem-vracha-do-i-posle-mrt-so-skidkoj-25',30,3);
/*!40000 ALTER TABLE `seo_seometadatamodelinstance` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `seo_seometadatapath`
--

DROP TABLE IF EXISTS `seo_seometadatapath`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `seo_seometadatapath` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(250) NOT NULL,
  `description` varchar(250) NOT NULL,
  `keywords` varchar(511) NOT NULL,
  `_path` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `_path` (`_path`),
  UNIQUE KEY `_path_2` (`_path`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `seo_seometadatapath`
--

LOCK TABLES `seo_seometadatapath` WRITE;
/*!40000 ALTER TABLE `seo_seometadatapath` DISABLE KEYS */;
/*!40000 ALTER TABLE `seo_seometadatapath` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `seo_seometadataview`
--

DROP TABLE IF EXISTS `seo_seometadataview`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `seo_seometadataview` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(250) NOT NULL,
  `description` varchar(250) NOT NULL,
  `keywords` varchar(511) NOT NULL,
  `_view` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `_view` (`_view`),
  UNIQUE KEY `_view_2` (`_view`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `seo_seometadataview`
--

LOCK TABLES `seo_seometadataview` WRITE;
/*!40000 ALTER TABLE `seo_seometadataview` DISABLE KEYS */;
/*!40000 ALTER TABLE `seo_seometadataview` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `slider_slider`
--

DROP TABLE IF EXISTS `slider_slider`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `slider_slider` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(250) NOT NULL,
  `show_on` varchar(250) NOT NULL,
  `image` varchar(100) NOT NULL,
  `name` varchar(250) NOT NULL,
  `url` varchar(10000) NOT NULL,
  `target` tinyint(1) NOT NULL,
  `display` tinyint(1) NOT NULL,
  `position` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `slider_slider`
--

LOCK TABLES `slider_slider` WRITE;
/*!40000 ALTER TABLE `slider_slider` DISABLE KEYS */;
/*!40000 ALTER TABLE `slider_slider` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `south_migrationhistory`
--

DROP TABLE IF EXISTS `south_migrationhistory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `south_migrationhistory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app_name` varchar(255) NOT NULL,
  `migration` varchar(255) NOT NULL,
  `applied` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=48 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `south_migrationhistory`
--

LOCK TABLES `south_migrationhistory` WRITE;
/*!40000 ALTER TABLE `south_migrationhistory` DISABLE KEYS */;
INSERT INTO `south_migrationhistory` VALUES (1,'menu','0001_initial','2014-05-29 13:46:40'),(2,'dashboard','0001_initial','2014-05-29 13:46:40'),(3,'dashboard','0002_auto__add_field_dashboardpreferences_dashboard_id','2014-05-29 13:46:40'),(4,'dashboard','0003_auto__add_unique_dashboardpreferences_dashboard_id_user','2014-05-29 13:46:40'),(5,'forms','0001_initial','2014-05-29 13:46:41'),(6,'forms','0002_auto__add_field_field_order','2014-05-29 13:46:41'),(7,'forms','0003_auto__add_field_field_slug','2014-05-29 13:46:42'),(8,'forms','0003_auto__chg_field_fieldentry_value','2014-05-29 13:46:42'),(9,'forms','0004_populate_field_slug','2014-05-29 13:46:42'),(10,'forms','0005_auto__chg_field_fieldentry_value__del_field_field__order__chg_field_fi','2014-05-29 13:46:42'),(11,'forms','0006_auto__del_unique_field_slug_form','2014-05-29 13:46:42'),(12,'filer','0001_initial','2014-05-29 13:46:44'),(13,'filer','0002_rename_file_field','2014-05-29 13:46:44'),(14,'filer','0003_add_description_field','2014-05-29 13:46:44'),(15,'filer','0004_auto__del_field_file__file__add_field_file_file__add_field_file_is_pub','2014-05-29 13:46:44'),(16,'filer','0005_auto__add_field_file_sha1__chg_field_file_file','2014-05-29 13:46:45'),(17,'filer','0006_polymorphic__add_field_file_polymorphic_ctype','2014-05-29 13:46:45'),(18,'filer','0007_polymorphic__content_type_data','2014-05-29 13:46:45'),(19,'filer','0008_polymorphic__del_field_file__file_type_plugin_name','2014-05-29 13:46:45'),(20,'filer','0009_auto__add_field_folderpermission_can_edit_new__add_field_folderpermiss','2014-05-29 13:46:45'),(21,'filer','0010_folderpermissions','2014-05-29 13:46:45'),(22,'filer','0011_auto__del_field_folderpermission_can_add_children__del_field_folderper','2014-05-29 13:46:45'),(23,'filer','0012_renaming_folderpermissions','2014-05-29 13:46:46'),(24,'filer','0013_remove_null_file_name','2014-05-29 13:46:46'),(25,'filer','0014_auto__add_field_image_related_url__chg_field_file_name','2014-05-29 13:46:46'),(26,'easy_thumbnails','0001_initial','2014-05-29 13:46:47'),(27,'easy_thumbnails','0002_filename_indexes','2014-05-29 13:46:47'),(28,'easy_thumbnails','0003_auto__add_storagenew','2014-05-29 13:46:47'),(29,'easy_thumbnails','0004_auto__add_field_source_storage_new__add_field_thumbnail_storage_new','2014-05-29 13:46:47'),(30,'easy_thumbnails','0005_storage_fks_null','2014-05-29 13:46:48'),(31,'easy_thumbnails','0006_copy_storage','2014-05-29 13:46:48'),(32,'easy_thumbnails','0007_storagenew_fks_not_null','2014-05-29 13:46:48'),(33,'easy_thumbnails','0008_auto__del_field_source_storage__del_field_thumbnail_storage','2014-05-29 13:46:48'),(34,'easy_thumbnails','0009_auto__del_storage','2014-05-29 13:46:48'),(35,'easy_thumbnails','0010_rename_storage','2014-05-29 13:46:49'),(36,'easy_thumbnails','0011_auto__add_field_source_storage_hash__add_field_thumbnail_storage_hash','2014-05-29 13:46:49'),(37,'easy_thumbnails','0012_build_storage_hashes','2014-05-29 13:46:49'),(38,'easy_thumbnails','0013_auto__del_storage__del_field_source_storage__del_field_thumbnail_stora','2014-05-29 13:46:49'),(39,'easy_thumbnails','0014_auto__add_unique_source_name_storage_hash__add_unique_thumbnail_name_s','2014-05-29 13:46:49'),(40,'easy_thumbnails','0015_auto__del_unique_thumbnail_name_storage_hash__add_unique_thumbnail_sou','2014-05-29 13:46:49'),(41,'easy_thumbnails','0016_auto__add_thumbnaildimensions','2014-05-29 13:46:49'),(42,'page','0001_initial','2014-05-29 13:46:50'),(43,'news','0001_initial','2014-05-29 13:46:50'),(44,'slider','0001_initial','2014-05-29 13:46:50'),(45,'device','0001_initial','2014-05-29 13:47:52'),(46,'device','0002_auto__chg_field_metro_name','2014-05-29 13:49:09'),(47,'device','0003_auto__chg_field_pricelist_value','2014-05-29 16:08:00');
/*!40000 ALTER TABLE `south_migrationhistory` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `thumbnail_kvstore`
--

DROP TABLE IF EXISTS `thumbnail_kvstore`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `thumbnail_kvstore` (
  `key` varchar(200) NOT NULL,
  `value` longtext NOT NULL,
  PRIMARY KEY (`key`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `thumbnail_kvstore`
--

LOCK TABLES `thumbnail_kvstore` WRITE;
/*!40000 ALTER TABLE `thumbnail_kvstore` DISABLE KEYS */;
INSERT INTO `thumbnail_kvstore` VALUES ('sorl-thumbnail||image||3c508897135b9b7b730df6745fbc5f42','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"uploads/news/tomograph.jpg\", \"size\": [120, 116]}'),('sorl-thumbnail||image||786ad5a9dd3e7b45853c047a74a4362d','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"cache/48/6f/486f9ad6b966a5fb01eb020a787c2628.jpg\", \"size\": [70, 68]}'),('sorl-thumbnail||thumbnails||3c508897135b9b7b730df6745fbc5f42','[\"786ad5a9dd3e7b45853c047a74a4362d\"]'),('sorl-thumbnail||image||4005210678b8190208a72f3e6999a909','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"cache/ff/52/ff52c6b0bc5d5151d32cfdc8b8b63f80.jpg\", \"size\": [150, 150]}'),('sorl-thumbnail||image||8bad9c00054e20cbf480260f2f4c6b90','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"cache/a8/ea/a8ea64ba9b68a6605b89780c9774c6d5.jpg\", \"size\": [300, 290]}'),('sorl-thumbnail||image||0594c54aa3538b7f1105ad8710137c9e','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"cache/32/fb/32fb7b7da1eb2a9eabf45878e6a693f9.jpg\", \"size\": [120, 116]}'),('sorl-thumbnail||image||0a76cef68340d4b13a2bfc47ce1b5ab0','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"cache/e0/b4/e0b4cb81e7a35132b8a1e4e8c795d803.jpg\", \"size\": [83, 80]}'),('sorl-thumbnail||image||be279145c516467826c340b963bfd691','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"uploads/news/tomograph_2.jpg\", \"size\": [120, 116]}'),('sorl-thumbnail||image||3a4b1535bc1ad49c7fe17218af15468a','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"cache/ea/6a/ea6a92d8ec8c735dcfd8fce47fd9cf09.jpg\", \"size\": [70, 68]}'),('sorl-thumbnail||thumbnails||be279145c516467826c340b963bfd691','[\"3a4b1535bc1ad49c7fe17218af15468a\"]'),('sorl-thumbnail||image||2654c351f90acdd91843a34d053306c2','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"uploads/news/tomograph_1.jpg\", \"size\": [120, 116]}'),('sorl-thumbnail||image||f0692af21329f6bd2a639853df0c4b87','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"cache/57/38/5738ee2debc645e268a0b6d2fa7b6431.jpg\", \"size\": [70, 68]}'),('sorl-thumbnail||thumbnails||2654c351f90acdd91843a34d053306c2','[\"f0692af21329f6bd2a639853df0c4b87\"]'),('sorl-thumbnail||image||223483f2654d2ce85280ce91f68949ac','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"cache/ed/9e/ed9e2bfb9dd23a277094e56289719e69.jpg\", \"size\": [120, 116]}'),('sorl-thumbnail||image||32931b4b0746d36841614e82eca700a3','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"cache/f8/d7/f8d763d38388112b9e9ce2fc200590d8.jpg\", \"size\": [120, 116]}'),('sorl-thumbnail||image||a8d998f28ad62131a8bb62584a8a8483','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"cache/29/12/2912a1618d4f6b95dfcaaecd350cfa15.jpg\", \"size\": [83, 80]}'),('sorl-thumbnail||image||d165f4ff462174f0831f4affa64b7db2','{\"storage\": \"django.core.files.storage.FileSystemStorage\", \"name\": \"cache/58/f4/58f4547878844bf8a1b2d98ecffe1f6e.jpg\", \"size\": [83, 80]}');
/*!40000 ALTER TABLE `thumbnail_kvstore` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-06-05 18:37:00
