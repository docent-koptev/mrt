# -*- coding: utf-8 -*-
from annoying.decorators import render_to
from plugins.slider.models import Slider
from plugins.device.models import Center,Inspection,Apparat,Metro,PriceList,Power
from django.shortcuts import get_object_or_404, redirect, render,render_to_response
from device.forms import OrderForm

@render_to(template='base.html')
def home(request):
	metro = Metro.objects.all().distinct().order_by("name")
	price_list = PriceList.objects.filter(center__active=True)

	insp_mrt = Inspection.objects.filter(parent=None,mrt=True)
	insp_kt = Inspection.objects.filter(parent=None,kt=True)

	insp_mrt_all = Inspection.objects.filter(mrt=True).exclude(parent=None)
	insp_kt_all = Inspection.objects.filter(kt=True).exclude(parent=None)

	all_inspections =  Inspection.objects.all().exclude(parent=None).order_by('-name')
	power = Power.objects.filter(slice=False).order_by("value")
	slice = Power.objects.filter(slice=True)
	form = OrderForm()
 	return locals()

@render_to(template='reviews/reviews.html')
def reviews(request):
	return locals()

def robots(request):
    return render(request, 'robots.txt', {}, content_type='text/plain')