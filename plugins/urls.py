# -*- coding: utf-8 -*-
from django.conf.urls import *
from django.views.generic.base import TemplateView

urlpatterns = patterns('plugins.views',
    url(r'^$', 'home', name='home'),
    url(r'^', include('plugins.page.urls')),
	url(r'^event/', include('plugins.news.urls')),
	url(r'^inspection/', include('plugins.device.urls')),
	url(r'^reviews/list$', 'reviews',name='reviews'),
	url(r'^robots.txt$', 'robots', name='robots'),
)
