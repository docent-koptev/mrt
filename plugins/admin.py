# -*- coding: utf-8 -*-
from django.contrib import admin
from rollyourown.seo.admin import register_seo_admin
from seo import SEOMetadata

register_seo_admin(admin.site, SEOMetadata)

