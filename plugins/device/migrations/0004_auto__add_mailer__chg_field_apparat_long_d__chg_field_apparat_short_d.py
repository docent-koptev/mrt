# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Mailer'
        db.create_table(u'device_mailer', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('from_email', self.gf('django.db.models.fields.EmailField')(max_length=75)),
            ('notification_emails', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
        ))
        db.send_create_signal(u'device', ['Mailer'])


        # Changing field 'Apparat.long_d'
        db.alter_column(u'device_apparat', 'long_d', self.gf('django.db.models.fields.TextField')(null=True))

        # Changing field 'Apparat.short_d'
        db.alter_column(u'device_apparat', 'short_d', self.gf('django.db.models.fields.TextField')(null=True))

    def backwards(self, orm):
        # Deleting model 'Mailer'
        db.delete_table(u'device_mailer')


        # Changing field 'Apparat.long_d'
        db.alter_column(u'device_apparat', 'long_d', self.gf('django.db.models.fields.TextField')(default=''))

        # Changing field 'Apparat.short_d'
        db.alter_column(u'device_apparat', 'short_d', self.gf('django.db.models.fields.TextField')(default=''))

    models = {
        u'device.apparat': {
            'Meta': {'object_name': 'Apparat'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'long_d': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'power': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['device.Power']", 'null': 'True', 'blank': 'True'}),
            'short_d': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '300'})
        },
        u'device.center': {
            'Meta': {'object_name': 'Center'},
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'address': ('django.db.models.fields.CharField', [], {'max_length': '300'}),
            'geox': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'geoy': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'info1': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'info2': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'metro': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': u"orm['device.Metro']", 'null': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '300'}),
            'site': ('django.db.models.fields.URLField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'})
        },
        u'device.inspection': {
            'Meta': {'object_name': 'Inspection'},
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'body': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'kt': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'mrt': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '300'}),
            'parent': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['device.Inspection']", 'null': 'True', 'blank': 'True'})
        },
        u'device.mailer': {
            'Meta': {'object_name': 'Mailer'},
            'from_email': ('django.db.models.fields.EmailField', [], {'max_length': '75'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'notification_emails': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'})
        },
        u'device.metro': {
            'Meta': {'object_name': 'Metro'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '301'}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '50', 'unique': 'True', 'null': 'True', 'blank': 'True'})
        },
        u'device.order': {
            'Meta': {'object_name': 'Order'},
            'apparat': ('django.db.models.fields.CharField', [], {'max_length': '250', 'null': 'True', 'blank': 'True'}),
            'data': ('django.db.models.fields.CharField', [], {'max_length': '250', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'inspection': ('django.db.models.fields.CharField', [], {'max_length': '250', 'null': 'True', 'blank': 'True'}),
            'metro': ('django.db.models.fields.CharField', [], {'max_length': '250', 'null': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '250'}),
            'phone': ('django.db.models.fields.CharField', [], {'max_length': '250'}),
            'price': ('django.db.models.fields.CharField', [], {'max_length': '250', 'null': 'True', 'blank': 'True'})
        },
        u'device.power': {
            'Meta': {'object_name': 'Power'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'ordering': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'slice': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'value': ('django.db.models.fields.CharField', [], {'max_length': '10'})
        },
        u'device.pricelist': {
            'Meta': {'object_name': 'PriceList'},
            'apparat': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['device.Apparat']"}),
            'center': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'center'", 'to': u"orm['device.Center']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'info': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'inspection': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'techinfo'", 'to': u"orm['device.Inspection']"}),
            'sale': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'blank': 'True'}),
            'value': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'})
        }
    }

    complete_apps = ['device']