# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Metro'
        db.create_table(u'device_metro', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=301)),
            ('slug', self.gf('django.db.models.fields.SlugField')(max_length=50, unique=True, null=True, blank=True)),
        ))
        db.send_create_signal(u'device', ['Metro'])

        # Adding model 'Power'
        db.create_table(u'device_power', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('value', self.gf('django.db.models.fields.CharField')(max_length=10)),
            ('slice', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('ordering', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
        ))
        db.send_create_signal(u'device', ['Power'])

        # Adding model 'Apparat'
        db.create_table(u'device_apparat', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=300)),
            ('short_d', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('long_d', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('power', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['device.Power'], null=True, blank=True)),
        ))
        db.send_create_signal(u'device', ['Apparat'])

        # Adding model 'Center'
        db.create_table(u'device_center', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=300)),
            ('address', self.gf('django.db.models.fields.CharField')(max_length=300)),
            ('geox', self.gf('django.db.models.fields.FloatField')(null=True, blank=True)),
            ('geoy', self.gf('django.db.models.fields.FloatField')(null=True, blank=True)),
            ('active', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('site', self.gf('django.db.models.fields.URLField')(max_length=200, null=True, blank=True)),
            ('info1', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('info2', self.gf('django.db.models.fields.TextField')(blank=True)),
        ))
        db.send_create_signal(u'device', ['Center'])

        # Adding M2M table for field metro on 'Center'
        m2m_table_name = db.shorten_name(u'device_center_metro')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('center', models.ForeignKey(orm[u'device.center'], null=False)),
            ('metro', models.ForeignKey(orm[u'device.metro'], null=False))
        ))
        db.create_unique(m2m_table_name, ['center_id', 'metro_id'])

        # Adding model 'Inspection'
        db.create_table(u'device_inspection', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=300)),
            ('body', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('active', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('parent', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['device.Inspection'], null=True, blank=True)),
            ('mrt', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('kt', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal(u'device', ['Inspection'])

        # Adding model 'PriceList'
        db.create_table(u'device_pricelist', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('center', self.gf('django.db.models.fields.related.ForeignKey')(related_name='center', to=orm['device.Center'])),
            ('apparat', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['device.Apparat'])),
            ('sale', self.gf('django.db.models.fields.CharField')(max_length=20, null=True, blank=True)),
            ('info', self.gf('django.db.models.fields.CharField')(max_length=200, null=True, blank=True)),
            ('inspection', self.gf('django.db.models.fields.related.ForeignKey')(related_name='techinfo', to=orm['device.Inspection'])),
            ('value', self.gf('django.db.models.fields.CharField')(max_length=200, blank=True)),
        ))
        db.send_create_signal(u'device', ['PriceList'])


    def backwards(self, orm):
        # Deleting model 'Metro'
        db.delete_table(u'device_metro')

        # Deleting model 'Power'
        db.delete_table(u'device_power')

        # Deleting model 'Apparat'
        db.delete_table(u'device_apparat')

        # Deleting model 'Center'
        db.delete_table(u'device_center')

        # Removing M2M table for field metro on 'Center'
        db.delete_table(db.shorten_name(u'device_center_metro'))

        # Deleting model 'Inspection'
        db.delete_table(u'device_inspection')

        # Deleting model 'PriceList'
        db.delete_table(u'device_pricelist')


    models = {
        u'device.apparat': {
            'Meta': {'object_name': 'Apparat'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'long_d': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'power': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['device.Power']", 'null': 'True', 'blank': 'True'}),
            'short_d': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '300'})
        },
        u'device.center': {
            'Meta': {'object_name': 'Center'},
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'address': ('django.db.models.fields.CharField', [], {'max_length': '300'}),
            'geox': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'geoy': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'info1': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'info2': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'metro': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': u"orm['device.Metro']", 'null': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '300'}),
            'site': ('django.db.models.fields.URLField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'})
        },
        u'device.inspection': {
            'Meta': {'object_name': 'Inspection'},
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'body': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'kt': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'mrt': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '300'}),
            'parent': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['device.Inspection']", 'null': 'True', 'blank': 'True'})
        },
        u'device.metro': {
            'Meta': {'object_name': 'Metro'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '301'}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '50', 'unique': 'True', 'null': 'True', 'blank': 'True'})
        },
        u'device.power': {
            'Meta': {'object_name': 'Power'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'ordering': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'slice': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'value': ('django.db.models.fields.CharField', [], {'max_length': '10'})
        },
        u'device.pricelist': {
            'Meta': {'object_name': 'PriceList'},
            'apparat': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['device.Apparat']"}),
            'center': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'center'", 'to': u"orm['device.Center']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'info': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'inspection': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'techinfo'", 'to': u"orm['device.Inspection']"}),
            'sale': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'blank': 'True'}),
            'value': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'})
        }
    }

    complete_apps = ['device']