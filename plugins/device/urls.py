from django.conf.urls import url, patterns

urlpatterns = patterns('plugins.device.views',
    url(r'^get$', 'ChangeInspection', name='ChangeInspection'),
    url(r'^want/$', 'want_a_inspection', name='want_a_inspection'),
    url(r'^call/$', 'call', name='call'),
)