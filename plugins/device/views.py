# -*- coding: utf-8 -*-
from django.http import Http404
from django.shortcuts import get_object_or_404
from django.http import HttpResponse


#from utils.annoying.functions import get_object_or_None
from django.utils.translation import ugettext_lazy as _
from datetime import datetime
from django.core.mail import send_mail
from django.contrib.auth.models import User
from django.utils import simplejson
from django.core import serializers
from plugins.device.models import Center,Inspection,Apparat,Metro,PriceList,Power,Order
from plugins.device.forms import OrderForm
from plugins.mail.utils import send_order_sent_mail,call_me_please

from django.shortcuts import _get_queryset
from django.db.models.query import QuerySet

from django.utils.html import escape

from annoying.functions import get_object_or_None

def sortUnique(list_ar):
	output = []
	for x in list_ar:
		if x not in output:
			output.append(x)
	output.sort()
	return output


def ChangeInspection(request):
	inspection =None
	metro = None
	power = None
	json_finish = {}
	json_table = []
	json_tabs = []
	json_map = []
	json_center = []

	price_list = PriceList.objects.filter(center__active = True)
	#Тип обследования
	if 'v' in request.GET:
		if request.GET['v'] == 'MRT':
			price_list = price_list.filter(inspection__mrt = True)
		else:
			price_list = price_list.filter(inspection__kt = True)

	#Вид обследования
	if 'inspection' in request.GET:
		inspection = get_object_or_None(Inspection,pk=int(request.GET['inspection']))
		json_finish['parent'] = inspection.parent.pk
		json_finish['body'] = inspection.body
		price_list = price_list.filter(inspection=inspection)


	#Мощности
	power_arr = []
	if 'power0' in request.GET:
		p_obj = get_object_or_None(Power,pk=int(request.GET['power0']))
		power_arr.append(p_obj)
	
	if 'power1' in request.GET:
		p_obj = get_object_or_None(Power,pk=int(request.GET['power1']))
		power_arr.append(p_obj)
	
	if 'power2' in request.GET:
		p_obj = get_object_or_None(Power,pk=int(request.GET['power2']))
		power_arr.append(p_obj)

	if power_arr:
		price_list = price_list.filter(apparat__power__in=power_arr)

	#Метро
	if 'metro' in request.GET:
		metro = get_object_or_None(Metro,pk=int(request.GET['metro']))

	for price in price_list:
		if metro:
			if metro in price.center.metro.all():
				#metro
				json_center.append([
					price.center.name,
					price.center.geox,
					price.center.geoy,
					price.center.info1,
					price.center.info2
				])

				json_table.append([
					metro.pk,
					metro.name,
					price.value,
					price.info,
					price.apparat.title,
					price.apparat.short_d,
					price.inspection.body
				])		
		else:
			json_center.append([
				price.center.name,
				price.center.geox,
				price.center.geoy,
				price.center.info1,
				price.center.info2
			])
			for m in price.center.metro.all():
				json_table.append([
					m.pk,
					m.name,
					price.value,
					price.info,
					price.apparat.title,
					price.apparat.short_d,
					price.inspection.body
				])

	
	json_center = sortUnique(json_center)
	json_finish['table'] = json_table
	json_finish['map'] = json_center
	return HttpResponse(simplejson.dumps(json_finish), mimetype="application/json" )			



def want_a_inspection(request):
	m_name = None
	i_name = None
	if request.method == 'POST':
		form = OrderForm(request.POST)
		if form.is_valid():
			cd = form.cleaned_data
			name = cd['name']
			phone = cd['phone'] 
			appName = cd['apparat'] or None
			insId = cd['inspection'] or None
			metroId = cd['metro'] or None
			data = cd['data'] or None
			price = cd['price'] or None

			#get metro object and inspection
			if metroId and insId:
				m_name = get_object_or_None(Metro,pk=int(metroId)).name
				i_name = get_object_or_None(Inspection,pk=int(insId)).name
				
			order = Order.objects.create(
				name = name,
				phone = phone,
				metro = m_name,
				inspection = i_name,
				apparat = appName,
				data = data,
				price = price
			)
			send_order_sent_mail(order)
			return HttpResponse(simplejson.dumps({"result":"success"}))
		else:
			errors = {}
			for k in form.errors:
			    errors[k] = form.errors[k][0]
			return HttpResponse(simplejson.dumps({"errors":errors,"result":"error"}))
	

def call(request):
	if request.method == "POST":
		errors = []
		if 'name' in request.POST and 'phone' in request.POST:
			name = escape(request.POST['name'])
			phone = escape(request.POST['phone'])
			call_me_please(name,phone)
			return HttpResponse(simplejson.dumps({"result":"success"}))
		else:
			errors = {}
			errors['name'] = u'заполните,пожалуйста'
			errors['phone'] = u'заполните,пожалуйста'
	return HttpResponse(simplejson.dumps({"errors":errors,"result":"error"}))



