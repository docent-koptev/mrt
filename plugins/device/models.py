# -*- coding: utf-8 -*-
import datetime
from django.contrib.sitemaps import Sitemap
from django.db import models
import mptt
import re

class Metro(models.Model):
	name = models.CharField(max_length=301,verbose_name=u'Название метро')
	slug = models.SlugField(max_length=50,unique=True,verbose_name=u'Для адреса',blank=True,null=True)
	
	class Meta:
		verbose_name = u'Метро'
		verbose_name_plural = u'Метро'

	def __unicode__(self):
		return self.name

#Мощность аппарата
class Power(models.Model):
	value = models.CharField(max_length=10,verbose_name=u'Значение мощности')
	ordering = models.IntegerField(u"Сортировка",blank=True,null=True,help_text=u"Желательно указать для группы")
	slice = models.BooleanField(u"Срез",default=False,help_text=u"Только для КТ")
	
	class Meta:
		verbose_name = u'Мощность'
		verbose_name_plural = u'Мощности'

	def __unicode__(self):
		return self.value

#Аппараты и их типы
class Apparat(models.Model):
	title = models.CharField(max_length=300,verbose_name=u'Название аппарата',help_text=u'Только для администратора')
	short_d = models.TextField(verbose_name=u'Свойство аппарата',blank=True,null=True)
	long_d = models.TextField(verbose_name=u'Краткое описание аппарата',blank=True,null=True)
	power = models.ForeignKey(Power,verbose_name=u"Мощность",blank=True,null=True)

	class Meta:
		verbose_name = u'Вид аппарата'
		verbose_name_plural = u'Виды аппаратов'

	def __unicode__(self):
		return self.title


class Center(models.Model):
	name = models.CharField(max_length=300,verbose_name=u'Название')
	address = models.CharField(max_length=300,verbose_name=u'Адрес')
	metro = models.ManyToManyField(Metro,verbose_name=u'Метро',blank=True,null=True)
	geox = models.FloatField(verbose_name=u'X-координата',blank=True,null=True)
	geoy = models.FloatField(verbose_name=u'Y-координата',blank=True,null=True)
	active = models.BooleanField(u"Центр работает", default=True)
	site = models.URLField(verbose_name=u'Ссылка на сайт',blank=True,null=True)
	info1 = models.TextField(verbose_name=u'Доп.инфо 1',blank=True)
	info2 = models.TextField(verbose_name=u'Доп.инфо 2',blank=True)

	class Meta:
		verbose_name = u'Исследовательский центр'
		verbose_name_plural = u'Исследовательские центры'

	def __unicode__(self):
		return self.name


	def get_all_metro(self):
		object = self
		metro = object.metro.all()
		return metro 

	def get_one_metro(self):
		object = self
		one_metro = object.get_all_metro()[0]
		return one_metro



#Вид обследования
class Inspection(models.Model):
	name = models.CharField(max_length=300,verbose_name=u'Название')
	body = models.TextField(verbose_name=u'Описание',blank=True)
	active = models.BooleanField(default=False,verbose_name=u'Активность')
	parent = models.ForeignKey('self',verbose_name=u'Категория',null=True,blank=True,help_text=u'Если не главная категория')
	mrt = models.BooleanField(verbose_name=u'МРТ',default=False)
	kt = models.BooleanField(verbose_name=u'КТ',default=False)


	class Meta:
		verbose_name = u'Вид обследования'
		verbose_name_plural = u'Виды обследования'

	def __unicode__(self):
		return self.name

	def get_type(self):
		if self.mrt:
			ins_t = 'mrt'
		else:
			ins_t = 'kt'
		return ins_t

#Цены и их вариации
class PriceList(models.Model):
	center = models.ForeignKey(Center,verbose_name=u'Центр', related_name="center")
	apparat = models.ForeignKey(Apparat,verbose_name=u'Аппарат')
	sale = models.CharField(u'Скидка %',max_length=20, blank=True,null=True,help_text=u'Указываем в процентах')
	info = models.CharField(u'Описание',max_length=200, blank=True,null=True)
	inspection = models.ForeignKey(Inspection, verbose_name=u'Обследование', related_name="techinfo")
	value = models.CharField(u'Цена в рублях',max_length=200, blank=True,help_text=u'Указываем в рублях')


	def __unicode__(self):
		return  u"%s - %s" % (self.center.name, self.value)

	class Meta:
		verbose_name = u'Ценник'
		verbose_name_plural = u'Ценники'

class Order(models.Model):
	name = models.CharField(verbose_name=u'Имя',max_length=250)
	phone = models.CharField(verbose_name=u'Телефон',max_length=250)
	inspection = models.CharField(verbose_name=u'Область обследования',max_length=250,blank=True,null=True)
	metro = models.CharField(verbose_name=u'Метро',blank=True,null=True,max_length=250)
	data = models.CharField(verbose_name=u'Дата приема',blank=True,null=True,max_length=250)
	price = models.CharField(verbose_name=u'Цена',blank=True,null=True,max_length=250)
	apparat = models.CharField(verbose_name=u'Аппарат',blank=True,null=True,max_length=250)

	def __unicode__(self):
		return u"%s - %s" % (self.name, self.phone)

	class Meta:
		verbose_name = u'Заказ'
		verbose_name_plural = u'Заказы'



class Mailer(models.Model):
	from_email = models.EmailField(u"От кого")
	notification_emails  = models.TextField("Пересыл на адреса",help_text=u'Пишем через пробел почтовые адреса',blank=True,null=True)

	def __unicode__(self):
		return self.from_email

	class Meta:
		verbose_name = u'Почтовый адрес'
		verbose_name_plural = u'Почтовые адреса'

	def get_notification_emails(self):
		adresses = re.split("[\s,]+", self.notification_emails)
		return adresses