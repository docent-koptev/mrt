# -*- coding: utf-8 -*-
# python imports
from datetime import datetime
from models import Order

# django imports
from django import forms


class OrderForm(forms.ModelForm):
	class Meta:
		model = Order
