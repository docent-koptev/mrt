# -*- coding: utf-8 -*-
# django imports
from django.contrib import admin
from django import forms
from django.db import models
from django.conf import settings

from models import Center,Apparat,Inspection,Metro,Power,PriceList,Order,Mailer
from rollyourown.seo.admin import register_seo_admin, get_inline
from plugins.seo import SEOMetadata
from ckeditor.widgets import CKEditorWidget
from mptt.admin import MPTTModelAdmin as FeinCMSModelAdmin

class PriceInfoForm(forms.ModelForm):
    class Meta:
        model = PriceList
        fields = ("apparat","inspection","info","sale","value",)


class ProductTechInfoInline(admin.TabularInline):
    model = PriceList
    extra = 1
    form = PriceInfoForm


class CenterAdminForm(forms.ModelForm):
        info1 = forms.CharField(widget=CKEditorWidget(),label=u'Описание 1')
        info2 = forms.CharField(widget=CKEditorWidget(),label=u'Описание 2')
        class Meta:
            model = Center

class CenterAdmin(admin.ModelAdmin):
	list_display = ('id', 'name', 'address', 'active', 'geox', 'geoy')
	list_filter = ('name','address','active')
	save_on_top = True
	search_fields = ('name', 'address', 'active')
	inlines = [ProductTechInfoInline,]
	list_display_links =("name",)
	fieldsets = (
		('Общая информация', {
			'fields': (('name', 'address','active'),'metro',('geox','geoy')),
		}),

		('Дополнительная информация', {
			'fields': ('site', 'info1','info2',),
		}),
	)
	form = CenterAdminForm

class MetroAdmin(admin.ModelAdmin):
    prepopulated_fields = {"slug": ("name", )}
    list_display = ('id', 'name', 'slug')
    list_display_links =("name",)

class InspectionAdminForm(forms.ModelForm):
        body = forms.CharField(widget=CKEditorWidget(),label=u'Полное описание')
        class Meta:
            model = Inspection

class InspectionAdmin(admin.ModelAdmin):
	list_display = ('name', 'active', 'mrt', 'kt')
	list_filter = ('mrt','kt','name','active')
	save_on_top = True
	search_fields = ('name', 'mrt', 'kt')
	form = InspectionAdminForm


class ApparatAdminForm(forms.ModelForm):
        long_d = forms.CharField(widget=CKEditorWidget(),label=u'Полное описание',required = False)
        short_d = forms.CharField(widget=CKEditorWidget(),label=u'Краткое описание',required = False)
        class Meta:
            model = Apparat


class PowerAdmin(admin.ModelAdmin):
	list_display = ('value', 'slice')
	list_display_links =("value",)

class ApparatAdmin(admin.ModelAdmin):
	form = ApparatAdminForm
	list_display = ('id', 'title', 'short_d','power')
	list_display_links =("title",)
	list_filter = ("title","power")
	

class OrderAdmin(admin.ModelAdmin):
	list_display = ('name','phone','data','price','apparat')
	list_filter = ('name','phone','price','apparat')
	list_display_links =("name",)

admin.site.register(Order,OrderAdmin)

class MailerAdmin(admin.ModelAdmin):
	list_display = ("from_email",)
	list_display_links = ("from_email",)

admin.site.register(Mailer,MailerAdmin)

admin.site.register(Center,CenterAdmin)
admin.site.register(Apparat,ApparatAdmin)
admin.site.register(Inspection,InspectionAdmin)
admin.site.register(Metro,MetroAdmin)
admin.site.register(Power,PowerAdmin)

