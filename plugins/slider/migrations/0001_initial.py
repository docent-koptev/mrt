# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Slider'
        db.create_table(u'slider_slider', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=250, blank=True)),
            ('show_on', self.gf('django.db.models.fields.CharField')(default=u'main', max_length=250)),
            ('image', self.gf('sorl.thumbnail.fields.ImageField')(max_length=100)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=250, blank=True)),
            ('url', self.gf('django.db.models.fields.CharField')(max_length=10000, blank=True)),
            ('target', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('display', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('position', self.gf('django.db.models.fields.IntegerField')(default=100)),
        ))
        db.send_create_signal(u'slider', ['Slider'])


    def backwards(self, orm):
        # Deleting model 'Slider'
        db.delete_table(u'slider_slider')


    models = {
        u'slider.slider': {
            'Meta': {'ordering': "('-position', 'show_on')", 'object_name': 'Slider'},
            'display': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('sorl.thumbnail.fields.ImageField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '250', 'blank': 'True'}),
            'position': ('django.db.models.fields.IntegerField', [], {'default': '100'}),
            'show_on': ('django.db.models.fields.CharField', [], {'default': "u'main'", 'max_length': '250'}),
            'target': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '250', 'blank': 'True'}),
            'url': ('django.db.models.fields.CharField', [], {'max_length': '10000', 'blank': 'True'})
        }
    }

    complete_apps = ['slider']