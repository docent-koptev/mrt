# -*- coding: utf-8 -*-
# django imports
from django.core.mail import EmailMultiAlternatives
from django.template.loader import render_to_string
from settings import * 



def send_order_sent_mail(order):
    subject = u"Поступила заявка на обследование"
    html = render_to_string("mail/order_sent_mail.html", {
        "order" : order
    })

    mail = EmailMultiAlternatives(
        subject=subject, body=html, from_email=from_email, to=to, bcc=bcc)

    mail.attach_alternative(html, "text/html")
    mail.send()



def call_me_please(name,phone):
    subject = u"Поступила заявка на звонок"
    html = render_to_string("mail/call_me_please.html", {
        "name" : name,
        "phone":phone
    })
    mail = EmailMultiAlternatives(
        subject=subject, body=html, from_email=from_email, to=to, bcc=bcc)

    mail.attach_alternative(html, "text/html")
    mail.send()