from django.shortcuts import _get_queryset
from django.db.models.query import QuerySet

from plugins.device.models import Mailer

def get_first_or_None(qsinit, *args, **kwargs):
    if not qsinit:
        return None

    if isinstance(qsinit, list):
        try:
            return qsinit[0]
        except IndexError:
            return None

    if isinstance(qsinit, QuerySet):
        queryset = qsinit
    elif isinstance(qsinit, type):
        queryset = _get_queryset(qsinit)
    try:
        return queryset.filter(*args, **kwargs)[0]
    except IndexError:
        return None


mailer = get_first_or_None(Mailer)
to = ['spam@chubarev.net',]
bcc = None
if mailer:
    from_email = mailer.from_email
    bcc = mailer.get_notification_emails()
