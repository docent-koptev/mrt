# -*- coding: utf-8 -*-
from rollyourown import seo

class SEOMetadata(seo.Metadata):
    title       = seo.Tag(head=True, max_length=250)
    description = seo.MetaTag(max_length=250)
    keywords    = seo.KeywordTag()

    class Meta:
        seo_models = ('page','news','device')
        seo_views = ('page','home','news')
        verbose_name = "SEO"
        verbose_name_plural = "SEO"