# -*- coding: utf-8 -*-
import datetime
from django.contrib.sitemaps import Sitemap
from django.db import models
from django.utils.translation import ugettext_lazy as _
from sorl.thumbnail.fields import ImageField

class News(models.Model):
    title = models.CharField(_(u'заголовок'), max_length=255)
    slug = models.SlugField(unique=True)
    body = models.TextField(verbose_name=u'текст акции', blank=True,null=True)
    short = models.TextField(verbose_name=u'кратко', blank=True,null=True)
    image = ImageField(u'изображение', upload_to='uploads/news', blank=True)
    is_share = models.BooleanField(u'акция?', default=False)
    display = models.BooleanField(u'опубликовать', default=True)
    due_date = models.DateField(u'дата окончания акции', blank=True, null=True)
    creation_date = models.DateTimeField(u'дата публикации', blank=True)
    last_modification = models.DateTimeField(u'дата последнего изменения', blank=True)
    is_sended = models.BooleanField(u'разослано', default=False)

    class Meta:
        ordering = ('-creation_date',)
        verbose_name = _(u'Акция')
        verbose_name_plural = _(u'Акции')

    def __unicode__(self):
        return self.title

    def save(self, force_insert=False, force_update=False, *args, **kw):
        self.last_modification = datetime.datetime.now()
        if not self.creation_date:
            self.creation_date = datetime.datetime.now()
        super(News, self).save(*args, **kw)

    @models.permalink
    def get_absolute_url(self):
        return 'news_detail', (), {'slug': self.slug}

class NewsSitemap(Sitemap):
    changefreq = "daily"
    priority = 0.5

    def items(self):
        return News.objects.filter(display=True)