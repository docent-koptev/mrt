# -*- coding: utf-8 -*-
from django.contrib import admin
from django.conf import settings
from django.db import models
from django import forms
from models import News
from sorl.thumbnail import get_thumbnail
from sorl.thumbnail.admin import AdminImageMixin

from ckeditor.widgets import CKEditorWidget
from rollyourown.seo.admin import register_seo_admin, get_inline

class NewsAdminForm(forms.ModelForm):
       # body = forms.CharField(widget=CKEditorWidget())
        short = forms.CharField(widget=CKEditorWidget())
        class Meta:
            model = News

class NewsAdmin(AdminImageMixin, admin.ModelAdmin):
    list_display = ['title', 'admin_image_preview', 'display', 'last_modification', 'creation_date','due_date']
    list_editable = ['display',]
    search_fields = ['title', 'short']
    list_filter = ['display',]
    date_hierarchy = 'creation_date'
    readonly_fields = ['last_modification',]
    formfield_overrides = {
        models.TextField: {'widget':forms.Textarea(attrs={'class':'ckeditor'})}
    }
    prepopulated_fields = {'slug': ['title',]}
    fieldsets = (
        (None,{
            'fields': ('title', 'slug', 'short', 'image', 'display', ('creation_date', 'last_modification'),'due_date' )
        }),
    )

    form = NewsAdminForm

    def admin_image_preview(self, obj):
        if obj.image:
            thumbnail = get_thumbnail(obj.image, '70')
            return '<a href="%s" target="_blank"><image style="max-height:70px;max-width:70px" src="%s"/></a>' % (obj.image.url, thumbnail.url)
        return ''
    admin_image_preview.short_description = u'превью'
    admin_image_preview.allow_tags = True



admin.site.register(News, NewsAdmin)