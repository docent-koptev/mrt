from datetime import datetime
from django import template
from plugins.news.models import News

register = template.Library()

@register.inclusion_tag('tags/last_news.html')
def last_news(number):
    last = News.objects.filter(display=True, creation_date__lt=datetime.now())[:number]
    return {'last': last}