from django.conf.urls import url, patterns

urlpatterns = patterns('plugins.news.views',
    url(r'^$', 'news', name='news'),
    url(r'^(?P<slug>[-\w]*)$', 'news_detail', name='news_detail'),
)