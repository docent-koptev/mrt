var mrtApp = angular.module('mrtApp', ['ngSanitize','yaMap',]);

mrtApp.config(['$httpProvider', function($httpProvider) {
    $httpProvider.defaults.xsrfCookieName = 'csrftoken';
    $httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken';    }
]);

mrtApp.directive('loading', function () {
      return {
        restrict: 'E',
        replace:false,
        template: '<div class="loading"><img src="/media/static/img/ajax-loader.gif" /></div>',
      }
  })

mrtApp.directive('sibs', function() {
    return {
        link: function(scope, element, attrs) {
            element.bind('click', function() {
                element.next("ul").removeClass('ng-hide').addClass('ng-show');
                element.addClass('ng-show');
            })
        },
    }
});

mrtApp.directive("myQtip", function () {
    return function (scope, element, attrs) {
        var text = element.attr('qtip-content') || null,
          title = element.attr('qtip-title') || null;
        scope.qtipSkin = (attrs.skin ? "qtip-" + attrs.skin : "qtip-dark");
        element.qtip({
			content: {
						text: $('#frm_recall')
					},
            style: {
				classes: 'qtip-light qtip-rounded qtip-shadow',
				tip: {
					corner: true
				}
			},
            show: {
                event: 'click',
                solo: true
            },
          	hide: {
			  event: 'unfocus'
			},
            position: {
				corner: true,
				my: 'top center',
				at: 'bottom center'
			},
        });

        scope.$on("$destroy", function () {
            $(element).qtip('destroy', true); // Immediately destroy all tooltips belonging to the selected elements
        });
        
    }
});


mrtApp.controller('ChangeInspectionController', ['$scope','$http',function($scope,$http) {
	
	$scope.mapTable = {};
	$scope.ktData = {}; //кт-обследование
	$scope.mrtData = {}; //мрт-обследование
	$scope.formData = {}; //Объект запроса. Берет в себя либо ktData либо mrtData
	
	$scope.parentInsp = null;//Родитель текущего обследования. Надо для табов
	$scope.inspBody = null; //Тело обследования. Надо для табов
	
	$scope.mainForm = {}; //Форма запроса главная
	$scope.center = {};
	$scope.mainFormErrors = {};
	
	$scope.callMeData = {};
	$scope.callFormErrors = {};
	$scope.tabsData = {};

	$scope.changeInspection = function(vid,tabs,block){
		if (vid == 'MRT'){
			//по умолчанию мрт головы
			if (tabs == 1){$scope.mrtData['inspection'] = 6;}
			if (block){$scope.mrtData['inspection'] = block;}			
			$scope.mrtData['v'] = 'MRT';
			$scope.formData = $scope.mrtData;

			
		}else{
			//по умолчанию кт головы
			if (tabs == 1){$scope.ktData['inspection'] = 45;}
			if (block){$scope.ktData['inspection'] = block;}
			$scope.ktData['v'] = 'KT';
			$scope.formData = $scope.ktData;
		}
		console.log($scope.tabsData);
		$scope.loading = true;

		if ($scope.formData.power0 == 'false'){delete $scope.formData.power0;}
		if ($scope.formData.power1 == 'false'){delete $scope.formData.power1;}
		if ($scope.formData.power2 == 'false'){delete $scope.formData.power2;}
		if ($scope.formData.metro == ''){delete $scope.formData.metro;}
		if ($scope.formData.inspection == ''){delete $scope.formData.inspection;}
	
		$http({
	        method  : 'get',
	        url     : '/inspection/get?' + $.param($scope.formData),
	        headers : { 'Content-Type': 'application/x-www-form-urlencoded', }  // set the headers so angular passing info as form data (not request payload)
	    })
	    .success(function(data) {
	    	$scope.loading = false;
	    	$scope.inspection = data['table'];
	    	$scope.parentInsp = data['parent'];
	    	$scope.inspBody = data['body'];
	    	$scope.center = data['map'];
	    	
	    	var obj = [];
			for(var i=0;i<$scope.center.length;i++){
				obj.push({
					geometry:{
			            type:'Point',
			            coordinates:[$scope.center[i][1],$scope.center[i][2],]
			        },
			        properties:{
			            balloonContentFooter:  $scope.center[i][4]
			        }
				});
			}
			$scope.geoObjects = obj;	
		    });

	};

}]);


//Controller for work with ajax-forms
mrtApp.controller('SendFormController', ['$scope','$http',function($scope,$http) {
	$scope.formBottomData = {};
	$scope.formBottomDataErrors = {};
	$scope.formBottomData['v'] = 'MRT';

	//Кликаем по таблице
	$scope.clickForTable = function(ins){
		$scope.mainFormErrors.name = null;
		$scope.mainFormErrors.phone = null;
		$scope.mainForm['apparat'] = ins[4];
		$scope.mainForm['price'] = ins[2];
		$scope.mainForm['inspection'] = $scope.formData.inspection;
		$scope.mainForm['metro'] = ins[0];
		$.fancybox.open({inline:true, href:"#order_content"});
	};

	//Отправка Main-формы
	$scope.mainFormSubmit = function(e){
		$scope.loading = true;
		e.preventDefault();
		$http({
			method:"POST",
			url:"/inspection/want/",
			data:$.param($scope.mainForm),
			headers :{'Content-Type':'application/x-www-form-urlencoded'}
		})
		.success(function(data){
			$scope.loading = false;
			if (data.errors){
				$scope.mainFormErrors['name'] = data.errors['name'];
				$scope.mainFormErrors['phone'] = data.errors['phone'];
			}else{
				$scope.mainFormErrors['success'] = '<b>Спасибо!</b> <br> Ваше сообщение успешно отправлено';
			}
		});
	};

	//Клик по верхней кнопке
	$scope.clickForButton = function(){
		$scope.mainFormErrors.name = null;
		$scope.mainFormErrors.phone = null;
		$scope.mainForm['inspection'] = $scope.formData.inspection;
		if ($scope.formData.metro){
			$scope.mainForm['metro'] = $scope.formData.metro;
		}else{
			delete $scope.mainForm.metro;
		}
		
		$.fancybox.open({inline:true, href:"#order_content"});
	};

	//Заказать звонок
	$scope.callMePlease = function(e){
		$scope.loading = true;
		$scope.callFormErrors.name = null;
		$scope.callFormErrors.phone = null;
		e.preventDefault();
		$http({
			method:"POST",
			url:"/inspection/call/",
			data:$.param($scope.callMeData),
			headers :{'Content-Type':'application/x-www-form-urlencoded'}
		})
		.success(function(data){
			$scope.loading = false;
			if(data.errors){
				$scope.callFormErrors['name'] = 'заполните,пожалуйста';
				$scope.callFormErrors['phone'] = 'заполните,пожалуйста';
			}else{
				$scope.callFormErrors['success'] = true;
			}
		});
	};

	//Форма в подвале страниц
	$scope.formInBottomSend = function(e){
		e.preventDefault;
		$scope.loading = true;
		$http({
			url:"/inspection/want/",
			method:"POST",
			data:$.param($scope.formBottomData),
			headers :{'Content-Type':'application/x-www-form-urlencoded'}
		})
		.success(function(data){
			$scope.loading = false;
			if(data.errors){
				$scope.formBottomDataErrors['name'] = 'заполните,пожалуйста';
				$scope.formBottomDataErrors['phone'] = 'заполните,пожалуйста';
			}else{
				$scope.formBottomDataErrors['success'] = true;
			}

		});

	};

}]);


mrtApp.controller('yandexMap', ['$scope','$http','$timeout',function($scope,$http,$timeout) {
	$scope.changeTableMap = function(element){
		if (element == 'table'){
			$scope.mapTable['vid'] = 'table';
		}else{
			$scope.mapTable['vid'] = 'map';
		}
	};
}]);
