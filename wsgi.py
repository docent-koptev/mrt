import os
import sys

sys.path.insert(0, '/var/www/docent/menv/lib/python2.7/site-packages')
sys.path.insert(0, '/var/www/docent/mrt')

from django.core.handlers.wsgi import WSGIHandler

os.environ['DJANGO_SETTINGS_MODULE'] = 'settings'
application = WSGIHandler()
