from datetime import datetime
from django import template
from django.template.defaultfilters import floatformat
from plugins.device.models import Center,Inspection,Apparat,Metro
from plugins.page.models import Page

register = template.Library()

@register.inclusion_tag('includes/selector.html', takes_context=True)
def selector(context,category,vid):
	if vid == 'mrt':
		children = Inspection.objects.filter(parent=category,mrt=True)
	else:
		children = Inspection.objects.filter(parent=category,kt=True)
	context.update({'children':children,'vid':vid,})
	return context

@register.inclusion_tag('includes/catalog.html', takes_context=True)
def catalog_all(context,metro):
	center = Center.objects.filter(metro__in = metro)
	context.update({'inspection':center,})
	return context


@register.inclusion_tag('includes/tabs.html', takes_context=True)
def tabular(context,category,vid):
	if vid == 'mrt':
		children = Inspection.objects.filter(parent=category,mrt=True)
	else:
		children = Inspection.objects.filter(parent=category,kt=True)
	context.update({'children':children,'vid':vid,})
	return context


def formatted_float(value):
	value = floatformat(value, arg=4)
	return str(value).replace(',','.')
register.filter('formatted_float', formatted_float)


@register.inclusion_tag('includes/form_order.html', takes_context=True)
def page_menu(context):
	mrt_insps = Inspection.objects.filter(mrt=True).order_by('-name')
	kt_insps = Inspection.objects.filter(kt=True).order_by('-name')
	metro = Metro.objects.all().distinct().order_by("name")
	context.update({'kt_insps':kt_insps,'mrt_insps':mrt_insps,'metro':metro,})
	return context


@register.inclusion_tag('includes/top_menu.html', takes_context=True)
def top_menu(context):
	all_inspections =  Inspection.objects.all().exclude(parent=None).order_by('-name')
	metro = Metro.objects.all().distinct().order_by("name")
	context.update({'all_inspections':all_inspections,'metro':metro,})
	return context