# -*- coding: utf-8 -*-
from django.core.urlresolvers import reverse
from django.utils.translation import ugettext_lazy as _
from django_generic_flatblocks.models import GenericFlatblock

from admin_tools.menu import items, Menu


def get_gblock(slug):
    try:
        return GenericFlatblock.objects.get(slug=slug).content_object
    except Exception as ex:
        return None

def get_gblock_admin_url(slug):
    related_object = get_gblock(slug)
    if related_object:
        app_label = related_object._meta.app_label
        module_name = related_object._meta.module_name
        return '/admin/%s/%s/%s/' % (app_label, module_name, related_object.pk)
    else:
        return '#'


class CustomMenu(Menu):
    """
    Custom Menu for docent admin site.
    """
    def __init__(self, **kwargs):
        Menu.__init__(self, **kwargs)
        self.children += [
            items.MenuItem(_('Dashboard'), reverse('admin:index')),
            items.Bookmarks(),            
            items.ModelList(
                u'Сайт',
                models=('page.models.ActionGroup','page.models.Action', 'page.models.Page', 'news.models.News', 'ecko.slider.models.Slider',),
                children = [
                    items.MenuItem(u'Редактируемые блоки',
                        children = [

                            items.MenuItem(
                                u'--------Подвал сайта-------',

                            ),
                            items.MenuItem(
                                u'ссылка для FB',
                                get_gblock_admin_url('fb'),
                            ),
                            items.MenuItem(
                                u'ссылка для VK',
                                get_gblock_admin_url('vk_link'),
                            ),
                            items.MenuItem(
                                u'ссылка для TWITTER',
                                get_gblock_admin_url('tw'),
                            ),
                            items.MenuItem(
                                u'ссылка для RSS',
                                get_gblock_admin_url('rss'),
                            ),

                            items.MenuItem(
                                u'ссылка для INSTAGRAM',
                                get_gblock_admin_url('instagram'),
                            ),
                            
                            items.MenuItem(
                                u'copyright',
                                get_gblock_admin_url('copyright'),
                            ),

                            items.MenuItem(
                                u'Логотип в футере',
                                get_gblock_admin_url('logo'),
                            ),
                            items.MenuItem(
                                u'------------------------------',

                            ),

                            items.MenuItem(
                                u'Текст на главной',
                                get_gblock_admin_url('help'),
                            ),
                            items.MenuItem(
                                u'О МРТ на главной',
                                get_gblock_admin_url('about_mrt'),
                            ),
                            items.MenuItem(
                                u'О КТ на главной',
                                get_gblock_admin_url('about_kt'),
                            ),

                            items.MenuItem(
                                u'Часы работы в шапке',
                                get_gblock_admin_url('phone'),
                            ),
                            items.MenuItem(
                                u'Метрика',
                                get_gblock_admin_url('footer_b'),
                            ),
                            items.MenuItem(
                                u'Акция на главной',
                                get_gblock_admin_url('our_choice'),
                            ),
                            
                            items.MenuItem(
                                u'Допольнительные META',
                                get_gblock_admin_url('meta_b'),
                            ),
    
                        ]
                    ),
                ]
            ),
            items.ModelList(
                u'SEO',
                ('rollyourown.seo.*', ),
                children = [
                    items.MenuItem(
                        u'код для счётчиков перед </footer>',
                        get_gblock_admin_url('footer_block'),
                    ),
                    items.MenuItem(
                        u'код для счётчиков перед </body>',
                        get_gblock_admin_url('bottom_block'),
                    ),
                    items.MenuItem(
                        u'robots.txt',
                        get_gblock_admin_url('robots_txt'),
                    ),
                ]
            ),
        ]


    def init_with_context(self, context):
        """
        Use this method if you need to access the request context.
        """
        return super(CustomMenu, self).init_with_context(context)
