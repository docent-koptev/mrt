# -*- coding: utf-8 -*-
"""
This file was generated with the customdashboard management command, it
contains the two classes for the main dashboard and app index dashboard.
You can customize these classes as you want.

To activate your index dashboard add the following to your settings.py::
    ADMIN_TOOLS_INDEX_DASHBOARD = 'docent.dashboard.CustomIndexDashboard'

And to activate the app index dashboard::
    ADMIN_TOOLS_APP_INDEX_DASHBOARD = 'docent.dashboard.CustomAppIndexDashboard'
"""

from django.utils.translation import ugettext_lazy as _
from django.core.urlresolvers import reverse

from admin_tools.dashboard import modules, Dashboard, AppIndexDashboard
from admin_tools.utils import get_admin_site_name


class CustomIndexDashboard(Dashboard):
    """
    Custom index dashboard for docent.
    """
    def init_with_context(self, context):
        site_name = get_admin_site_name(context)
        # append a link list module for "quick links"

        self.children.append(modules.ModelList(
            u'Сайт',
            models = ('plugins.page.models.ActionGroup', 'plugins.page.models.Page', 'plugins.news.models.News',
                    'django.contrib.*',),
        ))

        self.children.append(modules.ModelList(
            u'Заказы и настройка EMAIL адресов',
            ('plugins.device.models.Order','plugins.device.models.Mailer',),
        ))

        self.children.append(modules.ModelList(
            u'Настройка обследований и центров',
            ('plugins.device.models.Center','plugins.device.models.Inspection','plugins.device.models.Metro','plugins.device.models.Apparat','plugins.device.models.Power',),
        ))

        # append another link list module for "support".



class CustomAppIndexDashboard(AppIndexDashboard):
    """
    Custom app index dashboard for docent.
    """

    # we disable title because its redundant with the model list module
    title = ''

    def __init__(self, *args, **kwargs):
        AppIndexDashboard.__init__(self, *args, **kwargs)

        # append a model list module and a recent actions module
        self.children += [
            modules.ModelList(self.app_title, self.models),
            modules.RecentActions(
                _('Recent Actions'),
                include_list=self.get_app_content_types(),
                limit=5
            )
        ]

    def init_with_context(self, context):
        """
        Use this method if you need to access the request context.
        """
        return super(CustomAppIndexDashboard, self).init_with_context(context)
